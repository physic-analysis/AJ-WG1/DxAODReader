
void CollinearApproximation()
{
    TCanvas * C_LQLep            = new TCanvas("C_LQLep","C_LQLep");
    TCanvas * C_LQHad            = new TCanvas("C_LQHad","C_LQHad");

//   std::vector<std::string> mass_point = {"300", "500", "900", "1300", "1700"};
   std::vector<std::string> mass_point = {"300", "1700"};
    for ( int iMass=0; iMass< mass_point.size(); iMass++){
	std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/MyNtuple.M" + mass_point[iMass] + ".root";
	std::cout << file_name << std::endl;
	TFile* file = new TFile( file_name.c_str() );
	TTree* tree = (TTree*)file->Get("m_true");
	
        bool EventFlag;
        bool isLeptonEle;
        tree->SetBranchAddress("EventFlag"   , &EventFlag );
        tree->SetBranchAddress("isLeptonEle" , &isLeptonEle );

	TLorentzVector* TLV_Lepton    = nullptr;
	TLorentzVector* TLV_Electron  = nullptr;
	TLorentzVector* TLV_Muon      = nullptr;
	TLorentzVector* TLV_TauJet    = nullptr;
	TLorentzVector* TLV_LepNeu    = nullptr;
	TLorentzVector* TLV_EleNeu    = nullptr;
	TLorentzVector* TLV_MuNeu     = nullptr;
	TLorentzVector* TLV_HadTauNeu = nullptr;
	TLorentzVector* TLV_LepTauNeu = nullptr;
	TLorentzVector* TLV_HadBJet   = nullptr;
	TLorentzVector* TLV_LepBJet   = nullptr;
	TLorentzVector* TLV_HadBQuark = nullptr;
	TLorentzVector* TLV_LepBQuark = nullptr;
	tree->SetBranchAddress("TLV_Lepton"    , &TLV_Lepton   );
	tree->SetBranchAddress("TLV_Electron"  , &TLV_Electron );
	tree->SetBranchAddress("TLV_Muon"      , &TLV_Muon     );
	tree->SetBranchAddress("TLV_TauJet"    , &TLV_TauJet   );
	tree->SetBranchAddress("TLV_LepNeu"    , &TLV_LepNeu   );
	tree->SetBranchAddress("TLV_EleNeu"    , &TLV_EleNeu   );
	tree->SetBranchAddress("TLV_MuNeu"     , &TLV_MuNeu    );
	tree->SetBranchAddress("TLV_HadTauNeu" , &TLV_HadTauNeu);
	tree->SetBranchAddress("TLV_LepTauNeu" , &TLV_LepTauNeu);
	tree->SetBranchAddress("TLV_HadBJet"   , &TLV_HadBJet  );
	tree->SetBranchAddress("TLV_LepBJet"   , &TLV_LepBJet  );
	tree->SetBranchAddress("TLV_HadBQuark" , &TLV_HadBQuark  );
	tree->SetBranchAddress("TLV_LepBQuark" , &TLV_LepBQuark  );
	
	double Lep_P   ;
	double Lep_theta   ;
	double Lep_phi     ;
	double TauJet_P;
	double TauJet_theta;
	double TauJet_phi  ;
	double MET_x       ;
	double MET_y       ;
	tree->SetBranchAddress("Lep_P"                 , &Lep_P       );
        tree->SetBranchAddress("Lep_theta"             , &Lep_theta   );
	tree->SetBranchAddress("Lep_phi"               , &Lep_phi     );
	tree->SetBranchAddress("TauJet_P"          , &TauJet_P);
	tree->SetBranchAddress("TauJet_theta"          , &TauJet_theta);
	tree->SetBranchAddress("TauJet_phi"            , &TauJet_phi  );
	tree->SetBranchAddress("MET_x"                 , &MET_x       );
	tree->SetBranchAddress("MET_y"                 , &MET_y       );
	
	double LQ_M_lep_directLepAntiKt4Jet;
	double LQ_M_had_VisTauAntiKt4Jet;
        tree->SetBranchAddress("LQ_M_lep_directLepAntiKt4Jet" , &LQ_M_lep_directLepAntiKt4Jet   );
	tree->SetBranchAddress("LQ_M_had_VisTauAntiKt4Jet"    , &LQ_M_had_VisTauAntiKt4Jet   );
	
        TH1D* h_lep             = new TH1D("h_lep","h_lep;M_{lep} [GeV];"          ,80,0,2000);
	TH1D* h_lepAfter        = new TH1D("h_lepAfter","h_lepAfter;M_{lep} [GeV];",80,0,2000);
	TH1D* h_had      = new TH1D("h_had","h_had;M_{had} [GeV];"          ,80,0,2000);
	TH1D* h_hadAfter = new TH1D("h_hadAfter","h_hadAfter;M_{had} [GeV];",80,0,2000);
	

	EColor color[5] = { kBlack, kMagenta, kBlue, kGreen, kRed };
	
	int TotalEvents = 0 ;
	int PairingTrue = 0 ;
	int PairingFalse = 0 ;
	for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
	    tree->GetEntry(iEntry);

            if ( !EventFlag ) continue;
	    
	    // Collinear approximation
	    double phi_1   = Lep_phi;
	    double phi_2   = TauJet_phi;
	    double theta_1 = Lep_theta;
	    double theta_2 = TauJet_theta;
	    //  LQlep side tau
	    double p_miss1 = (MET_x*TMath::Sin(phi_2) - MET_y*TMath::Cos(phi_2))/(TMath::Sin(theta_1)*TMath::Sin(phi_2 - phi_1));
	    double x1      = Lep_P/(Lep_P + p_miss1);
	    
	    //  LQhad side tau
	    double p_miss2 = (MET_x*TMath::Sin(phi_1) - MET_y*TMath::Cos(phi_1))/(TMath::Sin(theta_2)*TMath::Sin(phi_1 - phi_2));
	    double x2      = TauJet_P/(TauJet_P + p_miss2);

	    if ( p_miss1 < 0 || p_miss2 < 0 ) continue;
	    static bool PmissCondition = false;
	    if ( !PmissCondition ) {
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		std::cout << "          P_{mis} < 0 will be rejected.         " << std::endl;
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		PmissCondition = true;
	    }
            TotalEvents++;
            
	    double mod_mass_lep = LQ_M_lep_directLepAntiKt4Jet/TMath::Sqrt(x1);
	    double mod_mass_had = LQ_M_had_VisTauAntiKt4Jet/TMath::Sqrt(x2);
#define BJet
//#define BQuark
#ifdef BJet
	    h_lep->Fill(LQ_M_lep_directLepAntiKt4Jet);
	    h_lepAfter->Fill(mod_mass_lep);
	    h_had->Fill(LQ_M_had_VisTauAntiKt4Jet);
	    h_hadAfter->Fill(mod_mass_had);
#endif
#ifdef BQuark
            h_lep     ->Fill( (*TLV_Lepton + *TLV_LepBQuark).M()/1000. );
            h_lepAfter->Fill( (*TLV_Lepton + *TLV_LepBQuark).M()/1000./TMath::Sqrt(x1) );
            h_had     ->Fill( (*TLV_TauJet + *TLV_HadBQuark).M()/1000. );
            h_hadAfter->Fill( (*TLV_TauJet + *TLV_HadBQuark).M()/1000./TMath::Sqrt(x2) );
#endif
        }
	
        /* Invariant mass */
        const double YRange = 0.3;
        // Lep side
	C_LQLep->cd();
	h_lep     ->SetLineColor( color[iMass] );   
	h_lepAfter->SetLineColor( color[iMass] );  
        h_lepAfter->SetFillColor( color[iMass] );  
        h_lepAfter->SetFillStyle(3335);;
        h_lep     ->Scale(1/h_lep     ->Integral());
	h_lepAfter->Scale(1/h_lepAfter->Integral());
        h_lep     ->GetYaxis()->SetRangeUser(0,YRange);
        h_lep     ->Draw("same hist");
	h_lepAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_lepAfter->Draw("same hist");
        ATLASLabel(0.32, 0.85, "Simulation Work in progress");
        TLatex* lt = new TLatex(0.45, 0.80, "Truth info.");
        lt->SetNDC();
        lt->SetTextSize(0.04);
        lt->Draw();
        TLegend* leg = new TLegend(0.45, 0.80, 0.5, 0.9);
        leg->AddEntry(h_lep, Form("%s GeV", mass_point[iMass].c_str()),"l");
        //leg->Draw();
        C_LQLep->RedrawAxis();
	
        // Had side
        C_LQHad->cd();
	h_had     ->SetLineColor( color[iMass] );   
	h_hadAfter->SetFillColor( color[iMass] );  
	h_hadAfter->SetLineColor( color[iMass] );  
	h_hadAfter->SetFillStyle(3335);;
	h_hadAfter->Scale(1/h_hadAfter->Integral());
	h_had     ->Scale(1/h_had->Integral());
	h_had     ->GetYaxis()->SetRangeUser(0,YRange);
	h_had     ->Draw("same hist");
	h_hadAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_hadAfter->Draw("same hist");
        ATLASLabel(0.32, 0.85, "Simulation Work in progress");
        lt->Draw();
    }
    
    /* Basic dist */
    C_LQLep       ->SaveAs("C_detail.pdf(");
    C_LQHad       ->SaveAs("C_detail.pdf)");
}
