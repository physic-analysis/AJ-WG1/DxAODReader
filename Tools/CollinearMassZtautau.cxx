
void CollinearMassZtautau()
{
    std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedZtautau/build/test.root";
    std::cout << file_name << std::endl;
    TFile* file = new TFile( file_name.c_str() );
    TTree* tree = (TTree*)file->Get("m_true");

    bool   isMatched             ; 
    bool   isZTauHad             ; 
    bool   isZAntiTauHad         ; 
    double Zmass                 ; 
    double ZmassTrue             ; 
    double Tau_Jet_P             ; 
    double Tau_Jet_theta         ; 
    double Tau_Jet_phi           ; 
    double Tau_Lepton_P          ; 
    double Tau_Lepton_theta      ; 
    double Tau_Lepton_phi        ; 
    double AntiTau_Jet_P         ; 
    double AntiTau_Jet_theta     ; 
    double AntiTau_Jet_phi       ; 
    double AntiTau_Lepton_P      ; 
    double AntiTau_Lepton_theta  ; 
    double AntiTau_Lepton_phi    ; 
    double MET_x                 ; 
    double MET_y                 ; 
      
    tree->SetBranchAddress("isMatched",              &isMatched              );    
    tree->SetBranchAddress("isZTauHad",              &isZTauHad              );    
    tree->SetBranchAddress("isZAntiTauHad",          &isZAntiTauHad          );    
    tree->SetBranchAddress("Zmass",                  &Zmass                  );    
    tree->SetBranchAddress("ZmassTrue",              &ZmassTrue              );    
    tree->SetBranchAddress("Tau_Jet_P",              &Tau_Jet_P              );    
    tree->SetBranchAddress("Tau_Jet_theta",          &Tau_Jet_theta          );    
    tree->SetBranchAddress("Tau_Jet_phi",            &Tau_Jet_phi            );    
    tree->SetBranchAddress("Tau_Lepton_P",           &Tau_Lepton_P           );    
    tree->SetBranchAddress("Tau_Lepton_theta",       &Tau_Lepton_theta       );    
    tree->SetBranchAddress("Tau_Lepton_phi",         &Tau_Lepton_phi         );   
    tree->SetBranchAddress("AntiTau_Jet_P",          &AntiTau_Jet_P          );    
    tree->SetBranchAddress("AntiTau_Jet_theta",      &AntiTau_Jet_theta      );    
    tree->SetBranchAddress("AntiTau_Jet_phi",        &AntiTau_Jet_phi        );    
    tree->SetBranchAddress("AntiTau_Lepton_P",       &AntiTau_Lepton_P       );    
    tree->SetBranchAddress("AntiTau_Lepton_theta",   &AntiTau_Lepton_theta   ); 
    tree->SetBranchAddress("AntiTau_Lepton_phi",     &AntiTau_Lepton_phi     );   
    tree->SetBranchAddress("MET_x",                  &MET_x                  );   
    tree->SetBranchAddress("MET_y",                  &MET_y                  );   

    TH1D* h_tmp      = new TH1D("h_tmp","h_tmp"          , 50, 0, 200);
    TH1D* h_tmpAfter = new TH1D("h_tmpAfter","h_tmpAfter", 50, 0, 200);
    for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
	tree->GetEntry(iEntry);

	if ( !isMatched ) continue;
//	if ( MET_x*MET_x + MET_y*MET_y < 100 ) continue;
	//if ( !isZTauHad || !isZAntiTauHad ) continue;

	// Collinear approximation
	// 1 : tau
	// 2 : anti-tau

	double Vis1_theta=0;
	double Vis1_phi=0;
	double Vis1_P=0;
	double Vis2_theta=0;
	double Vis2_phi=0;
	double Vis2_P=0;
	if       ( isZTauHad && isZAntiTauHad ) {
	    Vis1_theta =      Tau_Jet_theta;
	    Vis1_phi   =      Tau_Jet_phi;
	    Vis1_P     =      Tau_Jet_P;
	    Vis2_theta =  AntiTau_Jet_theta;
	    Vis2_phi   =  AntiTau_Jet_phi;
	    Vis2_P     =  AntiTau_Jet_P;
	}else if (  isZTauHad && !isZAntiTauHad ) {
	    Vis1_theta =      Tau_Jet_theta;
	    Vis1_phi   =      Tau_Jet_phi;
	    Vis1_P     =      Tau_Jet_P;
	    Vis2_theta =  AntiTau_Lepton_theta;
	    Vis2_phi   =  AntiTau_Lepton_phi;
	    Vis2_P     =  AntiTau_Lepton_P;
	}else if ( !isZTauHad &&  isZAntiTauHad ) {
	    Vis1_theta =      Tau_Lepton_theta;
	    Vis1_phi   =      Tau_Lepton_phi;
	    Vis1_P     =      Tau_Lepton_P;
	    Vis2_theta =  AntiTau_Jet_theta;
	    Vis2_phi   =  AntiTau_Jet_phi;
	    Vis2_P     =  AntiTau_Jet_P;
	}else if ( !isZTauHad && !isZAntiTauHad ) {
	    Vis1_theta =      Tau_Lepton_theta;
	    Vis1_phi   =      Tau_Lepton_phi;
	    Vis1_P     =      Tau_Lepton_P;
	    Vis2_theta =  AntiTau_Lepton_theta;
	    Vis2_phi   =  AntiTau_Lepton_phi;
	    Vis2_P     =  AntiTau_Lepton_P;
	}

//	if ( abs(Vis1_P) < 10 || abs(Vis2_P) < 10 ) continue;

	double  p_miss1 = (MET_x*TMath::Sin(Vis2_phi) - MET_y*TMath::Cos(Vis2_phi))/(TMath::Sin(Vis1_theta)*TMath::Sin(Vis2_phi - Vis1_phi));
	double  p_miss2 = (MET_x*TMath::Sin(Vis1_phi) - MET_y*TMath::Cos(Vis1_phi))/(TMath::Sin(Vis2_theta)*TMath::Sin(Vis1_phi - Vis2_phi));
	double  x1      = Vis1_P/(Vis1_P +  p_miss1);
	double  x2      = Vis2_P/(Vis2_P +  p_miss2);

	double mod_mass = Zmass/TMath::Sqrt(x1*x2);
	//std::cout << MET_x << " " << MET_y << "Vis1_P=" << Vis1_P << " pmiss1=" << p_miss1 << " Vis2_P=" << Vis2_P << " pmiss2=" << p_miss2 << " " << Vis1_phi << " " << Vis2_phi << std::endl;
	h_tmp->Fill(Zmass);
	h_tmpAfter->Fill(mod_mass);
    }
    h_tmp->Scale(1/h_tmp->Integral());
    h_tmpAfter->Scale(1/h_tmpAfter->Integral());
    h_tmp->Draw("same hist");
    h_tmpAfter->SetFillColor(kCyan-9);
    h_tmpAfter->SetFillStyle(3365);
    h_tmpAfter->Draw("same hist");
    h_tmp->Draw("same hist");

}
