
void Draw()
{

  const int MassPoint = 3;
  TTree* tree[MassPoint];
  std::string mass_point[5] = {"300","500","900","1300","1700"};
//  std::string mass_point[MassPoint] = {"300","1300","1700"};

  TFile* file = new TFile("test.root", "READ");
  
  std::vector<std::string> obj = {
    "LQ_M_lep",
    "LQ_M_lep_direct",
    "LQ_M_lep_directLepJet",
    "LQ_M_lep_directEleJet",
    "LQ_M_lep_directMuJet",
    "LQ_M_had",
    "LQ_M_had_direct",
    "LQ_M_had_TauJet",
    "LQ_M_had_VisTauJet",
    "dR_HadBTau", 
    "dR_HadBLep", 
    "dR_HadBEle", 
    "dR_HadBMu", 
    "dR_LepBTau", 
    "dR_LepBLep", 
    "dR_LepBEle", 
    "dR_LepBMu", 
    "dR_TauLep", 
    "dR_TauEle", 
    "dR_TauMu", 
    "dR_LepBHadB", 
    "dR_LQLQ",
    "dPhi_HadBTau", 
    "dPhi_HadBLep", 
    "dPhi_HadBEle", 
    "dPhi_HadBMu", 
    "dPhi_LepBTau", 
    "dPhi_LepBLep", 
    "dPhi_LepBEle", 
    "dPhi_LepBMu", 
    "dPhi_TauLep", 
    "dPhi_TauEle", 
    "dPhi_TauMu", 
    "dPhi_LepBHadB ", 
    "dPhi_LQLQ",
    "dEta_HadBTau", 
    "dEta_HadBLep", 
    "dEta_HadBEle", 
    "dEta_HadBMu", 
    "dEta_LepBTau", 
    "dEta_LepBLep", 
    "dEta_LepBEle", 
    "dEta_LepBMu", 
    "dEta_TauLep", 
    "dEta_TauEle", 
    "dEta_TauMu", 
    "dEta_LepBHadB", 
    "dEta_LQLQ",
    "BottomHad_Pt",  
    "BottomHad_Eta",  
    "BottomHad_Phi",  
    "BottomLep_Pt",  
    "BottomLep_Eta",  
    "BottomLep_Phi",  
    "LQLep_Pt", 
    "LQLep_Eta",
    "LQLep_Phi",
    "LQHad_Pt", 
    "LQHad_Eta",
    "LQHad_Phi",
    "VisTau_Pt", 
    "VisTau_Eta",  
    "VisTau_Phi",  
    "Electron_Pt",  
    "Electron_Eta", 
    "Electron_Phi", 
    "Muon_Pt", 
    "Muon_Eta", 
    "Muon_Phi", 
  };
  
  std::vector<std::string> description = {
    "LQ invariant mass", // "LQ_M_lep",
    "LQ invariant mass", // "LQ_M_lep_direct",
    "LQ invariant mass (lepton+jet)", // "LQ_M_lep_directLepJet",
    "LQ invariant mass (ele+jet)", // "LQ_M_lep_directEleJet",
    "LQ invariant mass (mu+jet)", // "LQ_M_lep_directMuJet",
    "LQ invariant mass", // "LQ_M_had",
    "LQ invariant mass", // "LQ_M_had_direct",
    "LQ invariant mass (tau+jet)", // "LQ_M_had_TauJet",
    "LQ invariant mass (vistau+jet)", // "LQ_M_had_VisTauJet",
    "#Delta R (B_{had},#tau)", // "dR_HadBTau", 
    "#Delta R (B_{had},#ell)", // "dR_HadBLep", 
    "#Delta R (B_{had},#ell)", // "dR_HadBEle", 
    "#Delta R (B_{had},#ell)", // "dR_HadBMu", 
    "#Delta R (B_{lep},#tau)", // "dR_LepBTau", 
    "#Delta R (B_{lep},#ell)", // "dR_LepBLep", 
    "#Delta R (B_{lep},ele)", // "dR_LepBEle", 
    "#Delta R (B_{lep},#mu)", // "dR_LepBMu", 
    "#Delta R (#tau,#ell)", // "dR_TauLep", 
    "#Delta R (#tau,ele)", // "dR_TauEle", 
    "#Delta R (#tau,#mu)", // "dR_TauMu", 
    "#Delta R (B_{lep},B_{had})", // "dR_LepBHadB", 
    "#Delta R (LQ,LQ)", // "dR_LQLQ",
    "#Delta #phi (LQ,LQ)", // "dPhi_HadBTau", 
    "#Delta #phi (B_{had},#ell)", // "dR_HadBLep", 
    "#Delta #phi (B_{had},#ell)", // "dR_HadBEle", 
    "#Delta #phi (B_{had},#ell)", // "dR_HadBMu", 
    "#Delta #phi (B_{lep},#tau)", // "dR_LepBTau", 
    "#Delta #phi (B_{lep},#ell)", // "dR_LepBLep", 
    "#Delta #phi (B_{lep},ele)", // "dR_LepBEle", 
    "#Delta #phi (B_{lep},#mu)", // "dR_LepBMu", 
    "#Delta #phi (#tau,#ell)", // "dR_TauLep", 
    "#Delta #phi (#tau,ele)", // "dR_TauEle", 
    "#Delta #phi (#tau,#mu)", // "dR_TauMu", 
    "#Delta #phi (B_{lep},B_{had})", // "dR_LepBHadB", 
    "#Delta #phi (LQ,LQ)", // "dR_LQLQ",
    "#Delta #eta (LQ,LQ)", // "dPhi_HadBTau", 
    "#Delta #eta (B_{had},#ell)", // "dR_HadBLep", 
    "#Delta #eta (B_{had},#ell)", // "dR_HadBEle", 
    "#Delta #eta (B_{had},#ell)", // "dR_HadBMu", 
    "#Delta #eta (B_{lep},#tau)", // "dR_LepBTau", 
    "#Delta #eta (B_{lep},#ell)", // "dR_LepBLep", 
    "#Delta #eta (B_{lep},ele)", // "dR_LepBEle", 
    "#Delta #eta (B_{lep},#mu)", // "dR_LepBMu", 
    "#Delta #eta (#tau,#ell)", // "dR_TauLep", 
    "#Delta #eta (#tau,ele)", // "dR_TauEle", 
    "#Delta #eta (#tau,#mu)", // "dR_TauMu", 
    "#Delta #eta (B_{lep},B_{had})", // "dR_LepBHadB", 
    "#Delta #eta (LQ,LQ)", // "dR_LQLQ",
    "", //"BottomHad_Pt",  
    "", //"BottomHad_Eta",  
    "", //"BottomHad_Phi",  
    "", //"BottomLep_Pt",  
    "", //"BottomLep_Eta",  
    "", //"BottomLep_Phi",  
    "", //"LQLep_Pt", 
    "", //"LQLep_Eta",
    "", //"LQLep_Phi",
    "", //"LQHad_Pt", 
    "", //"LQHad_Eta",
    "", //"LQHad_Phi",
    "", //"VisTau_Pt", 
    "", //"VisTau_Eta",  
    "", //"VisTau_Phi",  
    "", //"Electron_Pt",  
    "", //"Electron_Eta", 
    "", //"Electron_Phi", 
    "", //"Muon_Pt", 
    "", //"Muon_Eta", 
    "", //"Muon_Phi", 
  };
  
  TCanvas* c1 = new TCanvas("c1", "c1");
  c1->SaveAs("merged.pdf[");
  for ( int iPage=0; iPage<18; iPage++) {
        std::cout << iPage << std::endl;
    c1->Clear();
    c1->Divide(2,2);

    for ( int iPad = 1; iPad<=4; iPad++){
      if( iPad-1+4*iPage > 69 ) break;
      c1->cd(iPad);
      TText* t = new TText(0.7,0.85, description[iPad-1 + 4*iPage].c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(42);
      t->SetTextSize(0.05);
      t->SetTextAngle(0);
      for ( int iMass=0; iMass<5; iMass++){
        std::cout << iMass << std::endl;
        std::string hist_name = obj[ iPad-1 + 4*iPage] + "_M" + mass_point[iMass];
        TH1D* htemp = (TH1D*)file->Get( hist_name.c_str() );
        if ( iMass == 0 ) htemp->SetLineColor(kBlack);
        if ( iMass == 1 ) htemp->SetLineColor(kMagenta);
        if ( iMass == 2 ) htemp->SetLineColor(kBlue);
        if ( iMass == 3 ) htemp->SetLineColor(kGreen);
        if ( iMass == 4 ) htemp->SetLineColor(kRed);
        
        if ( iMass == 0 ) { htemp->Scale(1/htemp->Integral());htemp->GetYaxis()->SetRangeUser(0,htemp->GetBinContent(htemp->GetMaximumBin())+0.1);htemp->Draw("hist");}
        else              { htemp->Scale(1/htemp->Integral());htemp->Draw("same hist");}
        
      }
      t->Draw();
    }
    c1->SaveAs("merged.pdf");
  }
  c1->SaveAs("merged.pdf]");

//  for ( int iMass=0; iMass<MassPoint; iMass++){
//    std::string file_name = "test_M" + mass_point[iMass] + ".pdf";
//    std::string file_name_begin = file_name + "[";
//    std::string file_name_end   = file_name + "]";
//    TCanvas* c = new TCanvas("c", "c");
//    c->SaveAs(file_name_begin.c_str());
//    for ( int iPage=0; iPage<17; iPage++) {
//      c->Clear();
//      c->Divide(2,2);
//
//      for ( int iPad = 1; iPad<=4; iPad++){
//        if( iPad-1+4*iPage > 64 ) break;
//        c->cd(iPad);
//        std::string hist_name = obj[ iPad-1 + 4*iPage] + "_M" + mass_point[iMass];
//        TH1D* htemp = (TH1D*)file->Get( hist_name.c_str() );
//        htemp->SetLineColor(kBlack);
//        htemp->Scale(1/htemp->Integral());htemp->Draw("hist");
//      }
//      c->SaveAs(file_name.c_str());
//    }
//    c->SaveAs(file_name_end.c_str());
//  }

}
