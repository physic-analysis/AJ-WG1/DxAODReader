
void CoutVarNames()
{
    std::vector<TString> Object =
    {
	/* had-side */
	"HadB",     // truthDirectBJetHad,
	"HadTau",   // truthDirectTauHad,
	"HadTauNeu",// truthDirectHadTauNeu,

	/* lep-side */
	"LepB",     // truthDirectBJetLep,
	"LepTau",   // truthDirectTauLep,
	"LepTauNeu",// truthDirectLepTauNeu,
	"LepLepNeu",// truthDirectLepLepNeu,
	"LepEleNeu",// truthDirectLepEleNeu,
	"LepMuNeu", // truthDirectLepMuNeu,
	"LepLep",   // truthLepton,
	"LepEle",   // truthElectron,
	"LepMu",    // truthMuon,
    };

    int iObjectIndex = 0;
    for ( int iObj=0; iObj<Object.size(); iObj++){
	for ( int jObj=iObj+1; jObj<Object.size(); jObj++){
	    std::cout <<  "dR_"   + Object[iObj]+Object[jObj] << std::endl;
	    iObjectIndex++;
	}
    }
    iObjectIndex = 0;
    for ( int iObj=0; iObj<Object.size(); iObj++){
	for ( int jObj=iObj+1; jObj<Object.size(); jObj++){
	    std::cout <<  "dPhi_"   + Object[iObj]+Object[jObj] << std::endl;
	    iObjectIndex++;
	}
    }
    iObjectIndex = 0;
    for ( int iObj=0; iObj<Object.size(); iObj++){
	for ( int jObj=iObj+1; jObj<Object.size(); jObj++){
	    std::cout <<  "dEta_"   + Object[iObj]+Object[jObj] << std::endl;
	    iObjectIndex++;
	}
    }

}
