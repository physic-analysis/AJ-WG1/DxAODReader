
void DetailPlots()
{
    TCanvas * C_LQLep            = new TCanvas("C_LQLep","C_LQLep");
    TCanvas * C_LQLepPairing     = new TCanvas("C_LQLepPairing","C_LQLepPairing");
    TCanvas * C_LQHadPairing     = new TCanvas("C_LQHadPairing","C_LQHadPairing");
    TCanvas * C_LQHad            = new TCanvas("C_LQHad","C_LQHad");
    
    TCanvas * C_LQLepReco        = new TCanvas("C_LQLepReco","C_LQLepReco");
    TCanvas * C_LQHadReco        = new TCanvas("C_LQHadReco","C_LQHadReco");
    TCanvas * C_LQLepRecoPairing = new TCanvas("C_LQLepRecoPairing","C_LQLepRecoPairing");
    TCanvas * C_LQHadRecoPairing = new TCanvas("C_LQHadRecoPairing","C_LQHadRecoPairing");
 
    /* Basic dist */
    TCanvas * C_CALepTauMass = new TCanvas("C_CALepTauMass","C_CALepTauMass"); 
    TCanvas * C_CAHadTauMass = new TCanvas("C_CAHadTauMass","C_CAHadTauMass"); 
    
    TCanvas * C_METTruth     = new TCanvas("C_METTruth","C_METTruth", 1200, 600); C_METTruth->Divide(3,2); 
   
    TCanvas * C_LQLepCollinearMass_LeptonPt    = new TCanvas("C_LQLepCollinearMass_LeptonPt"    ,"C_LQLepCollinearMass_LeptonPt"  , 1200, 600);    C_LQLepCollinearMass_LeptonPt ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_Pmis1       = new TCanvas("C_LQLepCollinearMass_Pmis1"    ,"C_LQLepCollinearMass_Pmis1"  , 1200, 600);    C_LQLepCollinearMass_Pmis1 ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_MET         = new TCanvas("C_LQLepCollinearMass_MET"         ,"C_LQLepCollinearMass_MET"  , 1200, 600);         C_LQLepCollinearMass_MET ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_MET_x       = new TCanvas("C_LQLepCollinearMass_MET_x"       ,"C_LQLepCollinearMass_MET_x"  , 1200, 600);       C_LQLepCollinearMass_MET_x ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_MET_y       = new TCanvas("C_LQLepCollinearMass_MET_y"       ,"C_LQLepCollinearMass_MET_y"  , 1200, 600);       C_LQLepCollinearMass_MET_y ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_TauJetPt    = new TCanvas("C_LQLepCollinearMass_TauJetPt"    ,"C_LQLepCollinearMass_TauJetPt"  , 1200, 600);    C_LQLepCollinearMass_TauJetPt ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_TauJetPhi   = new TCanvas("C_LQLepCollinearMass_TauJetPhi"   ,"C_LQLepCollinearMass_TauJetPhi"  , 1200, 600);   C_LQLepCollinearMass_TauJetPhi ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_LeptonPhi   = new TCanvas("C_LQLepCollinearMass_LeptonPhi"   ,"C_LQLepCollinearMass_LeptonPhi"  , 1200, 600);   C_LQLepCollinearMass_LeptonPhi ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_LeptonTheta = new TCanvas("C_LQLepCollinearMass_LeptonTheta" ,"C_LQLepCollinearMass_LeptonTheta"  , 1200, 600); C_LQLepCollinearMass_LeptonTheta ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_LQLepVisibleMass = new TCanvas("C_LQLepCollinearMass_LQLepVisibleMass" ,"C_LQLepCollinearMass_LQLepVisibleMass"  , 1200, 600); C_LQLepCollinearMass_LQLepVisibleMass ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_TauJetPhiLeptonPhi = new TCanvas("C_LQLepCollinearMass_TauJetPhiLeptonPhi" ,"C_LQLepCollinearMass_TauJetPhiLeptonPhi"  , 1200, 600); C_LQLepCollinearMass_TauJetPhiLeptonPhi ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_LQHadCollinearMass = new TCanvas("C_LQLepCollinearMass_LQHadCollinearMass" ,"C_LQLepCollinearMass_LQHadCollinearMass"  , 1200, 600); C_LQLepCollinearMass_LQHadCollinearMass ->Divide(3,2);

    TCanvas * C_LQLepCollinearMass_MET_z = new TCanvas("C_LQLepCollinearMass_MET_z" ,"C_LQLepCollinearMass_MET_z"  , 1200, 600); C_LQLepCollinearMass_MET_z ->Divide(3,2);
    
    TCanvas * C_LQLepCollinearMass_dRLepTauLepTauNeu = new TCanvas("C_LQLepCollinearMass_dRLepTauLepTauNeu" ,"C_LQLepCollinearMass_dRLepTauLepTauNeu"  , 1200, 600); C_LQLepCollinearMass_dRLepTauLepTauNeu ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_dRLepTauLepLepNeu = new TCanvas("C_LQLepCollinearMass_dRLepTauLepLepNeu" ,"C_LQLepCollinearMass_dRLepTauLepLepNeu"  , 1200, 600); C_LQLepCollinearMass_dRLepTauLepLepNeu ->Divide(3,2);
    TCanvas * C_LQLepCollinearMass_dRLQLQ = new TCanvas("C_LQLepCollinearMass_dRLQLQ" ,"C_LQLepCollinearMass_dRLQLQ"  , 1200, 600); C_LQLepCollinearMass_dRLQLQ ->Divide(3,2);

    TCanvas * C_dRMetLep_dRMetTauJet           = new TCanvas("C_dRMetLep_dRMetTauJet"           ,"C_dRMetLep_dRMetTauJet"  , 1200, 600);           C_dRMetLep_dRMetTauJet ->Divide(3,2);

    TCanvas * C_LQLepVisibleMass_x1            = new TCanvas("C_LQLepVisibleMass_x1"            ,"C_LQLepVisibleMass_x1"  , 1200, 600);            C_LQLepVisibleMass_x1 ->Divide(3,2);
    TCanvas * C_LeptonPt_x1   	               = new TCanvas("C_LeptonPt_x1"                    ,"C_LeptonPt_x1"    , 1200, 600); C_LeptonPt_x1    ->Divide(3,2);
    TCanvas * C_TauJetPt_x1   	               = new TCanvas("C_TauJetPt_x1"                    ,"C_TauJetPt_x1"    , 1200, 600); C_TauJetPt_x1    ->Divide(3,2);
    TCanvas * C_LeptonTheta_x1   	       = new TCanvas("C_LeptonTheta_x1"                 ,"C_LeptonTheta_x1" , 1200, 600); C_LeptonTheta_x1 ->Divide(3,2);
    TCanvas * C_LeptonPhi_x1     	       = new TCanvas("C_LeptonPhi_x1"                   ,"C_LeptonPhi_x1"   , 1200, 600); C_LeptonPhi_x1   ->Divide(3,2);
    TCanvas * C_TauJetTheta_x1   	       = new TCanvas("C_TauJetTheta_x1"                 ,"C_TauJetTheta_x1" , 1200, 600); C_TauJetTheta_x1 ->Divide(3,2);
    TCanvas * C_TauJetPhi_x1     	       = new TCanvas("C_TauJetPhi_x1"                   ,"C_TauJetPhi_x1"   , 1200, 600); C_TauJetPhi_x1   ->Divide(3,2);
    TCanvas * C_TauJetPhiLeptonPhi_x1          = new TCanvas("C_TauJetPhiLeptonPhi_x1"                   ,"C_TauJetPhiLeptonPhi_x1"   , 1200, 600); C_TauJetPhiLeptonPhi_x1   ->Divide(3,2);
    TCanvas * C_Pmis_x1          = new TCanvas("C_Pmis_x1"                   ,"C_Pmis_x1"   , 1200, 600); C_Pmis_x1   ->Divide(3,2);
    
    TCanvas * C_dPhiLepMet_dPhiTauJetMet          = new TCanvas("C_dPhiLepMet_dPhiTauJetMet"                   ,"C_dPhiLepMet_dPhiTauJetMet"   , 1200, 600); C_dPhiLepMet_dPhiTauJetMet   ->Divide(3,2);
    
    TCanvas * C_LeptonP_Pmis   	               = new TCanvas("C_LeptonP_Pmis"                    ,"C_LeptonP_Pmis"    , 1200, 600); C_LeptonP_Pmis    ->Divide(3,2);
    
    TCanvas * C_LQHadCollinearMass_LeptonPt = new TCanvas("C_LQHadCollinearMass_LeptonPt"  ,"C_LQHadCollinearMass_LeptonPt"  , 1200, 600);  C_LQHadCollinearMass_LeptonPt ->Divide(3,2);
    TCanvas * C_LQHadCollinearMass_MET      = new TCanvas("C_LQHadCollinearMass_MET"  ,"C_LQHadCollinearMass_MET"  , 1200, 600);  C_LQHadCollinearMass_MET ->Divide(3,2);
    TCanvas * C_LQHadCollinearMass_MET_x    = new TCanvas("C_LQHadCollinearMass_MET_x"  ,"C_LQHadCollinearMass_MET_x"  , 1200, 600);  C_LQHadCollinearMass_MET_x ->Divide(3,2);
    TCanvas * C_LQHadCollinearMass_MET_y    = new TCanvas("C_LQHadCollinearMass_MET_y"  ,"C_LQHadCollinearMass_MET_y"  , 1200, 600);  C_LQHadCollinearMass_MET_y ->Divide(3,2);
    TCanvas * C_LQHadCollinearMass_TauJetPt = new TCanvas("C_LQHadCollinearMass_TauJetPt"  ,"C_LQHadCollinearMass_TauJetPt"  , 1200, 600);  C_LQHadCollinearMass_TauJetPt ->Divide(3,2);
    
    TCanvas * C_x1_dPhiTauJetLepton = new TCanvas("C_x1_dPhiTauJetLepton"  ,"C_x1_dPhiTauJetLepton" , 1200, 600); C_x1_dPhiTauJetLepton ->Divide(3,2);
   
    TCanvas * C_x1_lepSide   = new TCanvas("C_x1_lepSide","C_x1_lepSide");    
    TCanvas * C_x2_lepSide   = new TCanvas("C_x2_lepSide","C_x2_lepSide");    
    
    TCanvas * C_Pmis1_TruthLepNeu  = new TCanvas("C_Pmis1_TruthLepNeu" ,"C_Pmis1_TruthLepNeu" , 1200, 600); C_Pmis1_TruthLepNeu ->Divide(3,2);
    TCanvas * C_Pmis1_DeltaRLepNeu = new TCanvas("C_Pmis1_DeltaRLepNeu","C_Pmis1_DeltaRLepNeu", 1200, 600); C_Pmis1_DeltaRLepNeu->Divide(3,2);
    
    TCanvas * C_Pmis2_TruthTauNeu  = new TCanvas("C_Pmis2_TruthTauNeu","C_Pmis2_TruthTauNeu", 1200, 600); C_Pmis2_TruthTauNeu->Divide(3,2);    
    TCanvas * C_Pmis2_DeltaRTauNeu = new TCanvas("C_Pmis2_DeltaRTauNeu","C_Pmis2_DeltaRTauNeu", 1200, 600); C_Pmis2_DeltaRTauNeu->Divide(3,2);

   std::vector<std::string> mass_point = {"300", "500", "900", "1300", "1700"};
//   std::vector<std::string> mass_point = {"1700"};
    for ( int iMass=0; iMass< mass_point.size(); iMass++){
	std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/MyNtuple.M" + mass_point[iMass] + ".root";
//	std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/test.root";
	std::cout << file_name << std::endl;
	TFile* file = new TFile( file_name.c_str() );
	TTree* tree = (TTree*)file->Get("m_true");
	
        bool EventFlag;
        bool isLeptonEle;
        tree->SetBranchAddress("EventFlag"   , &EventFlag );
        tree->SetBranchAddress("isLeptonEle" , &isLeptonEle );

	double LQ_M_lep_directLepJet;
	double LQ_M_lep_directLepAntiKt4Jet;
	double LQ_M_had_VisTauJet;
	double LQ_M_had_VisTauAntiKt4Jet;
	double Lep_P   ;
	double Lep_theta   ;
	double Lep_phi     ;
	double Ele_theta   ;
	double Ele_phi     ;
	double Mu_theta    ;
	double Mu_phi      ;
	double TauJet_P;
	double TauJet_theta;
	double TauJet_phi  ;
	double MET_x       ;
	double MET_y       ;
      //{"dR_LepLepNeuLepLep",    "#Delta R #left(lepton, #nu_{lep} #right)" },
      //{"dR_LepTauNeuLepLep",    "#Delta R #left(lepton, #nu_{#tau} #right)" },
      //{"dR_LepTauLepLep",       "#Delta R #left(lepton, #tau_{lep} #right)" },
      //{"dR_LepTauNeuLepLepNeu", "#Delta R #left(#nu_{lep}, #nu_{#tau} #right)" },
      //{"dR_LepTauLepLepNeu",    "#Delta R #left(#nu_{lep}, #tau_{lep} #right)" },
      //{"dR_LepTauLepTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{lep} #right)" },
      //{"dR_HadTauHadTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{had} #right)" },
       double dR_LepTauLepLepNeu;
       double dR_LepTauLepTauNeu;
       double dR_LQLQ;

	TLorentzVector* TLV_Lepton    = nullptr;
	TLorentzVector* TLV_Electron  = nullptr;
	TLorentzVector* TLV_Muon      = nullptr;
	TLorentzVector* TLV_TauJet    = nullptr;
	TLorentzVector* TLV_LepNeu    = nullptr;
	TLorentzVector* TLV_EleNeu    = nullptr;
	TLorentzVector* TLV_MuNeu     = nullptr;
	TLorentzVector* TLV_HadTauNeu = nullptr;
	TLorentzVector* TLV_LepTauNeu = nullptr;
	TLorentzVector* TLV_HadBJet   = nullptr;
	TLorentzVector* TLV_LepBJet   = nullptr;
	TLorentzVector* TLV_HadBQuark = nullptr;
	TLorentzVector* TLV_LepBQuark = nullptr;
	tree->SetBranchAddress("TLV_Lepton"    , &TLV_Lepton   );
	tree->SetBranchAddress("TLV_Electron"  , &TLV_Electron );
	tree->SetBranchAddress("TLV_Muon"      , &TLV_Muon     );
	tree->SetBranchAddress("TLV_TauJet"    , &TLV_TauJet   );
	tree->SetBranchAddress("TLV_LepNeu"    , &TLV_LepNeu   );
	tree->SetBranchAddress("TLV_EleNeu"    , &TLV_EleNeu   );
	tree->SetBranchAddress("TLV_MuNeu"     , &TLV_MuNeu    );
	tree->SetBranchAddress("TLV_HadTauNeu" , &TLV_HadTauNeu);
	tree->SetBranchAddress("TLV_LepTauNeu" , &TLV_LepTauNeu);
	tree->SetBranchAddress("TLV_HadBJet"   , &TLV_HadBJet  );
	tree->SetBranchAddress("TLV_LepBJet"   , &TLV_LepBJet  );
	tree->SetBranchAddress("TLV_HadBQuark" , &TLV_HadBQuark  );
	tree->SetBranchAddress("TLV_LepBQuark" , &TLV_LepBQuark  );

	// Collinear mass
	tree->SetBranchAddress("LQ_M_lep_directLepJet" , &LQ_M_lep_directLepJet   );
	tree->SetBranchAddress("LQ_M_had_VisTauJet"    , &LQ_M_had_VisTauJet   );
	tree->SetBranchAddress("LQ_M_lep_directLepAntiKt4Jet" , &LQ_M_lep_directLepAntiKt4Jet   );
	tree->SetBranchAddress("LQ_M_had_VisTauAntiKt4Jet"    , &LQ_M_had_VisTauAntiKt4Jet   );
	tree->SetBranchAddress("Lep_P"                 , &Lep_P       );
	tree->SetBranchAddress("Lep_theta"             , &Lep_theta   );
	tree->SetBranchAddress("Lep_phi"               , &Lep_phi     );
	tree->SetBranchAddress("Ele_theta"             , &Ele_theta   );
	tree->SetBranchAddress("Ele_phi"               , &Ele_phi     );
	tree->SetBranchAddress("Mu_theta"              , &Mu_theta    );
	tree->SetBranchAddress("Mu_phi"                , &Mu_phi      );
	tree->SetBranchAddress("TauJet_P"          , &TauJet_P);
	tree->SetBranchAddress("TauJet_theta"          , &TauJet_theta);
	tree->SetBranchAddress("TauJet_phi"            , &TauJet_phi  );
	tree->SetBranchAddress("MET_x"                 , &MET_x       );
	tree->SetBranchAddress("MET_y"                 , &MET_y       );
	
	tree->SetBranchAddress("dR_LepTauLepLepNeu"                 , &dR_LepTauLepLepNeu       );
	tree->SetBranchAddress("dR_LepTauLepTauNeu"                 , &dR_LepTauLepTauNeu       );
	tree->SetBranchAddress("dR_LQLQ"      , &dR_LQLQ       );
	
  
        // Reco object information 
        std::vector<double>* Reco_Electron_Pt=0;         
        std::vector<double>* Reco_Electron_Eta=0;        
        std::vector<double>* Reco_Electron_Phi=0;        
        std::vector<double>* Reco_Electron_M=0;          
        std::vector<double>* Reco_Electron_truthPdgId=0; 
        std::vector<double>* Reco_Electron_charge=0;     
        std::vector<double>* Reco_Electron_dRTruthEle=0; 
        tree->SetBranchAddress("Reco_Electron_Pt"         , &Reco_Electron_Pt   );
        tree->SetBranchAddress("Reco_Electron_Eta"        , &Reco_Electron_Eta  );
        tree->SetBranchAddress("Reco_Electron_Phi"        , &Reco_Electron_Phi  );
        tree->SetBranchAddress("Reco_Electron_M"          , &Reco_Electron_M    );
        tree->SetBranchAddress("Reco_Electron_truthPdgId" , &Reco_Electron_truthPdgId);
        tree->SetBranchAddress("Reco_Electron_charge"     , &Reco_Electron_charge);
        tree->SetBranchAddress("Reco_Electron_dRTruthEle" , &Reco_Electron_dRTruthEle);

        std::vector<double>* Reco_Muon_Pt=0;             
        std::vector<double>* Reco_Muon_Eta=0;            
        std::vector<double>* Reco_Muon_Phi=0;            
        std::vector<double>* Reco_Muon_M=0;              
        std::vector<double>* Reco_Muon_truthPdgId=0;     
        std::vector<double>* Reco_Muon_charge=0;         
        std::vector<double>* Reco_Muon_dRTruthMu=0;      
        tree->SetBranchAddress("Reco_Muon_Pt"             , &Reco_Muon_Pt   );
        tree->SetBranchAddress("Reco_Muon_Eta"            , &Reco_Muon_Eta  );
        tree->SetBranchAddress("Reco_Muon_Phi"            , &Reco_Muon_Phi  );
        tree->SetBranchAddress("Reco_Muon_M"              , &Reco_Muon_M    );
        tree->SetBranchAddress("Reco_Muon_truthPdgId"     , &Reco_Muon_truthPdgId);
        tree->SetBranchAddress("Reco_Muon_charge"         , &Reco_Muon_charge);
        tree->SetBranchAddress("Reco_Muon_dRTruthMu"      , &Reco_Muon_dRTruthMu);

        std::vector<double>* Reco_TauJet_Pt=0;           
        std::vector<double>* Reco_TauJet_Eta=0;          
        std::vector<double>* Reco_TauJet_Phi=0;          
        std::vector<double>* Reco_TauJet_M=0;            
        std::vector<double>* Reco_TauJet_charge=0;       
        std::vector<double>* Reco_TauJet_dRTruthTauJet=0;
        std::vector<double>* Reco_TauJet_truthPdgId=0;   
        tree->SetBranchAddress("Reco_TauJet_Pt"           , &Reco_TauJet_Pt  );
        tree->SetBranchAddress("Reco_TauJet_Eta"          , &Reco_TauJet_Eta );
        tree->SetBranchAddress("Reco_TauJet_Phi"          , &Reco_TauJet_Phi );
        tree->SetBranchAddress("Reco_TauJet_M"            , &Reco_TauJet_M   );
        tree->SetBranchAddress("Reco_TauJet_charge"       , &Reco_TauJet_charge   );
        tree->SetBranchAddress("Reco_TauJet_dRTruthTauJet", &Reco_TauJet_dRTruthTauJet );
        tree->SetBranchAddress("Reco_TauJet_truthPdgId"   , &Reco_TauJet_truthPdgId );

        std::vector<double>* Reco_BJet_Pt=0;             
        std::vector<double>* Reco_BJet_Eta=0;            
        std::vector<double>* Reco_BJet_Phi=0;            
        std::vector<double>* Reco_BJet_M=0;              
        std::vector<double>* Reco_BJet_dRTruthHadBJet=0; 
        std::vector<double>* Reco_BJet_dRTruthLepBJet=0; 
        std::vector<double>* Reco_BJet_truthPdgId=0;     
        tree->SetBranchAddress("Reco_BJet_Pt"             , &Reco_BJet_Pt );
        tree->SetBranchAddress("Reco_BJet_Eta"            , &Reco_BJet_Eta);
        tree->SetBranchAddress("Reco_BJet_Phi"            , &Reco_BJet_Phi);
        tree->SetBranchAddress("Reco_BJet_M"              , &Reco_BJet_M  );
        tree->SetBranchAddress("Reco_BJet_dRTruthHadBJet" , &Reco_BJet_dRTruthHadBJet  );
        tree->SetBranchAddress("Reco_BJet_dRTruthLepBJet" , &Reco_BJet_dRTruthLepBJet  );
        tree->SetBranchAddress("Reco_BJet_truthPdgId"     , &Reco_BJet_truthPdgId  );

        double RecoMET_x;
        double RecoMET_y;
        double RecoMET_phi;
        tree->SetBranchAddress("RecoMET_x"  , &RecoMET_x   );
        tree->SetBranchAddress("RecoMET_y"  , &RecoMET_y   );
        tree->SetBranchAddress("RecoMET_phi", &RecoMET_phi );

	TH1D* h_lep             = new TH1D("h_lep","h_lep;M_{lep} [GeV];"          ,80,0,2000);
	TH1D* h_lepAfter        = new TH1D("h_lepAfter","h_lepAfter;M_{lep} [GeV];",80,0,2000);
	TH1D* h_lepPairing      = new TH1D("h_lepPairing","h_lepPairing;M_{lep} [GeV];"          ,80,0,2000);
	TH1D* h_lepAfterPairing = new TH1D("h_lepAfterPairing","h_lepAfterPairing;M_{lep} [GeV];",80,0,2000);
	TH1D* h_had      = new TH1D("h_had","h_had;M_{had} [GeV];"          ,80,0,2000);
	TH1D* h_hadAfter = new TH1D("h_hadAfter","h_hadAfter;M_{had} [GeV];",80,0,2000);
	TH1D* h_hadPairing      = new TH1D("h_hadPairing","h_hadPairing;M_{had} [GeV];"          ,80,0,2000);
	TH1D* h_hadAfterPairing = new TH1D("h_hadAfterPairing","h_hadAfterPairing;M_{had} [GeV];",80,0,2000);
	
        TH1D* h_lepReco              = new TH1D("h_lepReco"            ,"h_lepReco;M_{lepReco} [GeV];"          ,80,0,2000);
	TH1D* h_lepRecoAfter         = new TH1D("h_lepRecoAfter"       ,"h_lepRecoAfter;M_{lepReco} [GeV];",80,0,2000);
        TH1D* h_hadReco              = new TH1D("h_hadReco"            ,"h_hadReco;M_{hadReco} [GeV];"          ,80,0,2000);
	TH1D* h_hadRecoAfter         = new TH1D("h_hadRecoAfter"       ,"h_hadRecoAfter;M_{hadReco} [GeV];",80,0,2000);
        TH1D* h_lepRecoPairing       = new TH1D("h_lepRecoPairing"     ,"h_lepRecoPairing;M_{lepReco} [GeV];"          ,80,0,2000);
	TH1D* h_lepRecoAfterPairing  = new TH1D("h_lepRecoAfterPairing","h_lepRecoAfterPairing;M_{lepReco} [GeV];",80,0,2000);
        TH1D* h_hadRecoPairing       = new TH1D("h_hadRecoPairing"     ,"h_hadRecoPairing;M_{hadReco} [GeV];"          ,80,0,2000);
	TH1D* h_hadRecoAfterPairing  = new TH1D("h_hadRecoAfterPairing","h_hadRecoAfterPairing;M_{hadReco} [GeV];",80,0,2000);
        
        TH1D* h_CALepTauMass            = new TH1D("h_CALepTauMass","h_CALepTauMass;M_{#tau} [GeV];" ,100,-5,5);
        TH1D* h_CAHadTauMass            = new TH1D("h_CAHadTauMass","h_CAHadTauMass;M_{#tau} [GeV];" ,100,-5,5);

	// Coefficient for collinear mass
	TH1D* h_x1 = new TH1D("h_x1", "h_x1;1/#sqrt{x_{lep-side}};", 100, 0, 10);
	TH1D* h_x2 = new TH1D("h_x2", "h_x2;1/#sqrt{x_{had-side}};", 100, 0, 10);

	TH2D* h_LQLepCollinearMass_MET         = new TH2D("h_LQLepCollinearMass_MET"     , "h_LQLepCollinearMass_MET;M_{LQLep} [GeV];MET [GeV]"               , 80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQLepCollinearMass_MET_x       = new TH2D("h_LQLepCollinearMass_MET_x"   , "h_LQLepCollinearMass_MET_x;M_{LQLep} [GeV];MET_x [GeV]"           , 80, 0, 2000, 160, -2000, 2000);
	TH2D* h_LQLepCollinearMass_MET_y       = new TH2D("h_LQLepCollinearMass_MET_y"   , "h_LQLepCollinearMass_MET_y;M_{LQLep} [GeV];MET_y [GeV]"           , 80, 0, 2000, 160, -2000, 2000);
	TH2D* h_LQLepCollinearMass_MET_z       = new TH2D("h_LQLepCollinearMass_MET_z"   , "h_LQLepCollinearMass_MET_z;M_{LQLep} [GeV];MET_z [GeV]"           , 80, 0, 2000, 160, -2000, 2000);
	TH2D* h_LQLepCollinearMass_LeptonPt    = new TH2D("h_LQLepCollinearMass_LeptonPt", "h_LQLepCollinearMass_LeptonPt;M_{LQLep} [GeV];Lepton p_{T} [GeV]" , 80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQLepCollinearMass_LeptonPt_    = new TH2D("h_LQLepCollinearMass_LeptonPt_", "h_LQLepCollinearMass_LeptonPt_;M_{LQLep} [GeV];Lepton p_{T} [GeV]" , 80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQLepCollinearMass_Pmis1    = new TH2D("h_LQLepCollinearMass_Pmis1", "h_LQLepCollinearMass_Pmis1;M_{LQLep} [GeV];p_{mis1} [GeV]" , 80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQLepCollinearMass_TauJetPt    = new TH2D("h_LQLepCollinearMass_TauJetPt", "h_LQLepCollinearMass_TauJetPt;M_{LQLep} [GeV];TauJet p_{T} [GeV]" , 80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQLepCollinearMass_TauJetPhi   = new TH2D("h_LQLepCollinearMass_TauJetPhi", "h_LQLepCollinearMass_TauJetPhi;M_{LQLep} [GeV];TauJet #phi"      , 80, 0, 2000, 60, -TMath::Pi(), TMath::Pi());
	TH2D* h_LQLepCollinearMass_LeptonPhi   = new TH2D("h_LQLepCollinearMass_LeptonPhi", "h_LQLepCollinearMass_LeptonPhi;M_{LQLep} [GeV];Lepton #phi"      , 80, 0, 2000, 60, -TMath::Pi(), TMath::Pi());
	TH2D* h_LQLepCollinearMass_LeptonTheta = new TH2D("h_LQLepCollinearMass_LeptonTheta", "h_LQLepCollinearMass_LeptonTheta;M_{LQLep} [GeV];Lepton #theta", 80, 0, 2000, 60, 0, TMath::Pi());
	TH2D* h_LQLepCollinearMass_LQLepVisibleMass = new TH2D("h_LQLepCollinearMass_LQLepVisibleMass", "h_LQLepCollinearMass_LQLepVisibleMass;M_{LQLep} [GeV];m_{vis-lep} [GeV]", 80, 0, 2000, 80, 0, 2000);
	TH2D* h_LQLepCollinearMass_TauJetPhiLeptonPhi = new TH2D("h_LQLepCollinearMass_TauJetPhiLeptonPhi", "h_LQLepCollinearMass_TauJetPhiLeptonPhi;M_{LQLep} [GeV];#Delta #phi (#tau_{had}},lepton) [GeV]", 80, 0, 2000, 80, -TMath::Pi(), TMath::Pi());
	TH2D* h_LQLepCollinearMass_LQHadCollinearMass = new TH2D("h_LQLepCollinearMass_LQHadCollinearMass", "h_LQLepCollinearMass_LQHadCollinearMass;M_{LQLep} [GeV];M_{LQHad} [GeV]", 80, 0, 2000, 80, 0, 2000);
	
	
	TH2D* h_LQLepCollinearMass_dRLepTauLepTauNeu = new TH2D("h_LQLepCollinearMass_dRLepTauLepTauNeu", "h_LQLepCollinearMass_dRLepTauLepTauNeu;M_{LQLep} [GeV];#phi_{#tau} - #phi_{lepton} [GeV]", 80, 0, 2000, 80, 0, 0.1);
	TH2D* h_LQLepCollinearMass_dRLepTauLepLepNeu = new TH2D("h_LQLepCollinearMass_dRLepTauLepLepNeu", "h_LQLepCollinearMass_dRLepTauLepLepNeu;M_{LQLep} [GeV];#phi_{#tau} - #phi_{lepton} [GeV]", 80, 0, 2000, 80, 0, 0.1);
	TH2D* h_LQLepCollinearMass_dRLQLQ      = new TH2D("h_LQLepCollinearMass_dRLQLQ", "h_LQLepCollinearMass_dRLQLQ;M_{LQLep} [GeV];#Delta R(LQ_{lep}, LQ_{had})", 80, 0, 2000, 100, 0, 10);

	TH2D* h_dRMetLep_dRMetTauJet           = new TH2D("h_dRMetLep_dRMetTauJet", "h_dRMetLep_dRMetTauJet;#Delta R(E_{T},lep);#Delta R(E_{T},#tau_{had})", 100, 0, 10, 100, 0, 10);
	TH2D* h_LQLepVisibleMass_x1            = new TH2D("h_LQLepVisibleMass_x1", "h_LQLepVisibleMass_x1;m_{vis-lep} [GeV];1/#sqrt{x_{1}}"                 , 80, 0, 2000, 100, 0, 10);
	TH2D* h_LeptonPt_x1   	               = new TH2D("h_LeptonPt_x1"   , "h_LeptonPt_x1;Lepton p_{T} [GeV];1/#sqrt{x_{1}}"           , 80, 0, 2000, 100, 0, 10);
	TH2D* h_TauJetPt_x1                    = new TH2D("h_TauJetPt_x1"   , "h_TauJetPt_x1;TauJet p_{T} [GeV];1/#sqrt{x_{1}}"           , 80, 0, 2000, 100, 0, 10);
	TH2D* h_LeptonTheta_x1                 = new TH2D("h_LeptonTheta_x1", "h_LeptonTheta_x1;Lepton #theta;1/#sqrt{x_{1}}"             , 80, 0, TMath::Pi(), 100, 0, 10);
	TH2D* h_LeptonPhi_x1                   = new TH2D("h_LeptonPhi_x1"  , "h_LeptonPhi_x1;Lepton #phi;1/#sqrt{x_{1}}"                 , 80, -TMath::Pi(), TMath::Pi(), 100, 0, 10);
	TH2D* h_TauJetTheta_x1                 = new TH2D("h_TauJetTheta_x1", "h_TauJetTheta_x1;TauJet #theta;1/#sqrt{x_{1}}"             , 80, 0, TMath::Pi(), 100, 0, 10);
	TH2D* h_TauJetPhi_x1                   = new TH2D("h_TauJetPhi_x1"  , "h_TauJetPhi_x1;TauJet #phi;1/#sqrt{x_{1}}"                 , 80, -TMath::Pi(), TMath::Pi(), 100, 0, 10);
	TH2D* h_TauJetPhiLeptonPhi_x1          = new TH2D("h_TauJetPhiLeptonPhi_x1"  , "h_TauJetPhiLeptonPhi_x1;TauJet #phi - Lepton #phi;1/#sqrt{x_{1}}"  , 80, -TMath::Pi(), TMath::Pi(), 100, 0, 10);
	TH2D* h_Pmis_x1          = new TH2D("h_Pmis_x1"  , "h_Pmis_x1;P_{mis} [GeV];1/#sqrt{x_{1}}"  , 80, 0, 2000, 100, 0, 10);
	
	TH2D* h_dPhiLepMet_dPhiTauJetMet          = new TH2D("h_dPhiLepMet_dPhiTauJetMet"  , "h_dPhiLepMet_dPhiTauJetMet;#Delta #phi (lep, E_T);#Delta #phi (#tau_{had},E_T)"  , 80, -TMath::Pi(), TMath::Pi(), 80, -TMath::Pi(), TMath::Pi());
	
	TH2D* h_LeptonP_Pmis   	               = new TH2D("h_LeptonP_Pmis"   , "h_LeptonP_Pmis;Lepton P [GeV]; P_{mis} [GeV]"           , 80, 0, 2000, 80, 0, 2000);
	
	TH2D* h_LQHadCollinearMass_MET      = new TH2D("h_LQHadCollinearMass_MET"     , "h_LQHadCollinearMass_MET;M_{LQHad} [GeV];MET [GeV]"              ,  80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQHadCollinearMass_MET_x    = new TH2D("h_LQHadCollinearMass_MET_x"   , "h_LQHadCollinearMass_MET_x;M_{LQHad} [GeV];MET_x [GeV]"          ,  80, 0, 2000, 160, -1000, 1000);
	TH2D* h_LQHadCollinearMass_MET_y    = new TH2D("h_LQHadCollinearMass_MET_y"   , "h_LQHadCollinearMass_MET_y;M_{LQHad} [GeV];MET_y [GeV]"          ,  80, 0, 2000, 160, -1000, 1000);
	TH2D* h_LQHadCollinearMass_LeptonPt = new TH2D("h_LQHadCollinearMass_LeptonPt", "h_LQHadCollinearMass_LeptonPt;M_{LQHad} [GeV];Lepton p_{T} [GeV]",  80, 0, 2000, 80, 0, 1000);
	TH2D* h_LQHadCollinearMass_TauJetPt = new TH2D("h_LQHadCollinearMass_TauJetPt", "h_LQHadCollinearMass_TauJetPt;M_{LQHad} [GeV];TauJet p_{T} [GeV]",  80, 0, 2000, 80, 0, 1000);
	
	TH2D* h_x1_dPhiTauJetLepton = new TH2D("h_x1_dPhiTauJetLepton", "h_x1_dPhiTauJetLepton;x_1;#Delta #phi (#tau_{had}, lepton)",  100, -1, 1, 80, -TMath::Pi(), TMath::Pi());
	
	TH2D* h_Pmis1_TruthLepNeu = new TH2D("h_Pmis1_TruthLepNeu", "h_Pmis1_TruthLepNeu;P_{mis}^{1} [GeV];Truth #nu_{lep-side} Momentum [GeV]", 80, 0, 2000, 80, 0, 2000);
	TH2D* h_Pmis2_TruthTauNeu = new TH2D("h_Pmis2_TruthTauNeu", "h_Pmis2_TruthTauNeu;P_{mis}^{2} [GeV];Truth #nu_{had-side} Momentum [GeV]", 80, 0, 2000, 80, 0, 2000);
	
	TH2D* h_Pmis1_DeltaRLepNeu = new TH2D("h_Pmis1_DeltaRLepNeu", "h_Pmis1_DeltaRLepNeu;P_{mis}^{1} [GeV];DeltaR (lep, #nu_{lep-side})", 80, 0, 2000, 100, 0, 0.5);
	TH2D* h_Pmis2_DeltaRTauNeu = new TH2D("h_Pmis2_DeltaRTauNeu", "h_Pmis2_DeltaRTauNeu;P_{mis}^{2} [GeV];DeltaR (#tau, #nu_{had-side})", 80, 0, 2000, 100, 0, 0.5);
	
        TH2D* h_METTruth = new TH2D("h_METTruth", "h_METTruth;MET_{truth} [GeV];MET_{reco} [GeV]", 80, 0, 2000, 80, 0, 2000);

	EColor color[5] = { kBlack, kMagenta, kBlue, kGreen, kRed };
	
	int TotalEvents = 0 ;
	int PairingTrue = 0 ;
	int PairingFalse = 0 ;
	for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
	    tree->GetEntry(iEntry);

//	    if ( !TLV_Lepton || !TLV_TauJet || !TLV_HadBJet || !TLV_LepBJet ) continue;
            if ( !EventFlag ) continue;
	    
	    // Collinear approximation
	    double phi_1   = Lep_phi;
	    double phi_2   = TauJet_phi;
	    double theta_1 = Lep_theta;
	    double theta_2 = TauJet_theta;
	    //  LQlep side tau
	    double p_miss1 = (MET_x*TMath::Sin(phi_2) - MET_y*TMath::Cos(phi_2))/(TMath::Sin(theta_1)*TMath::Sin(phi_2 - phi_1));
	    double x1      = Lep_P/(Lep_P + p_miss1);
	    
	    //  LQhad side tau
	    double p_miss2 = (MET_x*TMath::Sin(phi_1) - MET_y*TMath::Cos(phi_1))/(TMath::Sin(theta_2)*TMath::Sin(phi_1 - phi_2));
	    double x2      = TauJet_P/(TauJet_P + p_miss2);

	    if ( p_miss1 < 0 || p_miss2 < 0 ) continue;
	    static bool PmissCondition = false;
	    if ( !PmissCondition ) {
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		std::cout << "          P_{mis} < 0 will be rejected.         " << std::endl;
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		PmissCondition = true;
	    }
            TotalEvents++;
//            std::cout << TMath::Sqrt(MET_x*MET_x + MET_y*MET_y) << " " << TMath::Sqrt(RecoMET_x*RecoMET_x + RecoMET_y*RecoMET_y) << std::endl;
            h_METTruth->Fill( TMath::Sqrt(MET_x*MET_x + MET_y*MET_y), TMath::Sqrt(RecoMET_x*RecoMET_x + RecoMET_y*RecoMET_y)/1000.);
            
            TLorentzVector Miss_NeuLep;
            Miss_NeuLep.SetPxPyPzE( 
                    p_miss1*TMath::Sin(Lep_theta)*TMath::Cos(Lep_phi)*1000, 
                    p_miss1*TMath::Sin(Lep_theta)*TMath::Sin(Lep_phi)*1000, 
                    p_miss1*TMath::Cos(Lep_theta)*1000, 
                    p_miss1*1000
                    );
            h_CALepTauMass->Fill( (*TLV_Lepton + Miss_NeuLep).M()/1000. );
            
            TLorentzVector Miss_NeuHad;
            Miss_NeuHad.SetPxPyPzE( 
                    p_miss2*TMath::Sin(TauJet_theta)*TMath::Cos(TauJet_phi)*1000, 
                    p_miss2*TMath::Sin(TauJet_theta)*TMath::Sin(TauJet_phi)*1000, 
                    p_miss2*TMath::Cos(TauJet_theta)*1000, 
                    p_miss2*1000
                    );
            h_CAHadTauMass->Fill( (*TLV_TauJet + Miss_NeuHad).M()/1000. );
	    
	    // Reference
	    h_Pmis1_TruthLepNeu ->Fill(p_miss1, (*TLV_LepNeu + *TLV_LepTauNeu).P()/1000. ) ;
	    h_Pmis1_DeltaRLepNeu->Fill(p_miss1, TLV_LepNeu->DeltaR(*TLV_LepTauNeu)) ;
	    
	    h_Pmis2_TruthTauNeu ->Fill(p_miss2, TLV_HadTauNeu->P()/1000. ) ;
	    h_Pmis2_DeltaRTauNeu->Fill(p_miss2, TLV_HadTauNeu->DeltaR(*TLV_TauJet)) ;
	    

	    h_x1->Fill(1/TMath::Sqrt(x1) );
	    h_x2->Fill(1/TMath::Sqrt(x2) );

#define BJet
//#define BQuark
	    double mod_mass_lep = LQ_M_lep_directLepAntiKt4Jet/TMath::Sqrt(x1);
	    double mod_mass_had = LQ_M_had_VisTauAntiKt4Jet/TMath::Sqrt(x2);
#ifdef BJet
	    h_lep->Fill(LQ_M_lep_directLepAntiKt4Jet);
	    h_lepAfter->Fill(mod_mass_lep);
	    h_had->Fill(LQ_M_had_VisTauAntiKt4Jet);
	    h_hadAfter->Fill(mod_mass_had);
#endif
#ifdef BQuark
            h_lep     ->Fill( (*TLV_Lepton + *TLV_LepBQuark).M()/1000. );
            h_lepAfter->Fill( (*TLV_Lepton + *TLV_LepBQuark).M()/1000./TMath::Sqrt(x1) );
            h_had     ->Fill( (*TLV_TauJet + *TLV_HadBQuark).M()/1000. );
            h_hadAfter->Fill( (*TLV_TauJet + *TLV_HadBQuark).M()/1000./TMath::Sqrt(x2) );
#endif
	    
	    h_LQLepCollinearMass_LeptonPt            ->Fill(mod_mass_lep, TLV_Lepton->Pt()/1000.);
	    if ( TMath::Abs(p_miss1 - (*TLV_LepNeu + *TLV_LepTauNeu).P()/1000. ) > 10 ) h_LQLepCollinearMass_LeptonPt_            ->Fill(mod_mass_lep, TLV_Lepton->Pt()/1000.);
	    h_LQLepCollinearMass_Pmis1               ->Fill(mod_mass_lep, p_miss1);
	    h_LQLepCollinearMass_TauJetPt            ->Fill(mod_mass_lep, TLV_TauJet->Pt()/1000.);
	   
	    h_LQLepCollinearMass_MET                 ->Fill(mod_mass_lep, TMath::Sqrt(MET_x*MET_x + MET_y*MET_y));
	    h_LQLepCollinearMass_MET_x               ->Fill(mod_mass_lep, MET_x);
	    h_LQLepCollinearMass_MET_y               ->Fill(mod_mass_lep, MET_y);
	    double MET_z = TLV_HadTauNeu->Pz() + TLV_LepTauNeu->Pz() + TLV_LepNeu->Pz();
	    h_LQLepCollinearMass_MET_z                ->Fill(mod_mass_lep, MET_z/1000.);
	    
	    h_LQLepCollinearMass_TauJetPhi           ->Fill(mod_mass_lep, TauJet_phi);
	    h_LQLepCollinearMass_LeptonPhi           ->Fill(mod_mass_lep, Lep_phi);
	    h_LQLepCollinearMass_LeptonTheta         ->Fill(mod_mass_lep, Lep_theta);
	    h_LQLepCollinearMass_LQLepVisibleMass    ->Fill(mod_mass_lep, LQ_M_lep_directLepAntiKt4Jet);
	    h_LQLepCollinearMass_TauJetPhiLeptonPhi  ->Fill(mod_mass_lep, TLV_TauJet->DeltaPhi( *TLV_Lepton ));
	    h_LQLepCollinearMass_LQHadCollinearMass  ->Fill(mod_mass_lep, mod_mass_had);
	    
	    h_LQLepCollinearMass_dRLepTauLepTauNeu   ->Fill(mod_mass_lep, dR_LepTauLepTauNeu); 
	    h_LQLepCollinearMass_dRLepTauLepLepNeu   ->Fill(mod_mass_lep, dR_LepTauLepLepNeu); 
	    
	    h_LQLepCollinearMass_dRLQLQ   ->Fill(mod_mass_lep, dR_LQLQ); 
	    
	    TLorentzVector METVec; METVec.SetPxPyPzE( MET_x/1000., MET_y/1000., MET_z/1000., 0);
	    h_dRMetLep_dRMetTauJet 	             ->Fill( TLV_Lepton->DeltaR(METVec), TLV_TauJet->DeltaR(METVec));

	    h_LQLepVisibleMass_x1 	             ->Fill(LQ_M_lep_directLepAntiKt4Jet, 1/TMath::Sqrt(x1));
	    h_LeptonPt_x1   	                     ->Fill(TLV_Lepton->Pt()/1000., 1/TMath::Sqrt(x1) );    
	    h_LeptonTheta_x1                         ->Fill(TLV_Lepton->Theta(),    1/TMath::Sqrt(x1) );
	    h_LeptonPhi_x1                           ->Fill(TLV_Lepton->Phi(),      1/TMath::Sqrt(x1) );
	    h_TauJetPt_x1                            ->Fill(TLV_TauJet->Pt()/1000., 1/TMath::Sqrt(x1) ); 
	    h_TauJetTheta_x1                         ->Fill(TLV_TauJet->Theta(),    1/TMath::Sqrt(x1) );
	    h_TauJetPhi_x1                           ->Fill(TLV_TauJet->Phi(),      1/TMath::Sqrt(x1) );
	    h_TauJetPhiLeptonPhi_x1                  ->Fill(TLV_TauJet->DeltaPhi( *TLV_Lepton ),  1/TMath::Sqrt(x1) );
	    h_Pmis_x1		                     ->Fill(p_miss1,  1/TMath::Sqrt(x1) );

	    h_dPhiLepMet_dPhiTauJetMet               ->Fill( TLV_Lepton->DeltaPhi(METVec), TLV_TauJet->DeltaPhi(METVec) );
	    
	    h_LeptonP_Pmis                           ->Fill(TLV_Lepton->P()/1000.,  p_miss1);
	

	    h_x1_dPhiTauJetLepton                    ->Fill(x1, TLV_TauJet->DeltaPhi(*TLV_Lepton) );
	   
	    // hadron side
	    h_LQHadCollinearMass_LeptonPt->Fill(mod_mass_had, TLV_Lepton->Pt()/1000.);
	    h_LQHadCollinearMass_TauJetPt->Fill(mod_mass_had, TLV_TauJet->Pt()/1000.);
	    h_LQHadCollinearMass_MET     ->Fill(mod_mass_had, TMath::Sqrt(MET_x*MET_x + MET_y*MET_y));
	    h_LQHadCollinearMass_MET_x   ->Fill(mod_mass_had, MET_x);
	    h_LQHadCollinearMass_MET_y   ->Fill(mod_mass_had, MET_y);

	    // Pairing method
	    double DeltaMTrue  = TMath::Abs((*TLV_Lepton + *TLV_LepBJet).M()/1000. - (*TLV_TauJet + *TLV_HadBJet).M()/1000.);
	    double DeltaMFalse = TMath::Abs((*TLV_Lepton + *TLV_HadBJet).M()/1000. - (*TLV_TauJet + *TLV_LepBJet).M()/1000.);

	    if ( DeltaMTrue < DeltaMFalse ) {
		PairingTrue++;
#ifdef BJet
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_LepBJet).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing->Fill(LQlepMass);
		h_lepAfterPairing->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_HadBJet).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing->Fill(LQhadMass);
		h_hadAfterPairing->Fill(col_mass_had);
#endif
#ifdef BQuark
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_LepBQuark).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing->Fill(LQlepMass);
		h_lepAfterPairing->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_HadBQuark).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing->Fill(LQhadMass);
		h_hadAfterPairing->Fill(col_mass_had);
#endif
	    }else if ( DeltaMTrue > DeltaMFalse ) {
		PairingFalse++;
#ifdef BJet
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_HadBJet).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing->Fill(LQlepMass);
		h_lepAfterPairing->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_LepBJet).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing->Fill(LQhadMass);
		h_hadAfterPairing->Fill(col_mass_had);
#endif
#ifdef BQuark
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_HadBQuark).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing->Fill(LQlepMass);
		h_lepAfterPairing->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_LepBQuark).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing->Fill(LQhadMass);
		h_hadAfterPairing->Fill(col_mass_had);
#endif
	    }
	    

            /* Pairing method using Reco */
            TLorentzVector vecTauJet, vecLepton, vecHadBJet, vecLepBJet;
            // TauJet
            std::vector<double>::iterator itr_TauJet = std::min_element(Reco_TauJet_dRTruthTauJet->begin(), Reco_TauJet_dRTruthTauJet->end());
            ssize_t min_TauJet = std::distance(Reco_TauJet_dRTruthTauJet->begin(), itr_TauJet);
            // std::cout << __LINE__ << " " << min_TauJet << " " << Reco_TauJet_Pt->size() << std::endl;
            // std::cout << __LINE__ << " " << Reco_TauJet_Pt->at(min_TauJet) << std::endl;
            vecTauJet.SetPtEtaPhiM(
                    Reco_TauJet_Pt->at(min_TauJet),
                    Reco_TauJet_Eta->at(min_TauJet),
                    Reco_TauJet_Phi->at(min_TauJet),
                    Reco_TauJet_M->at(min_TauJet)/1000.
                    );
            if ( isLeptonEle ) {            // Electron
                std::vector<double>::iterator itr_Electron = std::min_element(Reco_Electron_dRTruthEle->begin(), Reco_Electron_dRTruthEle->end());
                ssize_t min_Electron = std::distance(Reco_Electron_dRTruthEle->begin(), itr_Electron);
               //  std::cout << __LINE__ << " " << min_Electron << " " << Reco_Electron_Pt->size() << std::endl;
               //  std::cout << __LINE__ << " " << Reco_Electron_Pt->at(min_Electron) << std::endl;
            vecLepton.SetPtEtaPhiM(
                    Reco_Electron_Pt->at(min_Electron),
                    Reco_Electron_Eta->at(min_Electron),
                    Reco_Electron_Phi->at(min_Electron),
                    Reco_Electron_M->at(min_Electron)/1000.
                    );
            }
            if ( !isLeptonEle ) {            // Muon
                std::vector<double>::iterator itr_Muon = std::min_element(Reco_Muon_dRTruthMu->begin(), Reco_Muon_dRTruthMu->end());
                ssize_t min_Muon = std::distance(Reco_Muon_dRTruthMu->begin(), itr_Muon);
               //  std::cout << __LINE__ << " " << min_Muon << " " << Reco_Muon_Pt << std::endl;
               //  std::cout << __LINE__ << " " << min_Muon << " " << Reco_Muon_Pt->size() << std::endl;
               //  std::cout << __LINE__ << " " << Reco_Muon_Pt->at(min_Muon) << std::endl;
            vecLepton.SetPtEtaPhiM(
                    Reco_Muon_Pt->at(min_Muon),
                    Reco_Muon_Eta->at(min_Muon),
                    Reco_Muon_Phi->at(min_Muon),
                    Reco_Muon_M->at(min_Muon)/1000.
                    );
            }
            // BJet
            std::vector<double>::iterator itr_HadBJet = std::min_element(Reco_BJet_dRTruthHadBJet->begin(), Reco_BJet_dRTruthHadBJet->end());
            ssize_t min_HadBJet = std::distance(Reco_BJet_dRTruthHadBJet->begin(), itr_HadBJet);
            // std::cout << __LINE__ << " " << min_HadBJet << " " << Reco_BJet_Pt->size() << std::endl;
            // std::cout << __LINE__ << " " << Reco_BJet_Pt->at(min_HadBJet) << std::endl;
            vecHadBJet.SetPtEtaPhiM(
                    Reco_BJet_Pt ->at(min_HadBJet),
                    Reco_BJet_Eta->at(min_HadBJet),
                    Reco_BJet_Phi->at(min_HadBJet),
                    Reco_BJet_M  ->at(min_HadBJet)/1000.
                    );
            // BJet
            std::vector<double>::iterator itr_LepBJet = std::min_element(Reco_BJet_dRTruthLepBJet->begin(), Reco_BJet_dRTruthLepBJet->end());
            ssize_t min_LepBJet = std::distance(Reco_BJet_dRTruthLepBJet->begin(), itr_LepBJet);
            // std::cout << __LINE__ << " " << min_LepBJet << " " << Reco_BJet_Pt->size() << std::endl;
            // std::cout << __LINE__ << " " << Reco_BJet_Pt->at(min_LepBJet) << std::endl;
            vecLepBJet.SetPtEtaPhiM(
                    Reco_BJet_Pt ->at(min_LepBJet),
                    Reco_BJet_Eta->at(min_LepBJet),
                    Reco_BJet_Phi->at(min_LepBJet),
                    Reco_BJet_M  ->at(min_LepBJet)/1000.
                    );
	    
            // Collinera approximation 
            phi_1   = vecLepton.Phi();
	    phi_2   = vecTauJet.Phi();
	    theta_1 = vecLepton.Theta();
	    theta_2 = vecTauJet.Theta();
	    //  LQlep side tau
	    p_miss1 = (MET_x*TMath::Sin(phi_2) - MET_y*TMath::Cos(phi_2))/(TMath::Sin(theta_1)*TMath::Sin(phi_2 - phi_1));
	    x1      = vecLepton.P()/(vecLepton.P() + p_miss1);
	    
	    //  LQhad side tau
	    p_miss2 = (MET_x*TMath::Sin(phi_1) - MET_y*TMath::Cos(phi_1))/(TMath::Sin(theta_2)*TMath::Sin(phi_1 - phi_2));
	    x2      = vecTauJet.P()/(vecTauJet.P() + p_miss2);

            h_lepReco->Fill( (vecLepton + vecLepBJet).M() );
            h_hadReco->Fill( (vecTauJet + vecHadBJet).M() );
            //h_lepRecoAfter->Fill( (vecLepton + vecLepBJet).M()/TMath::Sqrt(x1) );
            //h_hadRecoAfter->Fill( (vecTauJet + vecHadBJet).M()/TMath::Sqrt(x2) );
           
            h_lepRecoAfter->Fill( (vecLepton + vecLepBJet).M()/TMath::Sqrt(x1) );
            h_lepRecoAfter->Fill( (vecLepton + vecHadBJet).M()/TMath::Sqrt(x1) );
            h_hadRecoAfter->Fill( (vecTauJet + vecLepBJet).M()/TMath::Sqrt(x2) );
            h_hadRecoAfter->Fill( (vecTauJet + vecHadBJet).M()/TMath::Sqrt(x2) );
            
            if ( TMath::Abs( (vecLepton + vecLepBJet).M() - (vecTauJet + vecHadBJet).M()) < TMath::Abs( (vecLepton + vecHadBJet).M() - (vecTauJet + vecLepBJet).M()) ){
                h_lepRecoPairing->Fill( (vecLepton + vecLepBJet).M() );
                h_hadRecoPairing->Fill( (vecTauJet + vecHadBJet).M() );
                h_lepRecoAfterPairing->Fill( (vecLepton + vecLepBJet).M()/TMath::Sqrt(x1)  );
                h_hadRecoAfterPairing->Fill( (vecTauJet + vecHadBJet).M()/TMath::Sqrt(x2)  );
            }
            else if ( TMath::Abs( (vecLepton + vecLepBJet).M() - (vecTauJet + vecHadBJet).M()) > TMath::Abs( (vecLepton + vecHadBJet).M() - (vecTauJet + vecLepBJet).M()) ){
                h_lepRecoPairing->Fill( (vecLepton + vecHadBJet).M() );
                h_hadRecoPairing->Fill( (vecTauJet + vecLepBJet).M() );
                h_lepRecoAfterPairing->Fill( (vecLepton + vecHadBJet).M()/TMath::Sqrt(x2)  );
                h_hadRecoAfterPairing->Fill( (vecTauJet + vecLepBJet).M()/TMath::Sqrt(x1)  );
            }
            
            Miss_NeuLep.SetPxPyPzE( 
                    p_miss1*TMath::Sin( vecLepton.Theta() )*TMath::Cos( vecLepton.Phi() ), 
                    p_miss1*TMath::Sin( vecLepton.Theta() )*TMath::Sin( vecLepton.Phi() ), 
                    p_miss1*TMath::Cos( vecLepton.Theta() ), 
                    p_miss1
                    );
            //h_CALepTauMass->Fill( ( vecLepton + Miss_NeuLep).M() );
            
            Miss_NeuHad.SetPxPyPzE( 
                    p_miss2*TMath::Sin( vecTauJet.Theta() )*TMath::Cos( vecTauJet.Phi() ), 
                    p_miss2*TMath::Sin( vecTauJet.Theta() )*TMath::Sin( vecTauJet.Phi() ), 
                    p_miss2*TMath::Cos( vecTauJet.Theta() ), 
                    p_miss2
                    );
            //h_CAHadTauMass->Fill( ( vecTauJet + Miss_NeuHad).M() );
        }
	
        /* Basic dist */
        C_CALepTauMass->cd();
        h_CALepTauMass->SetLineColor( color[iMass] );
        h_CALepTauMass->Scale(1/h_CALepTauMass->Integral());
        h_CALepTauMass->Draw("same hist");
        
        C_CAHadTauMass->cd();
        h_CAHadTauMass->SetLineColor( color[iMass] );
        h_CAHadTauMass->Scale(1/h_CAHadTauMass->Integral());
        h_CAHadTauMass->Draw("same hist");
	
        /* Invariant mass */
	h_lep     ->SetLineColor( color[iMass] );   
	h_lepAfter->SetFillColor( color[iMass] );  
	h_lepAfter->SetLineColor( color[iMass] );  
	h_lepAfter->SetFillStyle(3335);;
	
        h_lepReco     ->SetLineColor( color[iMass] );   
	h_lepRecoAfter->SetFillColor( color[iMass] );  
	h_lepRecoAfter->SetLineColor( color[iMass] );  
	h_lepRecoAfter->SetFillStyle(3335);;
	
	h_lepPairing->SetLineColor( color[iMass] );   
	h_lepAfterPairing->SetFillColor( color[iMass] );  
	h_lepAfterPairing->SetLineColor( color[iMass] );  
	h_lepAfterPairing->SetFillStyle(3335);;
	
        h_lepRecoPairing->SetLineColor( color[iMass] );   
	h_lepRecoAfterPairing->SetFillColor( color[iMass] );  
	h_lepRecoAfterPairing->SetLineColor( color[iMass] );  
	h_lepRecoAfterPairing->SetFillStyle(3335);;
	
	h_had     ->SetLineColor( color[iMass] );   
	h_hadAfter->SetFillColor( color[iMass] );  
	h_hadAfter->SetLineColor( color[iMass] );  
	h_hadAfter->SetFillStyle(3335);;
	
        h_hadReco     ->SetLineColor( color[iMass] );   
	h_hadRecoAfter->SetFillColor( color[iMass] );  
	h_hadRecoAfter->SetLineColor( color[iMass] );  
	h_hadRecoAfter->SetFillStyle(3335);;
	
	h_hadPairing->SetLineColor( color[iMass] );   
	h_hadAfterPairing->SetFillColor( color[iMass] );  
	h_hadAfterPairing->SetLineColor( color[iMass] );  
	h_hadAfterPairing->SetFillStyle(3335);;
	
        h_hadRecoPairing->SetLineColor( color[iMass] );   
	h_hadRecoAfterPairing->SetFillColor( color[iMass] );  
	h_hadRecoAfterPairing->SetLineColor( color[iMass] );  
	h_hadRecoAfterPairing->SetFillStyle(3335);;
	
	std::cout << (double)PairingTrue/(double)TotalEvents << std::endl;
	
        const double YRange = 0.4;
	C_LQLep->cd();
	h_lepAfter->Scale(1/h_lepAfter->Integral());
	h_lep     ->Scale(1/h_lep->Integral());
	h_lep     ->GetYaxis()->SetRangeUser(0,YRange);
	h_lep     ->Draw("same hist");
	h_lepAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_lepAfter->Draw("same hist");
	
        C_LQLepReco->cd();
	h_lepRecoAfter->Scale(1/h_lepRecoAfter->Integral());
	h_lepReco     ->Scale(1/h_lepReco->Integral());
	h_lepReco     ->GetYaxis()->SetRangeUser(0,YRange);
	h_lepReco     ->Draw("same hist");
	h_lepRecoAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_lepRecoAfter->Draw("same hist");
	
	C_LQLepPairing->cd();
	h_lepPairing     ->Scale(1/h_lepPairing->Integral());
	h_lepPairing     ->GetYaxis()->SetRangeUser(0,YRange);
	h_lepPairing     ->Draw("same hist");
	h_lepAfterPairing->Scale(1/h_lepAfterPairing->Integral());
	h_lepAfterPairing->GetYaxis()->SetRangeUser(0,YRange);
	h_lepAfterPairing->Draw("same hist");
	
        C_LQLepRecoPairing->cd();
	h_lepRecoPairing     ->Scale(1/h_lepRecoPairing->Integral());
	h_lepRecoPairing     ->GetYaxis()->SetRangeUser(0,YRange);
	h_lepRecoPairing     ->Draw("same hist");
	h_lepRecoAfterPairing->Scale(1/h_lepRecoAfterPairing->Integral());
	h_lepRecoAfterPairing->GetYaxis()->SetRangeUser(0,YRange);
	h_lepRecoAfterPairing->Draw("same hist");
	
	C_LQHadPairing->cd();
	h_hadPairing     ->Scale(1/h_hadPairing->Integral());
	h_hadPairing     ->GetYaxis()->SetRangeUser(0,YRange);
	h_hadPairing     ->Draw("same hist");
	h_hadAfterPairing->Scale(1/h_hadAfterPairing->Integral());
	h_hadAfterPairing->GetYaxis()->SetRangeUser(0,YRange);
	h_hadAfterPairing->Draw("same hist");
	
	C_LQHad->cd();
	h_hadAfter->Scale(1/h_hadAfter->Integral());
	h_had     ->Scale(1/h_had->Integral());
	h_had     ->GetYaxis()->SetRangeUser(0,YRange);
	h_had     ->Draw("same hist");
	h_hadAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_hadAfter->Draw("same hist");
	
        C_LQHadReco->cd();
	h_hadRecoAfter->Scale(1/h_hadRecoAfter->Integral());
	h_hadReco     ->Scale(1/h_hadReco->Integral());
	h_hadReco     ->GetYaxis()->SetRangeUser(0,YRange);
	h_hadReco     ->Draw("same hist");
	h_hadRecoAfter->GetYaxis()->SetRangeUser(0,YRange);
	h_hadRecoAfter->Draw("same hist");
	
        C_LQHadRecoPairing->cd();
	h_hadRecoPairing     ->Scale(1/h_hadRecoPairing->Integral());
	h_hadRecoPairing     ->GetYaxis()->SetRangeUser(0,YRange);
	h_hadRecoPairing     ->Draw("same hist");
	h_hadRecoAfterPairing->Scale(1/h_hadRecoAfterPairing->Integral());
	h_hadRecoAfterPairing->GetYaxis()->SetRangeUser(0,YRange);
	h_hadRecoAfterPairing->Draw("same hist");

	C_LQLepCollinearMass_LeptonPt->cd(iMass+1);
	//h_LQLepCollinearMass_LeptonPt->Scale(1/h_LQLepCollinearMass_LeptonPt->Integral());
	h_LQLepCollinearMass_LeptonPt->SetLineColor( color[iMass] );
//	h_LQLepCollinearMass_LeptonPt->Draw("colz same hist");
	h_LQLepCollinearMass_LeptonPt->SetMarkerColor(kBlue);
	h_LQLepCollinearMass_LeptonPt->SetMarkerSize(0.4);
	h_LQLepCollinearMass_LeptonPt->Draw("same");
	h_LQLepCollinearMass_LeptonPt_->SetMarkerColor(kMagenta);
	h_LQLepCollinearMass_LeptonPt_->SetMarkerSize(0.4);
	h_LQLepCollinearMass_LeptonPt_->Draw("same");
	
	C_LQLepCollinearMass_Pmis1->cd(iMass+1);
	//h_LQLepCollinearMass_Pmis1->Scale(1/h_LQLepCollinearMass_Pmis1->Integral());
	h_LQLepCollinearMass_Pmis1->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_Pmis1->Draw("colz same hist");
	
	C_LQLepCollinearMass_TauJetPt->cd(iMass+1);
	//h_LQLepCollinearMass_TauJetPt->Scale(1/h_LQLepCollinearMass_TauJetPt->Integral());
	h_LQLepCollinearMass_TauJetPt->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_TauJetPt->Draw("colz same hist");
	
	C_LQLepCollinearMass_MET->cd(iMass+1);
	//h_LQLepCollinearMass_MET->Scale(1/h_LQLepCollinearMass_MET->Integral());
	h_LQLepCollinearMass_MET->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_MET->Draw("colz same hist");
	
	C_LQLepCollinearMass_MET_x->cd(iMass+1);
	//h_LQLepCollinearMass_MET_x->Scale(1/h_LQLepCollinearMass_MET_x->Integral());
	h_LQLepCollinearMass_MET_x->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_MET_x->Draw("colz same hist");
	
	C_LQLepCollinearMass_MET_y->cd(iMass+1);
	//h_LQLepCollinearMass_MET_y->Scale(1/h_LQLepCollinearMass_MET_y->Integral());
	h_LQLepCollinearMass_MET_y->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_MET_y->Draw("colz same hist");
	
	C_LQLepCollinearMass_TauJetPhi->cd(iMass+1);
	//h_LQLepCollinearMass_TauJetPhi->Scale(1/h_LQLepCollinearMass_TauJet_Phi->Integral());
	h_LQLepCollinearMass_TauJetPhi->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_TauJetPhi->Draw("colz same hist");
	
	C_LQLepCollinearMass_LeptonPhi->cd(iMass+1);
	//h_LQLepCollinearMass_LeptonPhi->Scale(1/h_LQLepCollinearMass_Lepton_Phi->Integral());
	h_LQLepCollinearMass_LeptonPhi->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_LeptonPhi->Draw("colz same hist");
	
	C_LQLepCollinearMass_LeptonTheta->cd(iMass+1);
	//h_LQLepCollinearMass_LeptonTheta->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_LeptonTheta->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_LeptonTheta->Draw("colz same hist");
	
	C_LQLepCollinearMass_LQLepVisibleMass->cd(iMass+1);
	//h_LQLepCollinearMass_LQLepVisibleMass->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_LQLepVisibleMass->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_LQLepVisibleMass->Draw("colz same hist");
	
	C_LQLepCollinearMass_TauJetPhiLeptonPhi->cd(iMass+1);
	//h_LQLepCollinearMass_TauJetPhiLeptonPhi->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_TauJetPhiLeptonPhi->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_TauJetPhiLeptonPhi->Draw("colz same hist");
	
	C_LQLepCollinearMass_LQHadCollinearMass->cd(iMass+1);
	//h_LQLepCollinearMass_LQHadCollinearMass->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_LQHadCollinearMass->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_LQHadCollinearMass->Draw("colz same hist");
	
	C_LQLepCollinearMass_MET_z->cd(iMass+1);
	//h_LQLepCollinearMass_MET_z->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_MET_z->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_MET_z->Draw("colz same hist");
	
	C_LQLepCollinearMass_dRLepTauLepTauNeu->cd(iMass+1);
	//h_LQLepCollinearMass_dRLepTauLepTauNeu->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_dRLepTauLepTauNeu->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_dRLepTauLepTauNeu->Draw("colz same hist");
	
	C_LQLepCollinearMass_dRLepTauLepLepNeu->cd(iMass+1);
	//h_LQLepCollinearMass_dRLepTauLepLepNeu->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_dRLepTauLepLepNeu->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_dRLepTauLepLepNeu->Draw("colz same hist");
	
	C_LQLepCollinearMass_dRLQLQ->cd(iMass+1);
	//h_LQLepCollinearMass_dRLQLQ->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepCollinearMass_dRLQLQ->SetLineColor( color[iMass] );
	h_LQLepCollinearMass_dRLQLQ->Draw("colz same hist");
	
	C_dRMetLep_dRMetTauJet->cd(iMass+1);
	//h_LQLepCollinearMass_x1->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_dRMetLep_dRMetTauJet->SetLineColor( color[iMass] );
	h_dRMetLep_dRMetTauJet->Draw("colz same hist");
	
	C_LQLepVisibleMass_x1->cd(iMass+1);
	//h_LQLepCollinearMass_x1->Scale(1/h_LQLepCollinearMass_Lepton_Theta->Integral());
	h_LQLepVisibleMass_x1->SetLineColor( color[iMass] );
	h_LQLepVisibleMass_x1->Draw("colz same hist");


	C_LeptonPt_x1->cd(iMass+1);
	h_LeptonPt_x1->SetLineColor( color[iMass] );        
	h_LeptonPt_x1->Draw("colz same hist");

	C_LeptonTheta_x1->cd(iMass+1);
	h_LeptonTheta_x1->SetLineColor( color[iMass] );    
	h_LeptonTheta_x1->Draw("colz same hist");    

	C_LeptonPhi_x1->cd(iMass+1);
	h_LeptonPhi_x1->SetLineColor( color[iMass] );    
	h_LeptonPhi_x1->Draw("colz same hist");    
	
	C_TauJetPt_x1->cd(iMass+1);
	h_TauJetPt_x1->SetLineColor( color[iMass] );    
	h_TauJetPt_x1->Draw("colz same hist");    
	
	C_TauJetTheta_x1->cd(iMass+1);
	h_TauJetTheta_x1->SetLineColor( color[iMass] );    
	h_TauJetTheta_x1->Draw("colz same hist");    

	C_TauJetPhi_x1->cd(iMass+1);
	h_TauJetPhi_x1->SetLineColor( color[iMass] );    
	h_TauJetPhi_x1->Draw("colz same hist");    
	
	C_TauJetPhiLeptonPhi_x1->cd(iMass+1);
	h_TauJetPhiLeptonPhi_x1->SetLineColor( color[iMass] );    
	h_TauJetPhiLeptonPhi_x1->Draw("colz same hist");    
	
	C_Pmis_x1->cd(iMass+1);
	h_Pmis_x1->SetLineColor( color[iMass] );    
	h_Pmis_x1->Draw("colz same hist");    
	
	C_dPhiLepMet_dPhiTauJetMet->cd(iMass+1);
	h_dPhiLepMet_dPhiTauJetMet->SetLineColor( color[iMass] );    
	h_dPhiLepMet_dPhiTauJetMet->Draw("colz same hist");    
	
	C_LeptonP_Pmis->cd(iMass+1);
	h_LeptonP_Pmis->SetLineColor( color[iMass] );        
	h_LeptonP_Pmis->Draw("colz same hist");
	
	C_x1_dPhiTauJetLepton->cd(iMass+1);
	h_x1_dPhiTauJetLepton->SetLineColor( color[iMass] );        
	h_x1_dPhiTauJetLepton->Draw("colz same hist");
	
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	C_LQHadCollinearMass_LeptonPt->cd(iMass+1);
	//h_LQHadCollinearMass_LeptonPt->Scale(1/h_LQHadCollinearMass_LeptonPt->Integral());
	h_LQHadCollinearMass_LeptonPt->SetLineColor( color[iMass] );
	h_LQHadCollinearMass_LeptonPt->Draw("colz same hist");
	
	C_LQHadCollinearMass_TauJetPt->cd(iMass+1);
	//h_LQHadCollinearMass_TauJetPt->Scale(1/h_LQHadCollinearMass_TauJetPt->Integral());
	h_LQHadCollinearMass_TauJetPt->SetLineColor( color[iMass] );
	h_LQHadCollinearMass_TauJetPt->Draw("colz same hist");
	
	C_LQHadCollinearMass_MET->cd(iMass+1);
	//h_LQHadCollinearMass_MET->Scale(1/h_LQHadCollinearMass_MET->Integral());
	h_LQHadCollinearMass_MET->SetLineColor( color[iMass] );
	h_LQHadCollinearMass_MET->Draw("colz same hist");
	
	C_LQHadCollinearMass_MET_x->cd(iMass+1);
	//h_LQHadCollinearMass_MET_x->Scale(1/h_LQHadCollinearMass_MET_x->Integral());
	h_LQHadCollinearMass_MET_x->SetLineColor( color[iMass] );
	h_LQHadCollinearMass_MET_x->Draw("colz same hist");
	
	C_LQHadCollinearMass_MET_y->cd(iMass+1);
	//h_LQHadCollinearMass_MET_y->Scale(1/h_LQHadCollinearMass_MET_y->Integral());
	h_LQHadCollinearMass_MET_y->SetLineColor( color[iMass] );
	h_LQHadCollinearMass_MET_y->Draw("colz same hist");

	C_x1_lepSide->cd();
	h_x1->Scale(1/h_x1->Integral());
	h_x1->SetLineColor( color[iMass] );
	h_x1->Draw("same hist");
	
	C_x2_lepSide->cd();
	h_x2->Scale(1/h_x2->Integral());
	h_x2->SetLineColor( color[iMass] );
	h_x2->Draw("same hist");

	C_Pmis1_TruthLepNeu->cd(iMass+1);
	h_Pmis1_TruthLepNeu->Draw("colz same hist");
	
	C_Pmis1_DeltaRLepNeu->cd(iMass+1);
	h_Pmis1_DeltaRLepNeu->Draw("colz same hist");
	
	C_Pmis2_TruthTauNeu->cd(iMass+1);
	h_Pmis2_TruthTauNeu->Draw("colz same hist");
	
	C_Pmis2_DeltaRTauNeu->cd(iMass+1);
	h_Pmis2_DeltaRTauNeu->Draw("colz same hist");

        C_METTruth->cd(iMass+1);
        h_METTruth->Draw("colz same");
    }
    
    /* Basic dist */
    C_CALepTauMass         ->SaveAs("C_detail.pdf(");
    C_CAHadTauMass         ->SaveAs("C_detail.pdf");

    C_METTruth->SaveAs("C_detail.pdf");
    
    C_LQLep->SaveAs("C_detail.pdf");
    C_LQLepPairing->SaveAs("C_detail.pdf");
    C_LQHad->SaveAs("C_detail.pdf");
    C_LQHadPairing->SaveAs("C_detail.pdf");
   
    /* Reco */
    C_LQLepReco         ->SaveAs("C_detail.pdf");
    C_LQLepRecoPairing  ->SaveAs("C_detail.pdf");
    C_LQHadReco         ->SaveAs("C_detail.pdf");
    C_LQHadRecoPairing  ->SaveAs("C_detail.pdf");

    /* LQmass(lep) vs XX */
    C_LQLepCollinearMass_LeptonPt           ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_TauJetPt           ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_Pmis1              ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_MET                ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_MET_x              ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_MET_y              ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_MET_z              ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_LeptonPhi          ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_TauJetPhi          ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_TauJetPhiLeptonPhi ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_LeptonTheta        ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_LQLepVisibleMass   ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_LQHadCollinearMass ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_dRLepTauLepTauNeu  ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_dRLepTauLepLepNeu  ->SaveAs("C_detail.pdf");
    C_LQLepCollinearMass_dRLQLQ             ->SaveAs("C_detail.pdf");
    
    C_dRMetLep_dRMetTauJet          ->SaveAs("C_detail.pdf");

    /* x1 vs XX */
    C_LQLepVisibleMass_x1     ->SaveAs("C_detail.pdf");
    C_LeptonPt_x1   	      ->SaveAs("C_detail.pdf");
    C_LeptonTheta_x1   	      ->SaveAs("C_detail.pdf");
    C_LeptonPhi_x1     	      ->SaveAs("C_detail.pdf");
    C_TauJetPt_x1   	      ->SaveAs("C_detail.pdf");
    C_TauJetTheta_x1   	      ->SaveAs("C_detail.pdf");
    C_TauJetPhi_x1     	      ->SaveAs("C_detail.pdf");
    C_TauJetPhiLeptonPhi_x1   ->SaveAs("C_detail.pdf");
    C_Pmis_x1                 ->SaveAs("C_detail.pdf");
    C_x1_dPhiTauJetLepton       ->SaveAs("C_detail.pdf");
    C_dPhiLepMet_dPhiTauJetMet  	 ->SaveAs("C_detail.pdf");
	
    /* Pmis vs XX */
    C_Pmis1_TruthLepNeu->SaveAs("C_detail.pdf");
    C_Pmis1_DeltaRLepNeu->SaveAs("C_detail.pdf");
    C_Pmis2_TruthTauNeu->SaveAs("C_detail.pdf");
    C_Pmis2_DeltaRTauNeu->SaveAs("C_detail.pdf");
    C_LeptonP_Pmis       ->SaveAs("C_detail.pdf");

    /* LQmass(had) vs XX */
    C_LQHadCollinearMass_LeptonPt   ->SaveAs("C_detail.pdf");
    C_LQHadCollinearMass_MET        ->SaveAs("C_detail.pdf");
    C_LQHadCollinearMass_MET_x      ->SaveAs("C_detail.pdf");
    C_LQHadCollinearMass_MET_y      ->SaveAs("C_detail.pdf");
    C_LQHadCollinearMass_TauJetPt   ->SaveAs("C_detail.pdf)");
}
