
void CollinearMassFit()
{
    TCanvas * C_LQLep        = new TCanvas("C_LQLep","C_LQLep");
    TCanvas * C_LQLepPairing = new TCanvas("C_LQLepPairing","C_LQLepPairing");
    TCanvas * C_LQHadPairing = new TCanvas("C_LQHadPairing","C_LQHadPairing");
    TCanvas * C_LQLepCompare = new TCanvas("C_LQLepCompare","C_LQLepCompare");
    TCanvas * C_LQHad        = new TCanvas("C_LQHad","C_LQHad");
	
    TH1D* h_lep             [5]; 
    TH1D* h_lepAfter        [5]; 
    TH1D* h_lepPairing      [5]; 
    TH1D* h_lepAfterPairing [5]; 
    TH1D* h_had             [5]; 
    TH1D* h_hadAfter        [5]; 
    TH1D* h_hadPairing      [5]; 
    TH1D* h_hadAfterPairing [5]; 
    for ( int iMass=0; iMass<5; iMass++){
	h_lep             [iMass] = new TH1D( Form("h_lep%d", iMass)             ,"h_lep;M_{lep} [GeV];"            ,80,0,2000);
	h_lepAfter        [iMass] = new TH1D( Form("h_lepAfter%d", iMass)        ,"h_lepAfter;M_{lep} [GeV];"       ,80,0,2000);
	h_lepPairing      [iMass] = new TH1D( Form("h_lepPairing%d", iMass)      ,"h_lepPairing;M_{lep} [GeV];"     ,80,0,2000);
	h_lepAfterPairing [iMass] = new TH1D( Form("h_lepAfterPairing%d", iMass) ,"h_lepAfterPairing;M_{lep} [GeV];",80,0,2000);
	h_had             [iMass] = new TH1D( Form("h_had%d", iMass)             ,"h_had;M_{had} [GeV];"            ,80,0,2000);
	h_hadAfter        [iMass] = new TH1D( Form("h_hadAfter%d", iMass)        ,"h_hadAfter;M_{had} [GeV];"       ,80,0,2000);
	h_hadPairing      [iMass] = new TH1D( Form("h_hadPairing%d", iMass)      ,"h_hadPairing;M_{had} [GeV];"     ,80,0,2000);
	h_hadAfterPairing [iMass] = new TH1D( Form("h_hadAfterPairing%d", iMass) ,"h_hadAfterPairing;M_{had} [GeV];",80,0,2000);
    }

    std::vector<std::string> mass_point = {"300", "500", "900", "1300", "1700"};
    for ( int iMass=0; iMass< mass_point.size(); iMass++){
	std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/MyNtuple.M" + mass_point[iMass] + ".root";
//	std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/test.root";
	std::cout << file_name << std::endl;
	TFile* file = new TFile( file_name.c_str() );
	TTree* tree = (TTree*)file->Get("m_true");

	double LQ_M_lep_directLepJet;
	double LQ_M_lep_directLepAntiKt4Jet;
	double LQ_M_had_VisTauJet;
	double LQ_M_had_VisTauAntiKt4Jet;
	double Lep_P   ;
	double Lep_theta   ;
	double Lep_phi     ;
	double Ele_theta   ;
	double Ele_phi     ;
	double Mu_theta    ;
	double Mu_phi      ;
	double TauJet_P;
	double TauJet_theta;
	double TauJet_phi  ;
	double MET_x       ;
	double MET_y       ;
      //{"dR_LepLepNeuLepLep",    "#Delta R #left(lepton, #nu_{lep} #right)" },
      //{"dR_LepTauNeuLepLep",    "#Delta R #left(lepton, #nu_{#tau} #right)" },
      //{"dR_LepTauLepLep",       "#Delta R #left(lepton, #tau_{lep} #right)" },
      //{"dR_LepTauNeuLepLepNeu", "#Delta R #left(#nu_{lep}, #nu_{#tau} #right)" },
      //{"dR_LepTauLepLepNeu",    "#Delta R #left(#nu_{lep}, #tau_{lep} #right)" },
      //{"dR_LepTauLepTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{lep} #right)" },
      //{"dR_HadTauHadTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{had} #right)" },
       double dR_LepTauLepLepNeu;
       double dR_LepTauLepTauNeu;

	TLorentzVector* TLV_Lepton    = nullptr;
	TLorentzVector* TLV_Electron  = nullptr;
	TLorentzVector* TLV_Muon      = nullptr;
	TLorentzVector* TLV_TauJet    = nullptr;
	TLorentzVector* TLV_LepNeu    = nullptr;
	TLorentzVector* TLV_EleNeu    = nullptr;
	TLorentzVector* TLV_MuNeu     = nullptr;
	TLorentzVector* TLV_HadTauNeu = nullptr;
	TLorentzVector* TLV_LepTauNeu = nullptr;
	TLorentzVector* TLV_HadBJet   = nullptr;
	TLorentzVector* TLV_LepBJet   = nullptr;

	// Collinear mass
	tree->SetBranchAddress("LQ_M_lep_directLepJet" , &LQ_M_lep_directLepJet   );
	tree->SetBranchAddress("LQ_M_had_VisTauJet"    , &LQ_M_had_VisTauJet   );
	tree->SetBranchAddress("LQ_M_lep_directLepAntiKt4Jet" , &LQ_M_lep_directLepAntiKt4Jet   );
	tree->SetBranchAddress("LQ_M_had_VisTauAntiKt4Jet"    , &LQ_M_had_VisTauAntiKt4Jet   );
	tree->SetBranchAddress("Lep_P"                 , &Lep_P       );
	tree->SetBranchAddress("Lep_theta"             , &Lep_theta   );
	tree->SetBranchAddress("Lep_phi"               , &Lep_phi     );
	tree->SetBranchAddress("Ele_theta"             , &Ele_theta   );
	tree->SetBranchAddress("Ele_phi"               , &Ele_phi     );
	tree->SetBranchAddress("Mu_theta"              , &Mu_theta    );
	tree->SetBranchAddress("Mu_phi"                , &Mu_phi      );
	tree->SetBranchAddress("TauJet_P"          , &TauJet_P);
	tree->SetBranchAddress("TauJet_theta"          , &TauJet_theta);
	tree->SetBranchAddress("TauJet_phi"            , &TauJet_phi  );
	tree->SetBranchAddress("MET_x"                 , &MET_x       );
	tree->SetBranchAddress("MET_y"                 , &MET_y       );
	
	tree->SetBranchAddress("dR_LepTauLepLepNeu"                 , &dR_LepTauLepLepNeu       );
	tree->SetBranchAddress("dR_LepTauLepTauNeu"                 , &dR_LepTauLepTauNeu       );
	
	tree->SetBranchAddress("TLV_Lepton"    , &TLV_Lepton   );
	tree->SetBranchAddress("TLV_Electron"  , &TLV_Electron );
	tree->SetBranchAddress("TLV_Muon"      , &TLV_Muon     );
	tree->SetBranchAddress("TLV_TauJet"    , &TLV_TauJet   );
	tree->SetBranchAddress("TLV_LepNeu"    , &TLV_LepNeu   );
	tree->SetBranchAddress("TLV_EleNeu"    , &TLV_EleNeu   );
	tree->SetBranchAddress("TLV_MuNeu"     , &TLV_MuNeu    );
	tree->SetBranchAddress("TLV_HadTauNeu" , &TLV_HadTauNeu);
	tree->SetBranchAddress("TLV_LepTauNeu" , &TLV_LepTauNeu);
	tree->SetBranchAddress("TLV_HadBJet"   , &TLV_HadBJet  );
	tree->SetBranchAddress("TLV_LepBJet"   , &TLV_LepBJet  );

	
	int PairingTrue = 0 ;
	int PairingFalse = 0 ;
	for(int iEntry=0; iEntry< tree->GetEntries(); iEntry++){
	    tree->GetEntry(iEntry);

	    if ( !TLV_Lepton || !TLV_TauJet || !TLV_HadBJet || !TLV_LepBJet ) continue;
	    
	    // Collinear approximation
	    double phi_1   = Lep_phi;
	    double phi_2   = TauJet_phi;
	    double theta_1 = Lep_theta;
	    double theta_2 = TauJet_theta;
	    //  LQlep side tau
	    double p_miss1 = (MET_x*TMath::Sin(phi_2) - MET_y*TMath::Cos(phi_2))/(TMath::Sin(theta_1)*TMath::Sin(phi_2 - phi_1));
	    double x1      = Lep_P/(Lep_P + p_miss1);
	    
	    //  LQhad side tau
	    double p_miss2 = (MET_x*TMath::Sin(phi_1) - MET_y*TMath::Cos(phi_1))/(TMath::Sin(theta_2)*TMath::Sin(phi_1 - phi_2));
	    double x2      = TauJet_P/(TauJet_P + p_miss2);

	    if ( p_miss1 < 0 || p_miss2 < 0 ) continue;
	    static bool PmissCondition = false;
	    if ( !PmissCondition ) {
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		std::cout << "          P_{mis} < 0 will be rejected.         " << std::endl;
		std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
		PmissCondition = true;
	    }
	    
	    double mod_mass_lep = LQ_M_lep_directLepAntiKt4Jet/TMath::Sqrt(x1);
	    h_lep[iMass]->Fill(LQ_M_lep_directLepAntiKt4Jet);
	    h_lepAfter[iMass]->Fill(mod_mass_lep);
	    
	    double mod_mass_had = LQ_M_had_VisTauAntiKt4Jet/TMath::Sqrt(x2);
	    h_had[iMass]->Fill(LQ_M_had_VisTauAntiKt4Jet);
	    h_hadAfter[iMass]->Fill(mod_mass_had);

	    // Pairing method
	    double DeltaMTrue  = TMath::Abs((*TLV_Lepton + *TLV_LepBJet).M()/1000. - (*TLV_TauJet + *TLV_HadBJet).M()/1000.);
	    double DeltaMFalse = TMath::Abs((*TLV_Lepton + *TLV_HadBJet).M()/1000. - (*TLV_TauJet + *TLV_LepBJet).M()/1000.);

	    if ( DeltaMTrue < DeltaMFalse ) {
		PairingTrue++;
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_LepBJet).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing[iMass]->Fill(LQlepMass);
		h_lepAfterPairing[iMass]->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_HadBJet).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing[iMass]->Fill(LQhadMass);
		h_hadAfterPairing[iMass]->Fill(col_mass_had);
	    }else if ( DeltaMTrue > DeltaMFalse ) {
		PairingFalse++;
		// Lep
		double LQlepMass = (*TLV_Lepton + *TLV_HadBJet).M()/1000.;
		double col_mass_lep = LQlepMass/TMath::Sqrt(x1);
		h_lepPairing[iMass]->Fill(LQlepMass);
		h_lepAfterPairing[iMass]->Fill(col_mass_lep);
		// Had
		double LQhadMass = (*TLV_TauJet + *TLV_HadBJet).M()/1000.;
		double col_mass_had = LQhadMass/TMath::Sqrt(x2);
		h_hadPairing[iMass]->Fill(LQhadMass);
		h_hadAfterPairing[iMass]->Fill(col_mass_had);
	    }
	}
    }

    for ( int iMass=0; iMass<5; iMass++ ) {
	EColor color[5] = { kBlack, kMagenta, kBlue, kGreen, kRed };
	
	h_lep     [iMass]->SetLineColor( color[iMass] );   
	h_lepAfter[iMass]->SetFillColor( color[iMass] );  
	h_lepAfter[iMass]->SetLineColor( color[iMass] );  
	h_lepAfter[iMass]->SetFillStyle(3335);

	std::cout << h_lepAfter[iMass]->GetSkewness() << std::endl;

	h_lepPairing[iMass]->SetLineColor( color[iMass] );   
	h_lepAfterPairing[iMass]->SetFillColor( color[iMass] );  
	h_lepAfterPairing[iMass]->SetLineColor( color[iMass] );  
	h_lepAfterPairing[iMass]->SetFillStyle(3335);;

	h_had     [iMass]->SetLineColor( color[iMass] );   
	h_hadAfter[iMass]->SetFillColor( color[iMass] );  
	h_hadAfter[iMass]->SetLineColor( color[iMass] );  
	h_hadAfter[iMass]->SetFillStyle(3335);;

	h_hadPairing[iMass]->SetLineColor( color[iMass] );   
	h_hadAfterPairing[iMass]->SetFillColor( color[iMass] );  
	h_hadAfterPairing[iMass]->SetLineColor( color[iMass] );  
	h_hadAfterPairing[iMass]->SetFillStyle(3335);;

	C_LQLep->cd();
	h_lepAfter[iMass]->Scale(1/h_lepAfter[iMass]->Integral());
	h_lep     [iMass]->Scale(1/h_lep[iMass]->Integral());
	h_lep     [iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_lep     [iMass]->Draw("same hist");
	h_lepAfter[iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_lepAfter[iMass]->Draw("same hist");

	C_LQLepPairing->cd();
	h_lepPairing     [iMass]->Scale(1/h_lepPairing[iMass]->Integral());
	h_lepPairing     [iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_lepPairing     [iMass]->Draw("same hist");
	h_lepAfterPairing[iMass]->Scale(1/h_lepAfterPairing[iMass]->Integral());
	h_lepAfterPairing[iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_lepAfterPairing[iMass]->Draw("same hist");

	C_LQHadPairing->cd();
	h_hadPairing     [iMass]->Scale(1/h_hadPairing[iMass]->Integral());
	h_hadPairing     [iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_hadPairing     [iMass]->Draw("same hist");
	h_hadAfterPairing[iMass]->Scale(1/h_hadAfterPairing[iMass]->Integral());
	h_hadAfterPairing[iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_hadAfterPairing[iMass]->Draw("same hist");

	C_LQHad->cd();
        std::cout << mass_point[iMass] << " " << h_hadAfter[iMass]->GetEntries() << std::endl;
	h_hadAfter[iMass]->Scale(1/h_hadAfter[iMass]->Integral());
	h_had     [iMass]->Scale(1/h_had[iMass]->Integral());
	h_had     [iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_had     [iMass]->Draw("same hist");
	h_hadAfter[iMass]->GetYaxis()->SetRangeUser(0,0.25);
	h_hadAfter[iMass]->Draw("same hist");
    }
}
