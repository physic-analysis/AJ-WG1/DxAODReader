

void ScanTTree()
{
  std::vector<std::string> mass_point = {"300","500","900","1300","1700"};
  TTree* tree[ static_cast<const int>(mass_point.size()) ];

  for ( int iMass=0; iMass< mass_point.size(); iMass++){
    std::string file_name = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/DxAODBasedAnalysis/build/MyNtuple.M" + mass_point[iMass] + ".root";
    std::cout << file_name << std::endl;
    TFile* file = new TFile( file_name.c_str() );
    tree[iMass] = (TTree*)file->Get("m_true");
  }
  
  
  std::vector<std::vector<std::string>> obj = 
  {
    {"LQ_M_lep_direct"       , "(100, 0, 2000)" , "LQ invariant mass (#tau + jet)"  },
    {"LQ_M_lep_directLepJet" , "(100, 0, 2000)" , "LQ invariant mass (lepton+jet)"  },
    {"LQ_M_lep_directEleJet" , "(100, 0, 2000)" , "LQ invariant mass (ele+jet)"     },
    {"LQ_M_lep_directMuJet"  , "(100, 0, 2000)" , "LQ invariant mass (mu+jet)"      },

    {"LQ_M_had_direct"       , "(100, 0, 2000)" , "LQ invariant mass (#tau + jet)"  },
    {"LQ_M_had_VisTauJet"    , "(100, 0, 2000)" , "LQ invariant mass (vistau+jet)"  },
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},

    {"LQLep_Pt"              , "(100, 0, 1000)" , "LQ(lep) pT"  },
    {"LQLep_Eta"             , "(50, -3, 3)"    , "LQ(lep) #eta"},
    {"LQLep_Phi"             , "(50, -3, 3)"    , "LQ(lep) #phi"},
    {"Empty","Empty","Empty"},

    {"LQHad_Pt"              , "(100, 0, 1000)" , "LQ(had) pT"  },
    {"LQHad_Eta"             , "(50, -3, 3)"    , "LQ(had) #eta"},
    {"LQHad_Phi"             , "(50, -3, 3)"    , "LQ(had) #phi"},
    {"Empty","Empty","Empty"},
    
    {"BottomHad_Pt"          , "(100, 0, 1000)" , "b-quark pT"  },
    {"BottomHad_Eta"         , "(50, -3, 3)"    , "b-quark #eta"},
    {"BottomHad_Phi"         , "(50, -3, 3)"    , "b-quark #phi"}, 
    {"Empty","Empty","Empty"},

    {"BottomLep_Pt"          , "(100, 0, 1000)" , "b-quark pT"  }, 
    {"BottomLep_Eta"         , "(50, -3, 3)"    , "b-quark #eta"}, 
    {"BottomLep_Phi"         , "(50, -3, 3)"    , "b-quark #phi"}, 
    {"Empty","Empty","Empty"},


    {"VisTau_Pt"             , "(100, 0, 1000)" , "visTau pT"  },
    {"VisTau_Eta"            , "(50, -3, 3)"    , "visTau #eta"}, 
    {"VisTau_Phi"            , "(50, -3, 3)"    , "visTau #phi"}, 
    {"Empty","Empty","Empty"},

    {"Electron_Pt"           , "(100, 0, 1000)" , "electron pT"  }, 
    {"Electron_Eta"          , "(50, -3, 3)"    , "electron #eta"},
    {"Electron_Phi"          , "(50, -3, 3)"    , "electron #phi"},
    {"Empty","Empty","Empty"},

    {"Muon_Pt"               , "(100, 0, 1000)" , "muon pT"  },
    {"Muon_Eta"              , "(50, -3, 3)"    , "muon #eta"},
    {"Muon_Phi"              , "(50, -3, 3)"    , "muon #phi"},
    {"Empty","Empty","Empty"},
    
    {"dR_LQLQ"               , "(25,  0, 5)"    , "#Delta R (LQ,LQ)"                }, 
    {"dPhi_LQLQ"             , "(35, -3.5, 3.5)", "#Delta #phi (LQ,LQ)"             },
    {"dEta_LQLQ"             , "(35, 0, 3.5)", "#Delta #eta (LQ,LQ)"             },
    {"Empty","Empty","Empty"},

    // Delta R
    {"dR_HadBHadTau"            , "(25,  0, 5)"    , "#Delta R (b_{had},#tau)"                      },          
    {"dR_HadBHadTauNeu"         , "(25,  0, 5)"    , "#Delta R (b_{had},#tau_{had#nu})"             }, 
    {"dR_HadBLepB"              , "(25,  0, 5)"    , "#Delta R (b_{had},b_{lep})"                   }, 
    {"dR_HadBLepTau"            , "(25,  0, 5)"    , "#Delta R (b_{had},#tau_{lep})"                }, 

    {"dR_HadBLepTauNeu"         , "(25,  0, 5)"    , "#Delta R (b_{had},#nu_{#tau_{lep}})"          }, 
    {"dR_HadBLepLepNeu"         , "(25,  0, 5)"    , "#Delta R (b_{had},#nu_{lep})"                 }, 
    {"dR_HadBLepEleNeu"         , "(25,  0, 5)"    , "#Delta R (b_{had},#ell)"                      }, 
    {"dR_HadBLepMuNeu"          , "(25,  0, 5)"    , "#Delta R (b_{had},#mu)"                       }, 

    {"dR_HadBLepLep"            , "(25,  0, 5)"    , "#Delta R (b_{had},lepton)"                    },
    {"dR_HadBLepEle"            , "(25,  0, 5)"    , "#Delta R (b_{had},e)"                         },
    {"dR_HadBLepMu"             , "(25,  0, 5)"    , "#Delta R (b_{had},#mu)"                       },
    {"Empty","Empty","Empty"},

    {"dR_HadTauHadTauNeu"       , "(250, 0, 0.1)"  , "#Delta R (#tau_{had},#nu_{#tau_{had}})"       },
    {"dR_HadTauLepB"            , "(25,  0, 5)"    , "#Delta R (#tau_{had},b_{lep})"                },
    {"dR_HadTauLepTau"          , "(25,  0, 5)"    , "#Delta R (#tau_{had},#tau_{lep})"             },
    {"dR_HadTauLepTauNeu"       , "(25,  0, 5)"    , "#Delta R (#tau_{had},#nu_{#tau_{lep}})"       },

    {"dR_HadTauLepLepNeu"       , "(25,  0, 5)"    , "#Delta R (#tau_{had},#nu_{lep})"              },
    {"dR_HadTauLepEleNeu"       , "(25,  0, 5)"    , "#Delta R (#tau_{had},#nu_{e})"                },
    {"dR_HadTauLepMuNeu"        , "(25,  0, 5)"    , "#Delta R (#tau_{had},#nu_{#mu})"              },
    {"Empty","Empty","Empty"},
    
    {"dR_HadTauLepLep"          , "(25,  0, 5)"    , "#Delta R (#tau_{had},lepton)"                 },
    {"dR_HadTauLepEle"          , "(25,  0, 5)"    , "#Delta R (#tau_{had},ele)"                    },
    {"dR_HadTauLepMu"           , "(25,  0, 5)"    , "#Delta R (#tau_{had},#mu)"                    },
    {"Empty","Empty","Empty"},

    {"dR_HadTauNeuLepB"         , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},b_{lep})"          },
    {"dR_HadTauNeuLepTau"       , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#tau_{lep})"       },
    {"dR_HadTauNeuLepTauNeu"    , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#nu_{#tau_{lep}})" },
    {"dR_HadTauNeuLepLepNeu"    , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#nu_{lep})"        },

    {"dR_HadTauNeuLepEleNeu"    , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#nu_{ele})"        },
    {"dR_HadTauNeuLepMuNeu"     , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#nu_{#mu})"        },
    {"dR_HadTauNeuLepLep"       , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},lepton)"           },
    {"dR_HadTauNeuLepEle"       , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},ele)"              },

    {"dR_HadTauNeuLepMu"        , "(25,  0, 5)"    , "#Delta R (#nu_{#tau_{had}},#mu)"              },
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},

    {"dR_LepBLepTau"            , "(25,  0, 5)"    , "#Delta R (b_{lep},#tau_{lep})"                },
    {"dR_LepBLepTauNeu"         , "(25,  0, 5)"    , "#Delta R (b_{lep},#nu_{#tau_{lep}})"          },
    {"dR_LepBLepLepNeu"         , "(25,  0, 5)"    , "#Delta R (b_{lep},#nu_{lep})"                 },
    {"dR_LepBLepEleNeu"         , "(25,  0, 5)"    , "#Delta R (b_{lep},#nu_{ele})"                 },
                                                                                                   
    {"dR_LepBLepMuNeu"          , "(25,  0, 5)"    , "#Delta R (b_{lep},#nu_{mu})"                  },
    {"dR_LepBLepLep"            , "(25,  0, 5)"    , "#Delta R (b_{lep},lepton)"                    },
    {"dR_LepBLepEle"            , "(25,  0, 5)"    , "#Delta R (b_{lep},ele)"                       },
    {"dR_LepBLepMu"             , "(25,  0, 5)"    , "#Delta R (b_{lep},#mu)"                       },
                                                                                                   
    {"dR_LepTauLepTauNeu"       , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},#nu_{#tau_{lep}})"          },
    {"dR_LepTauLepLepNeu"       , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},#nu_{lep})"                 },
    {"dR_LepTauLepEleNeu"       , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},#nu_{ele})"                 },
    {"dR_LepTauLepMuNeu"        , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},#nu_{#mu})"                 },
                                                                                                   
    {"dR_LepTauLepLep"          , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},lepton)"                    },
    {"dR_LepTauLepEle"          , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},electron)"                  },
    {"dR_LepTauLepMu"           , "(100,  0, 0.1)" , "#Delta R (#tau_{lep},#mu)"                       },
    {"Empty","Empty","Empty"},                                                                     
                                                                                                   
    {"dR_LepTauNeuLepLepNeu"    , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},#nu_{lep})"       },
    {"dR_LepTauNeuLepEleNeu"    , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},#nu_{e})"         },
    {"dR_LepTauNeuLepMuNeu"     , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},#nu_{#mu})"       },
    {"Empty","Empty","Empty"},                                                                     
                                                                                                   
    {"dR_LepTauNeuLepLep"       , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},lepton)"           },
    {"dR_LepTauNeuLepEle"       , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},electron)"          },
    {"dR_LepTauNeuLepMu"        , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},#mu)"              },
    {"Empty","Empty","Empty"},                                                                     
                                                                                                   
    {"dR_LepLepNeuLepLep"       , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},lepton)"           },
    {"dR_LepLepNeuLepEle"       , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},electron)"         },
    {"dR_LepLepNeuLepMu"        , "(100,  0, 0.1)" , "#Delta R (#nu_{#tau_{lep}},#mu)"              },
    {"Empty","Empty","Empty"},                                                                     
                                                                                                   
    {"dR_LepEleNeuLepEle"       , "(100,  0, 0.1)" , "#Delta R (#nu_{e}, electron)"               },
    {"dR_LepMuNeuLepMu"         , "(100,  0, 0.1)" , "#Delta R (#nu_{#mu},#mu)"                   },
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},

    // Delta Phi
    {"dPhi_HadBHadTau"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#tau)"                      },
    {"dPhi_HadBHadTauNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#tau_{had#nu})"             },
    {"dPhi_HadBLepB"            , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},b_{lep})"                   },
    {"dPhi_HadBLepTau"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#tau_{lep})"                },
                                                                                                       
    {"dPhi_HadBLepTauNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#nu_{#tau_{lep}})"          },
    {"dPhi_HadBLepLepNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#nu_{lep})"                 },
    {"dPhi_HadBLepEleNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#ell)"                      },
    {"dPhi_HadBLepMuNeu"        , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#mu)"                       },
                                                                                                       
    {"dPhi_HadBLepLep"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},lepton)"                    },
    {"dPhi_HadBLepEle"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},e)"                         },
    {"dPhi_HadBLepMu"           , "(35, -3.5, 3.5)",   "#Delta #phi (b_{had},#mu)"                       },
    {"Empty","Empty","Empty"},                                                                         
                                                                                                       
    {"dPhi_HadTauHadTauNeu"     , "(200, -0.1, 0.1)",  "#Delta #phi (#tau_{had},#nu_{#tau_{had}})"       },
    {"dPhi_HadTauLepB"          , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},b_{lep})"                },
    {"dPhi_HadTauLepTau"        , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#tau_{lep})"             },
    {"dPhi_HadTauLepTauNeu"     , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#nu_{#tau_{lep}})"       },
                                                                                                       
    {"dPhi_HadTauLepLepNeu"     , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#nu_{lep})"              },
    {"dPhi_HadTauLepEleNeu"     , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#nu_{e})"                },
    {"dPhi_HadTauLepMuNeu"      , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#nu_{#mu})"              },
    {"Empty","Empty","Empty"},                                                                         
                                                                                                       
    {"dPhi_HadTauLepLep"        , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},lepton)"                 },
    {"dPhi_HadTauLepEle"        , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},ele)"                    },
    {"dPhi_HadTauLepMu"         , "(35, -3.5, 3.5)",   "#Delta #phi (#tau_{had},#mu)"                    },
    {"Empty","Empty","Empty"},                                                                         
                                                                                                       
    {"dPhi_HadTauNeuLepB"       , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},b_{lep})"          },
    {"dPhi_HadTauNeuLepTau"     , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#tau_{lep})"       },
    {"dPhi_HadTauNeuLepTauNeu"  , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#nu_{#tau_{lep}})" },
    {"dPhi_HadTauNeuLepLepNeu"  , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#nu_{lep})"        },
                                                                                                       
    {"dPhi_HadTauNeuLepEleNeu"  , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#nu_{ele})"        },
    {"dPhi_HadTauNeuLepMuNeu"   , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#nu_{#mu})"        },
    {"dPhi_HadTauNeuLepLep"     , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},lepton)"           },
    {"dPhi_HadTauNeuLepEle"     , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},ele)"              },
                                                                                                       
    {"dPhi_HadTauNeuLepMu"      , "(35, -3.5, 3.5)",   "#Delta #phi (#nu_{#tau_{had}},#mu)"              },
    {"Empty","Empty","Empty"},                                                                         
    {"Empty","Empty","Empty"},                                                                         
    {"Empty","Empty","Empty"},                                                                         
                                                                                                       
    {"dPhi_LepBLepTau"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#tau_{lep})"                },
    {"dPhi_LepBLepTauNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#nu_{#tau_{lep}})"          },
    {"dPhi_LepBLepLepNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#nu_{lep})"                 },
    {"dPhi_LepBLepEleNeu"       , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#nu_{ele})"                 },
                                                                                                    
    {"dPhi_LepBLepMuNeu"        , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#nu_{mu})"                  },
    {"dPhi_LepBLepLep"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},lepton)"                    },
    {"dPhi_LepBLepEle"          , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},ele)"                       },
    {"dPhi_LepBLepMu"           , "(35, -3.5, 3.5)",   "#Delta #phi (b_{lep},#mu)"                       },
                                                                                                    
    {"dPhi_LepTauLepTauNeu"     , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},#nu_{#tau_{lep}})"          },
    {"dPhi_LepTauLepLepNeu"     , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},#nu_{lep})"                 },
    {"dPhi_LepTauLepEleNeu"     , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},#nu_{ele})"                 },
    {"dPhi_LepTauLepMuNeu"      , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},#nu_{#mu})"                 },
                                                                                                    
    {"dPhi_LepTauLepLep"        , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},lepton)"                    },
    {"dPhi_LepTauLepEle"        , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},electron)"                  },
    {"dPhi_LepTauLepMu"         , "(100, -0.1, 0.1)",  "#Delta #phi (b_{had},#mu)"                       },
    {"Empty","Empty","Empty"},                                                                      
                                                                                                    
    {"dPhi_LepTauNeuLepLepNeu"  , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},#neu_{lep})"       },
    {"dPhi_LepTauNeuLepEleNeu"  , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},#neu_{ele})"       },
    {"dPhi_LepTauNeuLepMuNeu"   , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},#neu_{#mu})"       },
    {"Empty","Empty","Empty"},                                                                      
                                                                                                    
    {"dPhi_LepTauNeuLepLep"     , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},lepton)"           },
    {"dPhi_LepTauNeuLepEle"     , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},electron"          },
    {"dPhi_LepTauNeuLepMu"      , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#tau_{lep}},#mu)"              },
    {"Empty","Empty","Empty"},                                                                      
                                                                                                    
    {"dPhi_LepLepNeuLepLep"     , "(100,  0, 0.1)" ,   "#Delta #phi (#nu_{#tau_{lep}},lepton)"           },
    {"dPhi_LepLepNeuLepEle"     , "(100,  0, 0.1)" ,   "#Delta #phi (#nu_{#tau_{lep}},ele)"              },
    {"dPhi_LepLepNeuLepMu"      , "(100,  0, 0.1)" ,   "#Delta #phi (b_{had},#mu)"                       },
    {"Empty","Empty","Empty"},                                                                                 
                                                                                                    
    {"dPhi_LepEleNeuLepEle"     , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{ele}, electron)"               },
    {"dPhi_LepMuNeuLepMu"       , "(100, -0.1, 0.1)",  "#Delta #phi (#nu_{#mu},#mu)"                     },
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},

    {"dEta_HadBHadTau"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadBHadTauNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadBLepB"            , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },

    {"dEta_HadBLepTau"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadBLepTauNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadBLepLepNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadBLepEleNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },

    {"dEta_HadBLepMuNeu"        , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadBLepLep"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadBLepEle"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadBLepMu"           , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },

    {"dEta_HadTauHadTauNeu"     , "(350, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadTauLepB"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadTauLepTau"        , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadTauLepTauNeu"     , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    
    {"dEta_HadTauLepLepNeu"     , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadTauLepEleNeu"     , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadTauLepMuNeu"      , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadTauLepLep"        , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },

    {"dEta_HadTauLepEle"        , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadTauLepMu"         , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadTauNeuLepB"       , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadTauNeuLepTau"     , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },

    {"dEta_HadTauNeuLepTauNeu"  , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_HadTauNeuLepLepNeu"  , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadTauNeuLepEleNeu"  , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadTauNeuLepMuNeu"   , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },

    {"dEta_HadTauNeuLepLep"     , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_HadTauNeuLepEle"     , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_HadTauNeuLepMu"      , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepBLepTau"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },

    {"dEta_LepBLepTauNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepBLepLepNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepBLepEleNeu"       , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepBLepMuNeu"        , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },

    {"dEta_LepBLepLep"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepBLepEle"          , "(35, 0, 3.5)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepBLepMu"           , "(35, 0, 3.5)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepTauLepTauNeu"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },

    {"dEta_LepTauLepLepNeu"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepTauLepEleNeu"     , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepTauLepMuNeu"      , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepTauLepLep"        , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },

    {"dEta_LepTauLepEle"        , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepTauLepMu"         , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepTauNeuLepLepNeu"  , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepTauNeuLepEleNeu"  , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },

    {"dEta_LepTauNeuLepMuNeu"   , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepTauNeuLepLep"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepTauNeuLepEle"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepTauNeuLepMu"      , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },

    {"dEta_LepLepNeuLepLep"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepLepNeuLepEle"     , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepLepNeuLepMu"      , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepEleNeuLepMuNeu"   , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },

    {"dEta_LepEleNeuLepLep"     , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepEleNeuLepEle"     , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },
    {"dEta_LepEleNeuLepMu"      , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },
    {"dEta_LepMuNeuLepLep"      , "(100, 0, 0.1)", "#Delta #eta (#tau,#mu)"          },

    {"dEta_LepMuNeuLepEle"      , "(100, 0, 0.1)", "#Delta #eta (#tau,#ell)"         },
    {"dEta_LepMuNeuLepMu"       , "(100, 0, 0.1)", "#Delta #eta (#tau,ele)"          },
    {"Empty","Empty","Empty"},
    {"Empty","Empty","Empty"},
  };


  TFile* output = new TFile("/tmp/ktakeda_test.root","RECREATE");
  for ( int iVar=0; iVar< obj.size(); iVar++ ){
    for ( int iMass=0; iMass< mass_point.size(); iMass++){
      if ( obj[iVar][0] == "Empty" ) continue;
      std::string draw_option  = obj[iVar][0] + ">>" + obj[iVar][0] + "_M" +  mass_point[iMass] + obj[iVar][1];
      TCut draw_option2 = "";
      std::string hist_name   =  obj[iVar][0] + "_M"+ mass_point[iMass];          
      if ( obj[iVar][0].find("dR_")   != std::string::npos || 
           obj[iVar][0].find("dEta_") != std::string::npos ||
           obj[iVar][0].find("dPhi_") != std::string::npos 
          ){
	  std::string tmp = obj[iVar][0] + "<999";
	  draw_option2 = tmp.c_str();
      }
      std::cout << draw_option << std::endl;
      tree[iMass]->Draw( draw_option.c_str(), draw_option2 );
      TH1D* htemp = (TH1D*)gPad->GetPrimitive( hist_name.c_str() );
      htemp->SetXTitle( obj[iVar][0].c_str() );
      htemp->Write( hist_name.c_str() );
    }
  }
  output->Close();
  std::cout << "@@@@@@@ ROOT DRAW SCUCEEDED @@@@@@@" << std::endl;
  
  int numPage = 0;
  if ( obj.size()%4 == 0 ) numPage = obj.size()/4;
  else                     numPage = obj.size()/4+1;


  TFile* file = new TFile("/tmp/ktakeda_test.root", "READ");
  TCanvas* c1 = new TCanvas("c1", "c1");
  c1->SaveAs("merged.pdf[");
  for ( int iPage=0; iPage<numPage; iPage++) {
    c1->Clear();
    c1->Divide(2,2);

    for ( int iPad = 1; iPad<=4; iPad++){
      if( iPad-1+4*iPage > obj.size()-1 ) break;
      if ( obj[iPad-1 + 4*iPage][0] == "Empty" ) continue;

      c1->cd(iPad);
      TLatex* t = new TLatex(0.7,0.85, obj[iPad-1 + 4*iPage][2].c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(42);
      t->SetTextSize(0.05);
      t->SetTextAngle(0);
      for ( int iMass=0; iMass<5; iMass++){
        std::string hist_name = obj[ iPad-1 + 4*iPage][0] + "_M" + mass_point[iMass];
        TH1D* htemp = (TH1D*)file->Get( hist_name.c_str() );
        if ( iMass == 0 ) htemp->SetLineColor(kBlack);
        if ( iMass == 1 ) htemp->SetLineColor(kMagenta);
        if ( iMass == 2 ) htemp->SetLineColor(kBlue);
        if ( iMass == 3 ) htemp->SetLineColor(kGreen);
        if ( iMass == 4 ) htemp->SetLineColor(kRed);
        
        if ( iMass == 0 ) { htemp->Scale(1/htemp->Integral());htemp->GetYaxis()->SetRangeUser(0,htemp->GetBinContent(htemp->GetMaximumBin())+0.1);htemp->Draw("hist");}
        else              { htemp->Scale(1/htemp->Integral());htemp->Draw("same hist");}
        
      }
      if ( obj[ iPad-1 + 4*iPage][0].find("dEta_") == std::string::npos )
	  t->Draw();
    }
    c1->SaveAs("merged.pdf");
  }
  c1->SaveAs("merged.pdf]");
  
  TCanvas* c2 = new TCanvas("c2", "c2");
  std::vector<std::vector<std::string>> SumStr = {
      {"dR_LepLepNeuLepLep",    "#Delta R #left(lepton, #nu_{lep} #right)" },
      {"dR_LepTauNeuLepLep",    "#Delta R #left(lepton, #nu_{#tau} #right)" },
      {"dR_LepTauLepLep",       "#Delta R #left(lepton, #tau_{lep} #right)" },
      {"dR_LepTauNeuLepLepNeu", "#Delta R #left(#nu_{lep}, #nu_{#tau} #right)" },
      {"dR_LepTauLepLepNeu",    "#Delta R #left(#nu_{lep}, #tau_{lep} #right)" },
      {"dR_LepTauLepTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{lep} #right)" },
      {"dR_HadTauHadTauNeu",    "#Delta R #left(#nu_{#tau}, #tau_{had} #right)" },
  };
  EColor color[5] = { kBlack, kMagenta, kBlue, kGreen, kRed };
  c2->Divide(3,3);
  for ( int iVar=0; iVar<SumStr.size(); iVar++ ) {
      c2->cd(iVar+1);
      for ( int iMass=0; iMass<5; iMass++){
	  std::string hist_name = SumStr[iVar][0] + "_M" + mass_point[iMass];
	  TH1D* htemp = (TH1D*)file->Get( hist_name.c_str() );
	  htemp->SetLineColor(color[iMass]);
	  htemp->SetLineWidth(1);
	  htemp->Draw("hist same");
	  std::string mean_sigma = "#mu=" + std::to_string(htemp->GetMean()) + ", RMS=" + std::to_string(htemp->GetRMS());
	  TLatex* ms = new TLatex(0.7,0.75-iMass*0.07, mean_sigma.c_str());
	  ms->SetNDC(1);
	  ms->SetTextAlign(22);
	  ms->SetTextColor(kBlack);
	  ms->SetTextFont(42);
	  ms->SetTextSize(0.05);
	  ms->SetTextAngle(0);
	  ms->SetTextColor(color[iMass]);
	  ms->Draw();
      }
      TLatex* t = new TLatex(0.7,0.85, SumStr[iVar][1].c_str() );
      t->SetNDC(1);
      t->SetTextAlign(22);
      t->SetTextColor(kBlack);
      t->SetTextFont(42);
      t->SetTextSize(0.08);
      t->SetTextAngle(0);
      t->Draw();
      gPad->RedrawAxis();
  }
  c2->SaveAs("SummaryPlot.pdf");
}
