#include <AsgTools/MessageCheck.h>
#include <DxAODReader/DxAODReader.h>

#include "xAODEventInfo/EventInfo.h"

// Tau
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/TauDefs.h" 
#include "xAODTau/TauJet.h"

#include "xAODTau/TauTrack.h"
#include "xAODTau/TauTrackContainer.h"
#include "xAODTau/TauTrackAuxContainer.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"

#include "xAODCore/ShallowCopy.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"

// TauAnalysisTools (external package)


DxAODReader :: DxAODReader (const std::string& name,
    ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
{
}


StatusCode DxAODReader :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  m_true = new TTree("m_true","m_true");
  ANA_CHECK( histSvc()->regTree("/STREAM/m_true", m_true));
  m_true->Branch("eventNumber", &eventNumber, "eventNumber/I");

  m_true->Branch("EventFlag"  , &EventFlag  , "EventFlag/O"  );
  m_true->Branch("isLeptonEle", &isLeptonEle, "isLeptonEle/O");

  m_true->Branch("LQmass_lep"  , &LQmass_lep  , "LQmass_lep/D");
  m_true->Branch("LQmass_ele"  , &LQmass_ele  , "LQmass_ele/D");
  m_true->Branch("LQmass_mu"  , &LQmass_mu  , "LQmass_mu/D");
  m_true->Branch("LQmass_had"  , &LQmass_had  , "LQmass_had/D");
  
  // LQ(lep) invariant mass
  m_true->Branch("LQ_M_lep"           ,&LQ_M_lep           ,"LQ_M_lep/D");
  m_true->Branch("LQ_M_lep_direct"    ,&LQ_M_lep_direct    ,"LQ_M_lep_direct/D");
  m_true->Branch("LQ_M_lep_directLepJet",&LQ_M_lep_directLepJet ,"LQ_M_lep_directLepJet/D");
  m_true->Branch("LQ_M_lep_directEleJet",&LQ_M_lep_directEleJet ,"LQ_M_lep_directEleJet/D");
  m_true->Branch("LQ_M_lep_directMuJet" ,&LQ_M_lep_directMuJet  ,"LQ_M_lep_directMuJet/D");
  m_true->Branch("LQ_M_lep_directLepAntiKt4Jet",&LQ_M_lep_directLepAntiKt4Jet ,"LQ_M_lep_directLepAntiKt4Jet/D");
  m_true->Branch("LQ_M_lep_directEleAntiKt4Jet",&LQ_M_lep_directEleAntiKt4Jet ,"LQ_M_lep_directEleAntiKt4Jet/D");
  m_true->Branch("LQ_M_lep_directMuAntiKt4Jet" ,&LQ_M_lep_directMuAntiKt4Jet  ,"LQ_M_lep_directMuAntiKt4Jet/D");
  m_true->Branch("LQ_M_lep_TauJet"    ,&LQ_M_lep_TauJet    ,"LQ_M_lep_TauJet/D");
  m_true->Branch("LQ_M_lep_LepJet"    ,&LQ_M_lep_LepJet    ,"LQ_M_lep_LepJet/D");
  m_true->Branch("LQ_M_lep_EleJet"    ,&LQ_M_lep_EleJet    ,"LQ_M_lep_EleJet/D");
  m_true->Branch("LQ_M_lep_MuJet"     ,&LQ_M_lep_MuJet     ,"LQ_M_lep_MuJet/D");
  m_true->Branch("LQ_M_lep_minusTau"  ,&LQ_M_lep_minusTau  ,"LQ_M_lep_minusTau/D");
  m_true->Branch("LQ_M_lep_minusJet"  ,&LQ_M_lep_minusJet  ,"LQ_M_lep_minusJet/D");
  // LQ(had) invariant mass
  m_true->Branch("LQ_M_had"           ,&LQ_M_had           ,"LQ_M_had/D");
  m_true->Branch("LQ_M_had_direct"    ,&LQ_M_had_direct    ,"LQ_M_had_direct/D");
  m_true->Branch("LQ_M_had_TauJet"    ,&LQ_M_had_TauJet    ,"LQ_M_had_TauJet/D");
  m_true->Branch("LQ_M_had_VisTauJet" ,&LQ_M_had_VisTauJet ,"LQ_M_had_VisTauJet/D");
  m_true->Branch("LQ_M_had_VisTauAntiKt4Jet" ,&LQ_M_had_VisTauAntiKt4Jet ,"LQ_M_had_VisTauAntiKt4Jet/D");
  m_true->Branch("LQ_M_had_minusTau"  ,&LQ_M_had_minusTau  ,"LQ_M_had_minusTau/D");
  m_true->Branch("LQ_M_had_minusJet"  ,&LQ_M_had_minusJet  ,"LQ_M_had_minusJet/D");
 
  m_true->Branch("BJetLepM"           ,&BJetLepM           , "BJetLepM/D");
  m_true->Branch("BJetHadM"           ,&BJetHadM           , "BJetHadM/D");
  m_true->Branch("TauLepM"            ,&TauLepM            , "TauLepM/D");
  m_true->Branch("TauHadM"            ,&TauHadM            , "TauHadM/D");
 
  // dR
  m_true->Branch("dR_HadBTau"    , &dR_HadBTau    , "dR_HadBTau/D");
  m_true->Branch("dR_HadBLep"    , &dR_HadBLep    , "dR_HadBLep/D");
  m_true->Branch("dR_HadBEle"    , &dR_HadBEle    , "dR_HadBEle/D");
  m_true->Branch("dR_HadBMu"     , &dR_HadBMu     , "dR_HadBMu/D");
  m_true->Branch("dR_LepBTau"    , &dR_LepBTau    , "dR_LepBTau/D");
  m_true->Branch("dR_LepBLep"    , &dR_LepBLep    , "dR_LepBLep/D");
  m_true->Branch("dR_LepBEle"    , &dR_LepBEle    , "dR_LepBEle/D");
  m_true->Branch("dR_LepBMu"     , &dR_LepBMu     , "dR_LepBMu/D");
  m_true->Branch("dR_TauLep"     , &dR_TauLep     , "dR_TauLep/D");
  m_true->Branch("dR_TauEle"     , &dR_TauEle     , "dR_TauEle/D");
  m_true->Branch("dR_TauMu"      , &dR_TauMu      , "dR_TauMu/D");
  m_true->Branch("dR_LepBHadB"   , &dR_LepBHadB   , "dR_LepBHadB/D");
  m_true->Branch("dR_LQLQ"       , &dR_LQLQ       , "dR_LQLQ/D");
  // dPhi
  m_true->Branch("dPhi_HadBTau"  , &dPhi_HadBTau  , "dPhi_HadBTau/D");
  m_true->Branch("dPhi_HadBLep"  , &dPhi_HadBLep  , "dPhi_HadBLep/D");
  m_true->Branch("dPhi_HadBEle"  , &dPhi_HadBEle  , "dPhi_HadBEle/D");
  m_true->Branch("dPhi_HadBMu"   , &dPhi_HadBMu   , "dPhi_HadBMu/D");
  m_true->Branch("dPhi_LepBTau"  , &dPhi_LepBTau  , "dPhi_LepBTau/D");
  m_true->Branch("dPhi_LepBLep"  , &dPhi_LepBLep  , "dPhi_LepBLep/D");
  m_true->Branch("dPhi_LepBEle"  , &dPhi_LepBEle  , "dPhi_LepBEle/D");
  m_true->Branch("dPhi_LepBMu"   , &dPhi_LepBMu   , "dPhi_LepBMu/D");
  m_true->Branch("dPhi_TauLep"   , &dPhi_TauLep   , "dPhi_TauLep/D");
  m_true->Branch("dPhi_TauEle"   , &dPhi_TauEle   , "dPhi_TauEle/D");
  m_true->Branch("dPhi_TauMu"    , &dPhi_TauMu    , "dPhi_TauMu/D");
  m_true->Branch("dPhi_LepBHadB" , &dPhi_LepBHadB , "dPhi_LepBHadB/D");
  m_true->Branch("dPhi_LQLQ"     , &dPhi_LQLQ     , "dPhi_LQLQ/D");
  // dEta
  m_true->Branch("dEta_HadBTau"  , &dEta_HadBTau  , "dEta_HadBTau/D");
  m_true->Branch("dEta_HadBLep"  , &dEta_HadBLep  , "dEta_HadBLep/D");
  m_true->Branch("dEta_HadBEle"  , &dEta_HadBEle  , "dEta_HadBEle/D");
  m_true->Branch("dEta_HadBMu"   , &dEta_HadBMu   , "dEta_HadBMu/D");
  m_true->Branch("dEta_LepBTau"  , &dEta_LepBTau  , "dEta_LepBTau/D");
  m_true->Branch("dEta_LepBLep"  , &dEta_LepBLep  , "dEta_LepBLep/D");
  m_true->Branch("dEta_LepBEle"  , &dEta_LepBEle  , "dEta_LepBEle/D");
  m_true->Branch("dEta_LepBMu"   , &dEta_LepBMu   , "dEta_LepBMu/D");
  m_true->Branch("dEta_TauLep"   , &dEta_TauLep   , "dEta_TauLep/D");
  m_true->Branch("dEta_TauEle"   , &dEta_TauEle   , "dEta_TauEle/D");
  m_true->Branch("dEta_TauMu"    , &dEta_TauMu    , "dEta_TauMu/D");
  m_true->Branch("dEta_LepBHadB" , &dEta_LepBHadB , "dEta_LepBHadB/D");
  m_true->Branch("dEta_LQLQ"     , &dEta_LQLQ     , "dEta_LQLQ/D");
  
  // Truth 
  m_true->Branch("TauPt_truth"     , &TauPt_truth     , "TauPt_truth/D");
  m_true->Branch("ElectronPt_truth", &ElectronPt_truth, "ElectronPt_truth/D");
  m_true->Branch("MuonPt_truth"    , &MuonPt_truth    , "MuonPt_truth/D");
  m_true->Branch("HadBJetPt_truth" , &HadBJetPt_truth , "HadBJetPt_truth/D");
  m_true->Branch("LepBJetPt_truth" , &LepBJetPt_truth , "LepBJetPt_truth/D");
  
  m_true->Branch("TauPt", &TauPt, "TauPt/D");
  m_true->Branch("ElectronPt", &ElectronPt, "ElectronPt/D");
  m_true->Branch("MuonPt", &MuonPt, "MuonPt/D");
  m_true->Branch("HadBJetPt", &HadBJetPt, "HadBJetPt/D");
  m_true->Branch("LepBJetPt", &LepBJetPt, "LepBJetPt/D");
  
  m_true->Branch("TauEta", &TauEta, "TauEta/D");
  m_true->Branch("ElectronEta", &ElectronEta, "ElectronEta/D");
  m_true->Branch("MuonEta", &MuonEta, "MuonEta/D");
  m_true->Branch("HadBJetEta", &HadBJetEta, "HadBJetEta/D");
  m_true->Branch("LepBJetEta", &LepBJetEta, "LepBJetEta/D");
  
  m_true->Branch("TauPhi", &TauPhi, "TauPhi/D");
  m_true->Branch("ElectronPhi", &ElectronPhi, "ElectronPhi/D");
  m_true->Branch("MuonPhi", &MuonPhi, "MuonPhi/D");
  m_true->Branch("HadBJetPhi", &HadBJetPhi, "HadBJetPhi/D");
  m_true->Branch("LepBJetPhi", &LepBJetPhi, "LepBJetPhi/D");

  m_true->Branch("LQHad_P"         ,&LQHad_P        , "LQHad_P/D" );
  m_true->Branch("LQHad_Pt"         ,&LQHad_Pt        , "LQHad_Pt/D" );
  m_true->Branch("LQHad_Eta"        ,&LQHad_Eta       , "LQHad_Eta/D");
  m_true->Branch("LQHad_Phi"        ,&LQHad_Phi       , "LQHad_Phi/D");
  m_true->Branch("LQLep_P"         ,&LQLep_P        , "LQLep_P/D" );
  m_true->Branch("LQLep_Pt"         ,&LQLep_Pt        , "LQLep_Pt/D" );
  m_true->Branch("LQLep_Eta"        ,&LQLep_Eta       , "LQLep_Eta/D");
  m_true->Branch("LQLep_Phi"        ,&LQLep_Phi       , "LQLep_Phi/D");
  m_true->Branch("BottomHad_P"     ,&BottomHad_P    , "BottomHad_P/D" );
  m_true->Branch("BottomHad_Pt"     ,&BottomHad_Pt    , "BottomHad_Pt/D" );
  m_true->Branch("BottomHad_Eta"    ,&BottomHad_Eta   , "BottomHad_Eta/D");
  m_true->Branch("BottomHad_Phi"    ,&BottomHad_Phi   , "BottomHad_Phi/D");
  m_true->Branch("BottomLep_P"     ,&BottomLep_P    , "BottomLep_P/D" );
  m_true->Branch("BottomLep_Pt"     ,&BottomLep_Pt    , "BottomLep_Pt/D" );
  m_true->Branch("BottomLep_Eta"    ,&BottomLep_Eta   , "BottomLep_Eta/D");
  m_true->Branch("BottomLep_Phi"    ,&BottomLep_Phi   , "BottomLep_Phi/D");
  m_true->Branch("VisTau_P"        ,&VisTau_P       , "VisTau_P/D"    );
  m_true->Branch("VisTau_Pt"        ,&VisTau_Pt       , "VisTau_Pt/D"    );
  m_true->Branch("VisTau_Eta"       ,&VisTau_Eta      , "VisTau_Eta/D"   );
  m_true->Branch("VisTau_Phi"       ,&VisTau_Phi      , "VisTau_Phi/D"   );
  m_true->Branch("Lep_P"           ,&Lep_P          , "Lep_P/D"      );
  m_true->Branch("Lep_Pt"          ,&Lep_Pt         , "Lep_Pt/D"      );
  m_true->Branch("Lep_Eta"         ,&Lep_Eta        , "Lep_Eta/D"     );
  m_true->Branch("Lep_Phi"         ,&Lep_Phi        , "Lep_Phi/D"     );
  m_true->Branch("Electron_P"      ,&Electron_P     , "Electron_P/D"  );
  m_true->Branch("Electron_Pt"      ,&Electron_Pt     , "Electron_Pt/D"  );
  m_true->Branch("Electron_Eta"     ,&Electron_Eta    , "Electron_Eta/D" );
  m_true->Branch("Electron_Phi"     ,&Electron_Phi    , "Electron_Phi/D" );
  m_true->Branch("Muon_P"          ,&Muon_P         , "Muon_P/D"      );
  m_true->Branch("Muon_Pt"          ,&Muon_Pt         , "Muon_Pt/D"      );
  m_true->Branch("Muon_Eta"         ,&Muon_Eta        , "Muon_Eta/D"     );
  m_true->Branch("Muon_Phi"         ,&Muon_Phi        , "Muon_Phi/D"     );

  m_true->Branch("HadTauNeu_P"     , &HadTauNeu_P   , "HadTauNeu_P/D");
  m_true->Branch("HadTauNeu_Pt"     , &HadTauNeu_Pt   , "HadTauNeu_Pt/D");
  m_true->Branch("HadTauNeu_Eta"    , &HadTauNeu_Eta  , "HadTauNeu_Eta/D");
  m_true->Branch("HadTauNeu_Phi"    , &HadTauNeu_Phi  , "HadTauNeu_Phi/D");
  m_true->Branch("LepTauNeu_P"     , &LepTauNeu_P   , "LepTauNeu_P/D");
  m_true->Branch("LepTauNeu_Pt"     , &LepTauNeu_Pt   , "LepTauNeu_Pt/D");
  m_true->Branch("LepTauNeu_Eta"    , &LepTauNeu_Eta  , "LepTauNeu_Eta/D");
  m_true->Branch("LepTauNeu_Phi"    , &LepTauNeu_Phi  , "LepTauNeu_Phi/D");
  m_true->Branch("LepEleNeu_P"     , &LepEleNeu_P   , "LepEleNeu_P/D");
  m_true->Branch("LepEleNeu_Pt"     , &LepEleNeu_Pt   , "LepEleNeu_Pt/D");
  m_true->Branch("LepEleNeu_Eta"    , &LepEleNeu_Eta  , "LepEleNeu_Eta/D");
  m_true->Branch("LepEleNeu_Phi"    , &LepEleNeu_Phi  , "LepEleNeu_Phi/D");
  m_true->Branch("LepMuNeu_P;"     , &LepMuNeu_P    , "LepMuNeu_P/D");
  m_true->Branch("LepMuNeu_Pt;"     , &LepMuNeu_Pt    , "LepMuNeu_Pt/D");
  m_true->Branch("LepMuNeu_Eta;"    , &LepMuNeu_Eta   , "LepMuNeu_Eta/D");
  m_true->Branch("LepMuNeu_Phi;"    , &LepMuNeu_Phi   , "LepMuNeu_Phi/D");

  // Collinear mass
  m_true->Branch("Lep_theta"    ,  &Lep_theta    , "Lep_theta/D" );
  m_true->Branch("Lep_phi"      ,  &Lep_phi      , "Lep_phi/D" );
  m_true->Branch("Ele_theta"    ,  &Ele_theta    , "Ele_theta/D" );
  m_true->Branch("Ele_phi"      ,  &Ele_phi      , "Ele_phi/D" );
  m_true->Branch("Mu_theta"     ,  &Mu_theta     , "Mu_theta/D" );
  m_true->Branch("Mu_phi"       ,  &Mu_phi       , "Mu_phi/D" );
  m_true->Branch("TauJet_P"     ,  &TauJet_P     , "TauJet_P/D" );
  m_true->Branch("TauJet_theta" ,  &TauJet_theta , "TauJet_theta/D" );
  m_true->Branch("TauJet_phi"   ,  &TauJet_phi   , "TauJet_phi/D" );
  m_true->Branch("MET_x"        ,  &MET_x        , "MET_x/D" );
  m_true->Branch("MET_y"        ,  &MET_y        , "MET_y/D" );
  m_true->Branch("MET_phi"        ,  &MET_phi        , "MET_phi/D" );
  m_true->Branch("NonIntMET_x"        ,  &NonIntMET_x        , "NonIntMET_x/D" );
  m_true->Branch("NonIntMET_y"        ,  &NonIntMET_y        , "NonIntMET_y/D" );
  m_true->Branch("NonIntMET_phi"      ,  &NonIntMET_phi      , "NonIntMET_phi/D" );
  m_true->Branch("IntOutMET_x"        ,  &IntOutMET_x        , "IntOutMET_x/D" );
  m_true->Branch("IntOutMET_y"        ,  &IntOutMET_y        , "IntOutMET_y/D" );
  m_true->Branch("IntOutMET_phi"        ,  &IntOutMET_phi        , "IntOutMET_phi/D" );

  m_true->Branch("RecoMET_x"        ,  &RecoMET_x        , "RecoMET_x/D" );
  m_true->Branch("RecoMET_y"        ,  &RecoMET_y        , "RecoMET_y/D" );
  m_true->Branch("RecoMET_phi"        ,  &RecoMET_phi        , "RecoMET_phi/D" );

  m_true->Branch("TLV_Lepton"    , "TLorentzVector", &TLV_Lepton);
  m_true->Branch("TLV_Electron"  , "TLorentzVector", &TLV_Electron);
  m_true->Branch("TLV_Muon"      , "TLorentzVector", &TLV_Muon);
  m_true->Branch("TLV_TauJet"    , "TLorentzVector", &TLV_TauJet);
  m_true->Branch("TLV_LepNeu"    , "TLorentzVector", &TLV_LepNeu);
  m_true->Branch("TLV_EleNeu"    , "TLorentzVector", &TLV_EleNeu);
  m_true->Branch("TLV_MuNeu"     , "TLorentzVector", &TLV_MuNeu);
  m_true->Branch("TLV_HadTauNeu" , "TLorentzVector", &TLV_HadTauNeu);
  m_true->Branch("TLV_LepTauNeu" , "TLorentzVector", &TLV_LepTauNeu);
  m_true->Branch("TLV_HadBJet"   , "TLorentzVector", &TLV_HadBJet);
  m_true->Branch("TLV_LepBJet"   , "TLorentzVector", &TLV_LepBJet);
  m_true->Branch("TLV_HadBQuark"   , "TLorentzVector", &TLV_HadBQuark);
  m_true->Branch("TLV_LepBQuark"   , "TLorentzVector", &TLV_LepBQuark);
  
  // Reco object information 
  m_true->Branch("Reco_Electron_Pt"         , &Reco_Electron_Pt   );
  m_true->Branch("Reco_Electron_Eta"        , &Reco_Electron_Eta  );
  m_true->Branch("Reco_Electron_Phi"        , &Reco_Electron_Phi  );
  m_true->Branch("Reco_Electron_M"          , &Reco_Electron_M    );
  m_true->Branch("Reco_Electron_truthPdgId" , &Reco_Electron_truthPdgId);
  m_true->Branch("Reco_Electron_charge"     , &Reco_Electron_charge);
  m_true->Branch("Reco_Electron_dRTruthEle" , &Reco_Electron_dRTruthEle);
  
  m_true->Branch("Reco_Muon_Pt"             , &Reco_Muon_Pt   );
  m_true->Branch("Reco_Muon_Eta"            , &Reco_Muon_Eta  );
  m_true->Branch("Reco_Muon_Phi"            , &Reco_Muon_Phi  );
  m_true->Branch("Reco_Muon_M"              , &Reco_Muon_M    );
  m_true->Branch("Reco_Muon_truthPdgId"     , &Reco_Muon_truthPdgId);
  m_true->Branch("Reco_Muon_charge"         , &Reco_Muon_charge);
  m_true->Branch("Reco_Muon_dRTruthMu"      , &Reco_Muon_dRTruthMu);
//  m_true->Branch("Reco_Lepton_isLoose" , &Reco_Lepton_isLoose);
//  m_true->Branch("Reco_Lepton_isMedium", &Reco_Lepton_isMedium);
//  m_true->Branch("Reco_Lepton_isTight" , &Reco_Lepton_isTight);

  m_true->Branch("Reco_TauJet_Pt"             , &Reco_TauJet_Pt  );
  m_true->Branch("Reco_TauJet_Eta"            , &Reco_TauJet_Eta );
  m_true->Branch("Reco_TauJet_Phi"            , &Reco_TauJet_Phi );
  m_true->Branch("Reco_TauJet_M"              , &Reco_TauJet_M   );
  m_true->Branch("Reco_TauJet_charge"         , &Reco_TauJet_charge   );
  m_true->Branch("Reco_TauJet_dRTruthTauJet"  , &Reco_TauJet_dRTruthTauJet );
  m_true->Branch("Reco_TauJet_truthPdgId" , &Reco_TauJet_truthPdgId );
//  m_true->Branch("Reco_TauJet_BDTJetScore"    , &Reco_TauJet_BDTJetScore );

  m_true->Branch("Reco_BJet_Pt"             , &Reco_BJet_Pt );
  m_true->Branch("Reco_BJet_Eta"            , &Reco_BJet_Eta);
  m_true->Branch("Reco_BJet_Phi"            , &Reco_BJet_Phi);
  m_true->Branch("Reco_BJet_M"              , &Reco_BJet_M  );
  m_true->Branch("Reco_BJet_dRTruthHadBJet" , &Reco_BJet_dRTruthHadBJet  );
  m_true->Branch("Reco_BJet_dRTruthLepBJet" , &Reco_BJet_dRTruthLepBJet  );
  m_true->Branch("Reco_BJet_truthPdgId"     , &Reco_BJet_truthPdgId  );
  
  m_true->Branch("dPhiMet_1stQuad" , &dPhiMet_1stQuad , "dPhiMet_1stQuad/D");
  m_true->Branch("dPhiMet_2ndQuad" , &dPhiMet_2ndQuad , "dPhiMet_2ndQuad/D");
  m_true->Branch("dPhiMet_3rdQuad" , &dPhiMet_3rdQuad , "dPhiMet_3rdQuad/D");
  m_true->Branch("dPhiMet_4thQuad" , &dPhiMet_4thQuad , "dPhiMet_4thQuad/D");
  
  m_true->Branch("MetProjection_1stQuad" , &MetProjection_1stQuad , "MetProjection_1stQuad/D");
  m_true->Branch("MetProjection_2ndQuad" , &MetProjection_2ndQuad , "MetProjection_2ndQuad/D");
  m_true->Branch("MetProjection_3rdQuad" , &MetProjection_3rdQuad , "MetProjection_3rdQuad/D");
  m_true->Branch("MetProjection_4thQuad" , &MetProjection_4thQuad , "MetProjection_4thQuad/D");

  std::vector<TString> Object =
  {
    /* had-side */
    "HadB",     // truthBJetHad,
    "HadTau",   // truthDirectTauHad,
    "HadTauNeu",// truthDirectHadTauNeu,

    /* lep-side */
    "LepB",     // truthBJetLep,
    "LepTau",   // truthDirectTauLep,
    "LepTauNeu",// truthDirectLepTauNeu,
    "LepLepNeu",// truthDirectLepLepNeu,
    "LepEleNeu",// truthDirectLepEleNeu,
    "LepMuNeu", // truthDirectLepMuNeu,
    "LepLep",   // truthLepton,
    "LepEle",   // truthElectron,
    "LepMu",    // truthMuon,
  };
  int iObjectIndex = 0;
  for ( int iObj=0; iObj<Object.size(); iObj++){
    for ( int jObj=iObj+1; jObj<Object.size(); jObj++){
      m_true->Branch( "dR_"   + Object[iObj]+Object[jObj], &DeltaR  [iObjectIndex], Object[iObj]+Object[jObj]+ "/D");
      m_true->Branch( "dPhi_" + Object[iObj]+Object[jObj], &DeltaPhi[iObjectIndex], Object[iObj]+Object[jObj]+ "/D");
      m_true->Branch( "dEta_" + Object[iObj]+Object[jObj], &DeltaEta[iObjectIndex], Object[iObj]+Object[jObj]+ "/D");
      iObjectIndex++;
    }
  }
  
  //m_true->Branch("tmp", &tmp, "tmp/D");

  return StatusCode::SUCCESS;
}

StatusCode DxAODReader :: execute ()
{
  static int process_event = 0;
  if ( process_event % 1000 == 0 ) std::cout << "Processing events : " << process_event << std::endl;
  process_event++;

  /* This SCAN_XAOD part can be used to skim xAOD.
     In particular, this partis for InDetParticles before any reconstruction requirement.
     */
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve(ei, "EventInfo"));

  eventNumber=ei->eventNumber();

  const xAOD::TruthParticleContainer* truth = 0;
  ATH_CHECK( evtStore()->retrieve( truth, "TruthParticles"));


  //const xAOD::TrackMeasurementValidationContainer* pixCluster=0;
  //ATH_CHECK( evtStore()->retrieve(pixCluster, "PixelClusters"));

  /************************************/
  /*     Truth lep-had selection      */
  /*                                  */
  /*  LQ      : denoted as LQ0        */
  /*  anti LQ : denoted as LQ1        */
  /************************************/
  int bottom_fromLQ1 = 0;
  int bottom_fromLQ2 = 0;
  std::stringstream ss;
  const xAOD::TruthParticle* truthBJetLepPair   = nullptr; 
  const xAOD::TruthParticle* truthBJetHadPair   = nullptr; 
  const xAOD::TruthParticle* truthTauHad        = nullptr; 
  const xAOD::TruthParticle* truthTauLep        = nullptr; 
  const xAOD::TruthParticle* truthMuon          = nullptr;
  const xAOD::TruthParticle* truthElectron      = nullptr;
  const xAOD::TruthParticle* truthLepton        = nullptr;

  const xAOD::TruthParticle* truthDirectTauLep    = nullptr;
  const xAOD::TruthParticle* truthDirectBJetLep   = nullptr;
  const xAOD::Jet*           truthBJetLep         = nullptr;
  const xAOD::TruthParticle* truthDirectTauHad    = nullptr;
  const xAOD::TruthParticle* truthDirectBJetHad   = nullptr;
  const xAOD::Jet*           truthBJetHad         = nullptr;
  const xAOD::TruthParticle* truthDirectHadTauNeu = nullptr;
  const xAOD::TruthParticle* truthDirectLepTauNeu = nullptr;
  const xAOD::TruthParticle* truthDirectLepLepNeu = nullptr;
  const xAOD::TruthParticle* truthDirectLepEleNeu = nullptr;
  const xAOD::TruthParticle* truthDirectLepMuNeu  = nullptr;
  const xAOD::Jet*           truthTauJet          = nullptr;
  TLorentzVector truthDirectVisTauHad;

  bool isLQTauHad      = true;  
  bool isLQTauLep      = false;  
  bool isAntiLQTauHad  = true;  
  bool isAntiLQTauLep  = false;

  const xAOD::TruthParticle* LQlep = nullptr;
  const xAOD::TruthParticle* LQhad = nullptr;

  for ( unsigned int iLqAntiLq=0; iLqAntiLq<2; iLqAntiLq++){
    int LEPTOQUARK;
    if      ( iLqAntiLq == 0 ) LEPTOQUARK =  43;
    else if ( iLqAntiLq == 1 ) LEPTOQUARK = -43;

    for ( auto pTruth : *truth ) {
      /* Search target Leptoquark 
       *  This sample contains some LQs (pdg id =43). Each LQ doesn't decay into b+tau, but only radiate some gammas.
       *  Thus you should choose "the decay LQ" by looping the child particles.
       */
      /* (1) The target LQ has at least two child particles. */
      if ( pTruth->absPdgId() != LEPTOQUARK || pTruth->nChildren() < 2) continue;

      const xAOD::TruthParticle* tmp_tau  = nullptr;
      const xAOD::TruthParticle* tmp_bjet = nullptr;
      /* (2) The child particles from the target LQ is bottom and tau-lepton. */
      const int TAU = 15;
      const int BOTTOM = 5;
      /* (2-1) Search tau */ 
      const xAOD::TruthParticle* LQtau = nullptr;
      for ( int iChild=0; iChild< pTruth->nChildren(); iChild++ ) {
        if ( pTruth->child(iChild)  == NULL ) continue;
        if ( pTruth->child(iChild)->absPdgId() != TAU )  continue;
        LQtau   = pTruth->child(iChild);
        tmp_tau = pTruth->child(iChild);
      }
      /* (2-2) Search bottom */ 
      const xAOD::TruthParticle* LQbottom = nullptr;
      for ( int iChild=0; iChild< pTruth->nChildren(); iChild++ ) {
        if ( pTruth->child(iChild)  == NULL ) continue;
        if ( pTruth->child(iChild)->absPdgId() != BOTTOM )  continue;
        LQbottom = pTruth->child(iChild);
        tmp_bjet = pTruth->child(iChild);
      }

      // Select the LQ->b+tau decay mode.
      // If a LQ doesn't decay into b or tau, the events should be rejected. 
      // We need only the LQ(b+tau)LQ(b+tau).
      if ( LQbottom == nullptr || LQtau == nullptr ) return StatusCode::SUCCESS;

      /* (3)
       * The tau, which is from the LQ decay directly, won't be the same one with the TauJet tau.
       * This is because the direct-tau will decay after some radiations.
       * Thus we must find the final state tau by using the following logics. (Or you can consider the position matching.)
       */
      bool SearchFinalTau = true;
      while( SearchFinalTau ) {
        SearchFinalTau = false;
        for ( int i=0; i<LQtau->nChildren(); i++ ){
          if ( LQtau->child(i)->absPdgId() != 15 ) continue;
          LQtau = LQtau->child(i);
          SearchFinalTau = true;
        }
      }

      /* (3-1) Classify the decay mode */
      bool monitorFlag = false;
      bool isTauHad = true;
      bool isTauLep = false;
      for (unsigned int iChild=0; iChild< LQtau->nChildren(); ++iChild) {
        const xAOD::TruthParticle* child = (const xAOD::TruthParticle*) LQtau->child(iChild);     
        if (!child) continue;
        if (child->absPdgId() == 11 || child->absPdgId() == 13){
          monitorFlag = true;

          truthLepton = (const xAOD::TruthParticle*) child;
          if      ( child->absPdgId() == 11 ) truthElectron = (const xAOD::TruthParticle*) child;
          else if ( child->absPdgId() == 13 ) truthMuon     = (const xAOD::TruthParticle*) child;

          isTauLep = true;
          isTauHad = false;
        }
      }
      if      ( isTauHad ){
	  // Store the hadronic-tau neutorino by for loop
	  for (unsigned int iChild=0; iChild< LQtau->nChildren(); ++iChild) {
	      const xAOD::TruthParticle* child = (const xAOD::TruthParticle*) LQtau->child(iChild);     
	      if      (child->absPdgId() == 16 ){
		  truthDirectHadTauNeu = (const xAOD::TruthParticle*) child;
	      }
	  }
	  // Choose the truth Tau Jets from the AntiKt4TruthJets container
	  const xAOD::JetContainer* jetContainer = nullptr;
	  ATH_CHECK( evtStore()->retrieve( jetContainer, "AntiKt4TruthJets" ));
	  double dPtMax = 0;
	  const xAOD::Jet* xTruthJetMatch = nullptr;
	  for (auto xTruthJetIt : *jetContainer) {
	      if (LQtau->p4().DeltaR(xTruthJetIt->p4()) <= 0.2) {
		  if (xTruthJetIt->pt()<dPtMax)
		      continue;
		  xTruthJetMatch = xTruthJetIt;
		  dPtMax = xTruthJetIt->pt();
	      }
	  }
	  if (xTruthJetMatch) {
	      truthTauJet = xTruthJetMatch;
	  } 
      }
      else if ( isTauLep ){
        for (unsigned int iChild=0; iChild< LQtau->nChildren(); ++iChild) {
          const xAOD::TruthParticle* child = (const xAOD::TruthParticle*) LQtau->child(iChild);     
          if (child->absPdgId() == 12 || child->absPdgId() == 14) {
            truthDirectLepLepNeu = (const xAOD::TruthParticle*) child;
          }
          if (child->absPdgId() == 12) {
            truthDirectLepEleNeu = (const xAOD::TruthParticle*) child;
          }
          if (child->absPdgId() == 14) {
            truthDirectLepMuNeu  = (const xAOD::TruthParticle*) child;
          }
          if (child->absPdgId() == 16) {
            truthDirectLepTauNeu  = (const xAOD::TruthParticle*) child;
          }
        }
      }

      /* (4) Find the final b-jet.
       *
       * This b-jet also radiates the some particles and its momentum will be reduced. 
       * We should choose the final b-jet which will create the jet structures. 
       * Such b-jet has a lot of children, ex about 40 particles, but we can't access these particles. 
       * Thus we must check if the child pointer is null or not.*/
      bool FindFinalBJet = true;
      while( FindFinalBJet ) {
        FindFinalBJet = false;
        for ( int i=0; i<LQbottom->nChildren(); i++){
          if ( LQbottom->child(i) == nullptr ) continue;
          if ( LQbottom->child(i)->absPdgId() != 5 ) continue;

          LQbottom = LQbottom->child(i);
          FindFinalBJet = true;
          break;
        }

        if ( LQbottom->nChildren() > 10 ) break; 
      }
      /* (4-1) Find b-jet from Jet Container */
      const xAOD::JetContainer* jetContainer = nullptr;
      ATH_CHECK( evtStore()->retrieve( jetContainer, "AntiKt4TruthJets" ));
      double dPtMax = 0;
      const xAOD::Jet* xTruthJetMatch = nullptr;
      for (auto xTruthJetIt : *jetContainer) {
	  if (LQbottom->p4().DeltaR(xTruthJetIt->p4()) <= 0.2) {
	      if (xTruthJetIt->pt()<dPtMax)
		  continue;
	      xTruthJetMatch = xTruthJetIt;
	      dPtMax = xTruthJetIt->pt();
	  }
      }
      if (xTruthJetMatch) {
	  if      ( isTauHad ) truthBJetHad = xTruthJetMatch;
	  else if ( isTauLep ) truthBJetLep = xTruthJetMatch;
      } 

      if      ( isTauLep ) {
        LQlep              = pTruth; 
        truthBJetLepPair   = LQbottom;
        truthTauLep        = LQtau;
        truthDirectTauLep  = tmp_tau;
        truthDirectBJetLep = tmp_bjet;
      }
      else if ( isTauHad ) { 
        LQhad              = pTruth; 
        truthBJetHadPair   = LQbottom;
        truthTauHad        = LQtau;
        
        truthDirectVisTauHad.SetPtEtaPhiM(0,0,0,0);
        for ( unsigned int iVisTauHad=0; iVisTauHad<truthTauHad->nChildren(); iVisTauHad++){
          const xAOD::TruthParticle* child = (const xAOD::TruthParticle*) truthTauHad->child(iVisTauHad);     
          if ( !child->isHadron() ) continue;
          truthDirectVisTauHad += child->p4();
        }
        truthDirectTauHad  = tmp_tau;
        truthDirectBJetHad = tmp_bjet;

        if ( monitorFlag ) { std::cerr << "The tau decays leptonicaly, but it is considered as the hadronic tau." << std::endl;}
      }
    }
  }
  /* Select the LQLQ->btau+btau (lephad) event */
  if ( LQlep == nullptr || LQhad == nullptr ) return StatusCode::SUCCESS;

  /* Remove the fail event. (fail event = The b-jet isn't choosed correctly.)  */
  if ( truthBJetLepPair == nullptr || truthBJetHadPair == nullptr ) return StatusCode::SUCCESS;

  // There a 4 definition for the Truth MET:
  //NonInt   - all stable, non-interacting particles including neutrinos, SUSY LSPs, Kaluza-Klein particles etc.
  //Int      - all stable, interacting particles within detector acceptance (|eta|<5) excluding muons (approximate calo term)
  //IntOut   - all stable, interacting particles outside detector acceptance (|eta|>5)
  //IntMuons - all final state muons in |eta|<5 and with pt>6 GeV
  //the truth MET is calculated as being the sum of 'Int' and 'IntMuons' informations, to be coherent with the definiton of the Truth MET in the TruthFramework 
  
  const xAOD::MissingETContainer * METContainer = nullptr;
  ATH_CHECK( evtStore()->retrieve( METContainer, "MET_Truth" ));
  double metX = 0;
  double metY = 0;
  for ( auto pMET : *METContainer){
      if ( pMET->name() == "Int" || pMET->name() == "IntMuons" ) {
	  metX += pMET->mpx();
	  metY += pMET->mpy();
//	   std::cout << pMET->name() << " : mpx=" << pMET->mpx()<< " mpy=" << pMET->mpy() << " sqrt(mpx^2+mpy^2)=" << TMath::Sqrt(pMET->mpx()*pMET->mpx() + pMET->mpy()*pMET->mpy()) << " met=" <<  pMET->met() << " phi=" << pMET->phi() << std::endl;
      }
      if ( pMET->name() == "NonInt" ) {
          NonIntMET_x   = pMET->mpx()/1000.;
          NonIntMET_y   = pMET->mpy()/1000.;
          NonIntMET_phi = pMET->phi();
      }
      if ( pMET->name() == "IntOut" ) {
          IntOutMET_x   = pMET->mpx()/1000.;
          IntOutMET_y   = pMET->mpy()/1000.;
          IntOutMET_phi = pMET->phi();
      }
  }
  MET_x = metX/1000.;
  MET_y = metY/1000.;
  MET_phi = TMath::ATan(MET_y/MET_x);

  TLV_Lepton     = truthLepton->p4();
  TLV_LepNeu     = truthDirectLepLepNeu->p4();
  TLV_HadTauNeu  = truthDirectHadTauNeu->p4();
  TLV_LepTauNeu  = truthDirectLepTauNeu->p4();
  TLV_HadBQuark  = truthDirectBJetHad->p4();
  TLV_LepBQuark  = truthDirectBJetLep->p4();
  if ( truthElectron ) TLV_Electron   = truthElectron->p4();
  else                 TLV_Electron   . SetPtEtaPhiM(0,0,0,0);

  if ( truthMuon     ) TLV_Muon       = truthMuon->p4();
  else                 TLV_Muon       . SetPtEtaPhiM(0,0,0,0);

  if ( truthTauJet   ) TLV_TauJet     = truthTauJet->p4();
  else                 TLV_TauJet     . SetPtEtaPhiM(0,0,0,0);

  if ( truthElectron ) TLV_EleNeu     = truthDirectLepEleNeu->p4();
  else                 TLV_EleNeu     . SetPtEtaPhiM(0,0,0,0);

  if ( truthMuon     ) TLV_MuNeu      = truthDirectLepMuNeu->p4();
  else                 TLV_MuNeu      . SetPtEtaPhiM(0,0,0,0);

  if ( truthBJetHad  ) TLV_HadBJet    = truthBJetHad->p4();
  else                 TLV_HadBJet    . SetPtEtaPhiM(0,0,0,0);

  if ( truthBJetLep  ) TLV_LepBJet    = truthBJetLep->p4();
  else                 TLV_LepBJet    . SetPtEtaPhiM(0,0,0,0);

  EventFlag   = false;
  isLeptonEle = false;
  if ( truthTauJet && truthBJetHad && truthBJetLep && truthLepton ) {
      EventFlag = true;
      if ( truthElectron && !truthMuon ) 
          isLeptonEle = true;
  }
  
  /* Quadrant dedicat analysis */
  bool is1stQuadrant = false;
  bool is2ndQuadrant = false;
  bool is3rdQuadrant = false;
  bool is4thQuadrant = false;
  float Axis1_phi    = 0;
  float Axis2_phi    = 0;
  float Axis3_phi    = 0;
  float Axis4_phi    = 0;

  TLorentzVector TLV_MET;
  TLV_MET.SetXYZM( MET_x, MET_y, 0, 0);

  // Original met
  // const xAOD::TruthParticleContainer* truthPart = 0;
  // ATH_CHECK( evtStore()->retrieve( truthPart, "TruthParticles"));
  // for ( auto pTruth : *truth ) {
  //     if ( pTruth->pdgId() != 12 && pTruth->pdgId() != 14 && pTruth->pdgId() != 16 ) continue;
  //     std::cout << pTruth->pdgId() << " " << pTruth->barcode() << " " << pTruth->status() << std::endl;
  // }
  // std::cout << "-----------------------------------------" << std::endl;

  if ( truthTauJet && truthMuon || truthElectron ) {
      if      ( TLV_Lepton.Phi() < TLV_TauJet.Phi() ) { Axis1_phi = TLV_Lepton.Phi(); Axis2_phi = TLV_TauJet.Phi();}
      else if ( TLV_Lepton.Phi() > TLV_TauJet.Phi() ) { Axis1_phi = TLV_TauJet.Phi(); Axis2_phi = TLV_Lepton.Phi(); }

      if      ( Axis1_phi > 0 ) Axis3_phi = Axis1_phi - TMath::Pi();
      else if ( Axis1_phi < 0 ) Axis3_phi = Axis1_phi + TMath::Pi();              
      if      ( Axis2_phi > 0 ) Axis4_phi = Axis2_phi - TMath::Pi();
      else if ( Axis2_phi < 0 ) Axis4_phi = Axis2_phi + TMath::Pi();              

      for ( int iQuadrant=0; iQuadrant<4; iQuadrant++ ){
          double phi_max = 0;
          double phi_min = 0;
          if ( iQuadrant == 0 )             { phi_max = Axis2_phi; phi_min = Axis1_phi;}
          if ( iQuadrant == 1 ) { 
              if   ( Axis2_phi > Axis3_phi ){ phi_max = Axis2_phi; phi_min = Axis3_phi;} 
              else                          { phi_max = Axis3_phi; phi_min = Axis2_phi;} 
          }
          if ( iQuadrant == 2 ) { 
              if   ( Axis3_phi > Axis4_phi ){ phi_max = Axis3_phi; phi_min = Axis4_phi;} 
              else                          { phi_max = Axis4_phi; phi_min = Axis3_phi;} 
          }
          if ( iQuadrant == 3 ) { 
              if   ( Axis4_phi > Axis1_phi ){ phi_max = Axis4_phi; phi_min = Axis1_phi;} 
              else                          { phi_max = Axis1_phi; phi_min = Axis4_phi;} 
          }

          if ( phi_max - phi_min < TMath::Pi() ) {
              if ( phi_min < TLV_MET.Phi() && TLV_MET.Phi() < phi_max ) {
                  if      ( iQuadrant == 0 ){is1stQuadrant=true;}
                  else if ( iQuadrant == 1 ){is2ndQuadrant=true;}
                  else if ( iQuadrant == 2 ){is3rdQuadrant=true;}
                  else if ( iQuadrant == 3 ){is4thQuadrant=true;}
              }
          }
          else {
              if ( (-1*TMath::Pi() < TLV_MET.Phi() && TLV_MET.Phi() < phi_min) ||
                      (phi_max     < TLV_MET.Phi() && TLV_MET.Phi() < TMath::Pi()) ){
                  if      ( iQuadrant == 0 ){is1stQuadrant=true;}
                  else if ( iQuadrant == 1 ){is2ndQuadrant=true;}
                  else if ( iQuadrant == 2 ){is3rdQuadrant=true;}
                  else if ( iQuadrant == 3 ){is4thQuadrant=true;}
              }
          }
      }

      float Min_dPhiMet = 999;
      if  ( fabs(TLV_MET.DeltaPhi( TLV_TauJet )) < fabs(TLV_MET.DeltaPhi( TLV_Lepton )) ) 
          Min_dPhiMet =TLV_MET.DeltaPhi( TLV_TauJet );
      else 
          Min_dPhiMet =TLV_MET.DeltaPhi( TLV_Lepton );
      
      dPhiMet_1stQuad = 999;
      dPhiMet_2ndQuad = 999;
      dPhiMet_3rdQuad = 999;
      dPhiMet_4thQuad = 999;
      MetProjection_1stQuad =999; 
      MetProjection_2ndQuad =999; 
      MetProjection_3rdQuad =999; 
      MetProjection_4thQuad =999; 

      if ( is1stQuadrant ){ dPhiMet_1stQuad = fabs(Min_dPhiMet); MetProjection_1stQuad = TLV_MET.Perp()*TMath::Sin(fabs(Min_dPhiMet));}
      if ( is2ndQuadrant ){ dPhiMet_2ndQuad = fabs(Min_dPhiMet); MetProjection_2ndQuad = TLV_MET.Perp()*TMath::Sin(fabs(Min_dPhiMet));} 
      if ( is3rdQuadrant ){ dPhiMet_3rdQuad = fabs(Min_dPhiMet); MetProjection_3rdQuad = TLV_MET.Perp()*TMath::Sin(fabs(Min_dPhiMet));} 
      if ( is4thQuadrant ){ dPhiMet_4thQuad = fabs(Min_dPhiMet); MetProjection_4thQuad = TLV_MET.Perp()*TMath::Sin(fabs(Min_dPhiMet));} 
  }

  // LQ(lep)
  LQ_M_lep                = LQlep->p4().M()/1000.;
  LQ_M_lep_direct         = ( truthDirectTauLep->p4() + truthDirectBJetLep->p4() ).M()/1000.;
  LQ_M_lep_directLepJet   = ( truthLepton->p4()       + truthDirectBJetLep->p4() ).M()/1000.;
  if ( truthBJetLep != nullptr ) LQ_M_lep_directLepAntiKt4Jet   = ( truthLepton->p4()       + truthBJetLep->p4() ).M()/1000.;
  LQ_M_lep_TauJet         = ( truthTauLep->p4()       + truthBJetLepPair  ->p4() ).M()/1000.;
  LQ_M_lep_LepJet         = ( truthLepton->p4()       + truthBJetLepPair  ->p4() ).M()/1000.;
  if ( truthElectron != nullptr ) {
    LQ_M_lep_EleJet       = ( truthElectron->p4() + truthBJetLepPair  ->p4() ).M()/1000.;
    LQ_M_lep_directEleJet = ( truthElectron->p4() + truthDirectBJetLep->p4() ).M()/1000.;
   if ( truthBJetLep != nullptr )  LQ_M_lep_directEleAntiKt4Jet = ( truthElectron->p4() + truthBJetLep->p4() ).M()/1000.;
  }
  if ( truthMuon     != nullptr ) {
    LQ_M_lep_MuJet        = ( truthMuon->p4() + truthBJetLepPair  ->p4() ).M()/1000.;
    LQ_M_lep_directMuJet  = ( truthMuon->p4() + truthDirectBJetLep->p4() ).M()/1000.;
   if ( truthBJetLep != nullptr )  LQ_M_lep_directMuAntiKt4Jet = ( truthMuon->p4() + truthBJetLep->p4() ).M()/1000.;
  }
  LQ_M_lep_minusTau       = ( LQlep->p4() - truthTauLep->p4()      ).M()/1000.;
  LQ_M_lep_minusJet       = ( LQlep->p4() - truthBJetLepPair->p4() ).M()/1000.;
  
  // LQ(had)
  LQ_M_had                = LQhad->p4().M()/1000.;
  LQ_M_had_direct         = ( truthDirectTauHad->p4() + truthDirectBJetHad->p4()).M()/1000.;
  LQ_M_had_VisTauJet      = ( truthDirectVisTauHad    + truthDirectBJetHad->p4()).M()/1000.;
  if ( truthBJetHad != nullptr ) LQ_M_had_VisTauAntiKt4Jet      = ( truthDirectVisTauHad    + truthBJetHad->p4()).M()/1000.;
  LQ_M_had_TauJet         = ( truthTauHad->p4()       + truthBJetHadPair->p4()  ).M()/1000.;
  LQ_M_had_minusTau       = ( LQhad->p4() - truthTauHad->p4()      ).M()/1000.;
  LQ_M_had_minusJet       = ( LQhad->p4() - truthBJetHadPair->p4() ).M()/1000.;

  BJetLepM = truthBJetLepPair->p4().M()/1000.;
  BJetHadM = truthBJetHadPair->p4().M()/1000.;
  TauLepM  = truthTauLep->p4().M()/1000.;
  TauHadM  = truthTauHad->p4().M()/1000.;

  TauPt_truth      = truthTauHad->pt()/1000.;
  if ( truthElectron != nullptr ) ElectronPt_truth = truthElectron->pt()/1000.;
  if ( truthMuon     != nullptr ) MuonPt_truth     = truthMuon->pt()/1000.;
  HadBJetPt_truth  = truthBJetHadPair->pt()/1000.;
  LepBJetPt_truth  = truthBJetLepPair->pt()/1000.;

  /* You mustn't change the order.*/
  std::vector<const xAOD::TruthParticle*> Object =
  {
    /* had-side */
    (const xAOD::TruthParticle*) truthBJetHad,
    truthDirectTauHad,
    truthDirectHadTauNeu,

    /* lep-side */
    (const xAOD::TruthParticle*) truthBJetLep,
    truthDirectTauLep,
    truthDirectLepTauNeu,
    truthDirectLepLepNeu,
    truthDirectLepEleNeu,
    truthDirectLepMuNeu,
    truthLepton,
    truthElectron,
    truthMuon,
  };
  int iObjectIndex = 0;
  for ( unsigned int iObj=0; iObj<Object.size(); iObj++){
    for ( unsigned int jObj=iObj+1; jObj<Object.size(); jObj++){
      DeltaR  [iObjectIndex] = 999;
      DeltaPhi[iObjectIndex] = 999;
      DeltaEta[iObjectIndex] = 999;
      iObjectIndex++;
    }
  }
  iObjectIndex = -1;
  for ( unsigned int iObj=0; iObj<Object.size(); iObj++){
    for ( unsigned int jObj=iObj+1; jObj<Object.size(); jObj++){
      iObjectIndex++;
      if ( Object[iObj] == nullptr ) continue;
      if ( Object[jObj] == nullptr ) continue;
      DeltaR[iObjectIndex]   = Object[iObj]->p4().DeltaR  (Object[jObj]->p4());
      DeltaPhi[iObjectIndex] = Object[iObj]->p4().DeltaPhi(Object[jObj]->p4());
      DeltaEta[iObjectIndex] = Object[iObj]->p4().Eta() -  Object[jObj]->p4().Eta();
    }
  }
  
  dR_LQLQ    = LQlep ->p4().DeltaR(   LQhad ->p4() );
  dPhi_LQLQ  = LQlep ->p4().DeltaPhi( LQhad ->p4() );
  dEta_LQLQ  = LQlep ->p4().Eta()   - LQhad ->p4().Eta();

  if ( truthBJetHad != nullptr ) {
      BottomHad_P   = truthBJetHad->p4().P()/1000.;
      BottomHad_Pt  = truthBJetHad->p4().Pt()/1000.;
      BottomHad_Eta = truthBJetHad->p4().Eta();
      BottomHad_Phi = truthBJetHad->p4().Phi();
  }

  if ( truthBJetLep != nullptr ) {
      BottomLep_P   = truthBJetLep->p4().P()/1000.;
      BottomLep_Pt  = truthBJetLep->p4().Pt()/1000.;
      BottomLep_Eta = truthBJetLep->p4().Eta();
      BottomLep_Phi = truthBJetLep->p4().Phi();
  }

  LQLep_P   = LQlep->p4().P()/1000.;
  LQLep_Pt  = LQlep->p4().Pt()/1000.;
  LQLep_Eta = LQlep->p4().Eta();     
  LQLep_Phi = LQlep->p4().Phi();     
  
  LQHad_P  = LQhad->p4().P()/1000.;
  LQHad_Pt  = LQhad->p4().Pt()/1000.;
  LQHad_Eta = LQhad->p4().Eta();     
  LQHad_Phi = LQhad->p4().Phi();     
  
  VisTau_P     = truthDirectVisTauHad.P()/1000.;
  VisTau_Pt     = truthDirectVisTauHad.Pt()/1000.;
  VisTau_Eta    = truthDirectVisTauHad.Eta();
  VisTau_Phi    = truthDirectVisTauHad.Phi();

  HadTauNeu_P  = truthDirectHadTauNeu->p4().P()/1000.;
  HadTauNeu_Pt  = truthDirectHadTauNeu->p4().Pt()/1000.;
  HadTauNeu_Eta = truthDirectHadTauNeu->p4().Eta();
  HadTauNeu_Phi = truthDirectHadTauNeu->p4().Phi();
  
  LepTauNeu_P  = truthDirectLepTauNeu->p4().P()/1000.;
  LepTauNeu_Pt  = truthDirectLepTauNeu->p4().Pt()/1000.;
  LepTauNeu_Eta = truthDirectLepTauNeu->p4().Eta();
  LepTauNeu_Phi = truthDirectLepTauNeu->p4().Phi();
    
  Lep_P   = truthLepton->p4().P()/1000.;
  Lep_Pt  = truthLepton->p4().Pt()/1000.;
  Lep_Eta = truthLepton->p4().Eta();
  Lep_Phi = truthLepton->p4().Phi();
  
  if ( truthElectron != nullptr ) {
    Electron_P  = truthElectron->p4().P()/1000.;
    Electron_Pt  = truthElectron->p4().Pt()/1000.;
    Electron_Eta = truthElectron->p4().Eta();
    Electron_Phi = truthElectron->p4().Phi();
    LepEleNeu_P = truthDirectLepEleNeu->p4().P()/1000.;
    LepEleNeu_Pt = truthDirectLepEleNeu->p4().Pt()/1000.;
    LepEleNeu_Eta= truthDirectLepEleNeu->p4().Eta();
    LepEleNeu_Phi= truthDirectLepEleNeu->p4().Phi();
  }
  
  if ( truthMuon     != nullptr ) {
    Muon_P      = truthMuon->p4().P()/1000.;
    Muon_Pt      = truthMuon->p4().Pt()/1000.;
    Muon_Eta     = truthMuon->p4().Eta();
    Muon_Phi     = truthMuon->p4().Phi();
    LepMuNeu_P  = truthDirectLepMuNeu->p4().P()/1000.;
    LepMuNeu_Pt  = truthDirectLepMuNeu->p4().Pt()/1000.;
    LepMuNeu_Eta = truthDirectLepMuNeu->p4().Eta();
    LepMuNeu_Phi = truthDirectLepMuNeu->p4().Phi();
  }

  // Needed info to construct the collinear mass
  Lep_theta    = truthLepton  ->p4().Theta();
  Lep_phi      = truthLepton  ->p4().Phi();
  if ( truthElectron != nullptr ) {
      Ele_theta    = truthElectron->p4().Theta();
      Ele_phi      = truthElectron->p4().Phi();
  }
  if ( truthMuon     != nullptr ) {
      Mu_theta     = truthMuon    ->p4().Theta();
      Mu_phi       = truthMuon    ->p4().Phi();
  }
  if ( truthTauJet != nullptr ) {
      TauJet_P     = truthTauJet->p4().P()/1000.;
      TauJet_theta = truthTauJet->p4().Theta();
      TauJet_phi   = truthTauJet->p4().Phi();
  }

  const xAOD::TauJet*   RecoTau      = 0;
  const xAOD::Electron* RecoElectron = 0;
  const xAOD::Muon*     RecoMuon     = 0;
  const xAOD::Jet*      RecoHadBJet  = 0;
  const xAOD::Jet*      RecoLepBJet  = 0;

  // Initialize
  // Electron
  Reco_Electron_Pt         .clear();
  Reco_Electron_Eta        .clear();
  Reco_Electron_Phi        .clear();
  Reco_Electron_M          .clear();
  Reco_Electron_truthPdgId .clear();
  Reco_Electron_charge     .clear();
  Reco_Electron_dRTruthEle .clear();
  // Muon
  Reco_Muon_Pt             .clear();
  Reco_Muon_Eta            .clear();
  Reco_Muon_Phi            .clear();
  Reco_Muon_M              .clear();
  Reco_Muon_truthPdgId     .clear();
  Reco_Muon_charge         .clear();
  Reco_Muon_dRTruthMu      .clear();
  // TauJet
  Reco_TauJet_Pt           .clear();
  Reco_TauJet_Eta          .clear();
  Reco_TauJet_Phi          .clear();
  Reco_TauJet_M            .clear();
  Reco_TauJet_charge       .clear();
  Reco_TauJet_dRTruthTauJet.clear();
  Reco_TauJet_truthPdgId   .clear();
  // BJet
  Reco_BJet_Pt             .clear();
  Reco_BJet_Eta            .clear();
  Reco_BJet_Phi            .clear();
  Reco_BJet_M              .clear();
  Reco_BJet_dRTruthHadBJet .clear();
  Reco_BJet_dRTruthLepBJet .clear();
  Reco_BJet_truthPdgId     .clear();

  const xAOD::TauJetContainer* RecoTaus = 0;
  ATH_CHECK( evtStore()->retrieve( RecoTaus, "TauJets" ));
  typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
  static const char* NAME = "truthParticleLink";
  for ( auto pTau : *RecoTaus ){

      Reco_TauJet_Pt            .push_back( pTau->p4().Pt()/1000.);
      Reco_TauJet_Eta           .push_back( pTau->p4().Eta());
      Reco_TauJet_Phi           .push_back( pTau->p4().Phi());
      Reco_TauJet_M             .push_back( pTau->p4().M());
      Reco_TauJet_charge        .push_back( pTau->charge());

      if ( truthTauHad ) Reco_TauJet_dRTruthTauJet.push_back( pTau->p4().DeltaR(truthTauHad->p4() ) );
      else               Reco_TauJet_dRTruthTauJet.push_back( 999 );

      // Truth info
      if ( pTau->isAvailable<Link_t>(NAME)) {
	  const Link_t& link = pTau->auxdata<Link_t>(NAME);
	  if (link.isValid()) {
	      xAOD::TruthParticle *truthPtcs = (xAOD::TruthParticle*) *link;
	      Reco_TauJet_truthPdgId.push_back( truthPtcs->pdgId() );
	  }
      } else {
	  Reco_TauJet_truthPdgId.push_back( -1 );
      }
  }

  const xAOD::ElectronContainer* RecoEles = 0;
  ATH_CHECK( evtStore()->retrieve( RecoEles, "Electrons" ));
  for ( auto pEle : *RecoEles ){
      Reco_Electron_Pt            .push_back( pEle->p4().Pt()/1000.);
      Reco_Electron_Eta           .push_back( pEle->p4().Eta());
      Reco_Electron_Phi           .push_back( pEle->p4().Phi());
      Reco_Electron_M             .push_back( pEle->p4().M());
      Reco_Electron_charge        .push_back( pEle->charge());
      // Truth info
      if ( truthElectron && pEle->isAvailable<Link_t>(NAME)) {
	  const Link_t& link = pEle->auxdata<Link_t>(NAME);
	  if (link.isValid()) {
	      xAOD::TruthParticle *truthPtcs = (xAOD::TruthParticle*) *link;
	      Reco_Electron_truthPdgId.push_back( truthPtcs->pdgId() );
	  }
      } else {
	  Reco_Electron_truthPdgId.push_back( -1 );
      }
      // Truth info
      if ( truthElectron ) Reco_Electron_dRTruthEle.push_back( truthElectron->p4().DeltaR( pEle->p4() ));
      else  	           Reco_Electron_dRTruthEle.push_back( 999 );
  }

  const xAOD::MuonContainer* RecoMuons = 0;
  ATH_CHECK( evtStore()->retrieve( RecoMuons, "Muons" ));
  for ( auto pMu : *RecoMuons ) {
      Reco_Muon_Pt            .push_back( pMu->p4().Pt()/1000.);
      Reco_Muon_Eta           .push_back( pMu->p4().Eta());
      Reco_Muon_Phi           .push_back( pMu->p4().Phi());
      Reco_Muon_M             .push_back( pMu->p4().M());
      Reco_Muon_charge        .push_back( pMu->charge());
      // Truth info
      if ( truthMuon ) Reco_Muon_dRTruthMu.push_back( truthMuon->p4().DeltaR( pMu->p4() ));
      else  	       Reco_Muon_dRTruthMu.push_back( 999 );
  }
  
  const xAOD::JetContainer* RecoJets = 0;
  ATH_CHECK( evtStore()->retrieve( RecoJets, "AntiKt4EMTopoJets" ));
  for ( auto pJet : *RecoJets ){
      const xAOD::BTagging* bjet = pJet->btagging();
      double mv2c10 = 0;
      bjet->MVx_discriminant("MV2c10", mv2c10 );
      bool isBTag = mv2c10>-0.8244;
      //if ( isBTag ) {
      //    Reco_BJet_Pt.push_back( pJet->p4().Pt()/1000. );
      //    Reco_BJet_Eta.push_back( pJet->p4().Eta() );
      //    Reco_BJet_Phi.push_back( pJet->p4().Phi() );
      //    Reco_BJet_M.push_back( pJet->p4().M() );
      //}else {
      //    Reco_BJet_Pt .push_back( 999 );
      //    Reco_BJet_Eta.push_back( 999 );
      //    Reco_BJet_Phi.push_back( 999 );
      //    Reco_BJet_M  .push_back( 999 );
      //}

      Reco_BJet_Pt .push_back( pJet->p4().Pt()/1000. );
      Reco_BJet_Eta.push_back( pJet->p4().Eta() );
      Reco_BJet_Phi.push_back( pJet->p4().Phi() );
      Reco_BJet_M  .push_back( pJet->p4().M() );

      if ( truthDirectBJetHad ) Reco_BJet_dRTruthHadBJet.push_back( pJet->p4().DeltaR( truthDirectBJetHad->p4()) );
      else  	                Reco_BJet_dRTruthHadBJet.push_back( 999 );
      if ( truthDirectBJetLep ) Reco_BJet_dRTruthLepBJet.push_back( pJet->p4().DeltaR( truthDirectBJetLep->p4()) );
      else  	                Reco_BJet_dRTruthLepBJet.push_back( 999 );
  }
  
  const xAOD::MissingETContainer* RecoMETContainer = 0;
  ATH_CHECK( evtStore()->retrieve( RecoMETContainer, "MET_Reference_AntiKt4EMTopo" ));
  
  double metx=0;
  double mety=0;
  for ( auto pRecoMET : *RecoMETContainer){
//      std::cout << pRecoMET->name() << " mpx=" << pRecoMET->mpx() << " mpy=" << pRecoMET->mpy() << " " << pRecoMET->met() << " " << pRecoMET->phi() << std::endl;
      //if ( pRecoMET->name() == "PVSoftTrk" || pRecoMET->name() == "FinalTrk" || pRecoMET->name() == "FinalClus" ) continue;
      if ( pRecoMET->name() == "FinalTrk" || pRecoMET->name() == "FinalClus" ) continue;
      metx += pRecoMET->mpx();
      mety += pRecoMET->mpy();
  }
  //std::cout << "Sum metx=" << metx << " mety=" << mety << " met=" << TMath::Sqrt(metx*metx + mety*mety) << std::endl;
  RecoMET_x = metx;
  RecoMET_y = mety;
  RecoMET_phi = TMath::ATan(mety/metx);

  m_true->Fill();

  return StatusCode::SUCCESS;
}

StatusCode DxAODReader :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  m_true->Write();
  return StatusCode::SUCCESS;
}
