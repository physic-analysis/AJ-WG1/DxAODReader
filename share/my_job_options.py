import AthenaRootComps.ReadAthenaxAODHybrid

## Test job for tau jets
path="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/"
filesInput = path + "/mc16_13TeV.310556.aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M1300.deriv.DAOD_HIGG4D2.e6981_e5984_a875_r10201_r10210_p3759/*" 

#says how many events to run over. Set to -1 for all events
theApp.EvtMax= 62000
#theApp.EvtMax= 1000

from glob import glob
jps.AthenaCommonFlags.FilesInput = glob(vars().get("filesInput","*.root"))  #list of input files

# create our algorithm with teh given name
#alg = CfgMgr.MyxAODAnalysis()
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'DxAODReader', 'AnalysisAlg')

# later on we'll add some configuration options for our algorithm that go here
athAlgSeq += alg

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

ServiceMgr += CfgMgr.THistSvc()
#ServiceMgr.THistSvc.Output += [ "MYSTREAM DATAFILE='MyxAODAnalysis.10000events.outputs.root' OPT='RECREATE'" ]

another_stream = "STREAM DATAFILE='test.root' OPT='RECREATE'"
ServiceMgr.THistSvc.Output += [ another_stream ]
