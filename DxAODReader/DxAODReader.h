//!  DxAODReader
/*!
  A more elaborate class description.
  */

#ifndef DxAODReader_H
#define DxAODReader_H

#include <AnaAlgorithm/AnaAlgorithm.h>

// Tau
#include "xAODTau/TauJetContainer.h"
// xAOD Tracking
#include "xAODTracking/TrackParticleContainer.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

#include <TH1.h>

class DxAODReader : public EL::AnaAlgorithm
{
  public:
    //! this is a standard algorithm constructor
    DxAODReader (const std::string& name, ISvcLocator* pSvcLocator);

    //! these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    //
    TTree* m_true;
    int eventNumber;
    bool EventFlag;
    bool isLeptonEle;

    double LQmass_lep;
    double LQmass_ele;
    double LQmass_mu;
    double LQmass_had;
    
    // LQ(lep)
    double LQ_M_lep;
    double LQ_M_lep_direct;
    double LQ_M_lep_directLepJet;
    double LQ_M_lep_directEleJet;
    double LQ_M_lep_directMuJet;
    double LQ_M_lep_directLepAntiKt4Jet;
    double LQ_M_lep_directEleAntiKt4Jet;
    double LQ_M_lep_directMuAntiKt4Jet;
    double LQ_M_lep_TauJet;
    double LQ_M_lep_LepJet;
    double LQ_M_lep_EleJet;
    double LQ_M_lep_MuJet;
    double LQ_M_lep_minusTau;
    double LQ_M_lep_minusJet;
   
    // LQ(had)
    double LQ_M_had;
    double LQ_M_had_direct;
    double LQ_M_had_TauJet;
    double LQ_M_had_VisTauJet;
    double LQ_M_had_VisTauAntiKt4Jet;
    double LQ_M_had_minusTau;
    double LQ_M_had_minusJet;

    double BJetLepM;
    double BJetHadM;
    double TauLepM;
    double TauHadM;
    double dR_HadBTau   ; 
    double dR_HadBLep   ; 
    double dR_HadBEle   ; 
    double dR_HadBMu    ; 
    double dR_LepBTau   ; 
    double dR_LepBLep   ; 
    double dR_LepBEle   ; 
    double dR_LepBMu    ; 
    double dR_TauLep    ; 
    double dR_TauEle    ; 
    double dR_TauMu     ; 
    double dR_LepBHadB  ; 
    double dR_LQLQ      ;
    
    double dPhi_HadBTau   ; 
    double dPhi_HadBLep   ; 
    double dPhi_HadBEle   ; 
    double dPhi_HadBMu    ; 
    double dPhi_LepBTau   ; 
    double dPhi_LepBLep   ; 
    double dPhi_LepBEle   ; 
    double dPhi_LepBMu    ; 
    double dPhi_TauLep    ; 
    double dPhi_TauEle    ; 
    double dPhi_TauMu     ; 
    double dPhi_LepBHadB  ; 
    double dPhi_LQLQ      ;
    
    double dEta_HadBTau   ; 
    double dEta_HadBLep   ; 
    double dEta_HadBEle   ; 
    double dEta_HadBMu    ; 
    double dEta_LepBTau   ; 
    double dEta_LepBLep   ; 
    double dEta_LepBEle   ; 
    double dEta_LepBMu    ; 
    double dEta_TauLep    ; 
    double dEta_TauEle    ; 
    double dEta_TauMu     ; 
    double dEta_LepBHadB  ; 
    double dEta_LQLQ      ;

    double TauPt;
    double ElectronPt;
    double MuonPt;
    double HadBJetPt;
    double LepBJetPt;
    
    double TauPt_truth;
    double ElectronPt_truth;
    double MuonPt_truth;
    double HadBJetPt_truth;
    double LepBJetPt_truth;
    
    double TauEta;
    double ElectronEta;
    double MuonEta;
    double HadBJetEta;
    double LepBJetEta;
    
    double TauPhi;
    double ElectronPhi;
    double MuonPhi;
    double HadBJetPhi;
    double LepBJetPhi;
    
    double LQLep_P       ;
    double LQLep_Pt       ;
    double LQLep_Eta      ;
    double LQLep_Phi      ;
    double LQHad_P       ;
    double LQHad_Pt       ;
    double LQHad_Eta      ;
    double LQHad_Phi      ;
    double BottomHad_P   ; 
    double BottomHad_Pt   ; 
    double BottomHad_Eta  ; 
    double BottomHad_Phi  ; 
    double BottomLep_P   ; 
    double BottomLep_Pt   ; 
    double BottomLep_Eta  ; 
    double BottomLep_Phi  ; 
    double VisTau_P      ; 
    double VisTau_Pt      ; 
    double VisTau_Eta     ; 
    double Lep_P        ; 
    double Lep_Pt        ; 
    double Lep_Eta       ; 
    double Lep_Phi       ; 
    double VisTau_Phi     ; 
    double Electron_P    ; 
    double Electron_Pt    ; 
    double Electron_Eta   ; 
    double Electron_Phi   ; 
    double Muon_P        ; 
    double Muon_Pt        ; 
    double Muon_Eta       ; 
    double Muon_Phi       ; 

    double HadTauNeu_P ; 
    double HadTauNeu_Pt ; 
    double HadTauNeu_Eta; 
    double HadTauNeu_Phi; 
    double LepTauNeu_P ; 
    double LepTauNeu_Pt ; 
    double LepTauNeu_Eta; 
    double LepTauNeu_Phi; 
    double LepEleNeu_P ;
    double LepEleNeu_Pt ;
    double LepEleNeu_Eta;
    double LepEleNeu_Phi;
    double LepMuNeu_P ; 
    double LepMuNeu_Pt ; 
    double LepMuNeu_Eta; 
    double LepMuNeu_Phi; 

    double Lep_theta   ;
    double Lep_phi     ;
    double Ele_theta   ;
    double Ele_phi     ;
    double Mu_theta    ;
    double Mu_phi      ;
    double TauJet_P;
    double TauJet_theta;
    double TauJet_phi  ;
    double MET_x;
    double MET_y;
    double MET_phi;
    double NonIntMET_x;
    double NonIntMET_y;
    double NonIntMET_phi;
    double IntOutMET_x;
    double IntOutMET_y;
    double IntOutMET_phi;
    double RecoMET_x;
    double RecoMET_y;
    double RecoMET_phi;
    TLorentzVector TLV_Lepton;
    TLorentzVector TLV_Electron;
    TLorentzVector TLV_Muon;
    TLorentzVector TLV_TauJet;
    TLorentzVector TLV_LepNeu;
    TLorentzVector TLV_EleNeu;
    TLorentzVector TLV_MuNeu;
    TLorentzVector TLV_HadTauNeu;
    TLorentzVector TLV_LepTauNeu;
    TLorentzVector TLV_HadBJet;
    TLorentzVector TLV_LepBJet;
    TLorentzVector TLV_HadBQuark;
    TLorentzVector TLV_LepBQuark;
   
    std::vector<double>  Reco_Electron_Pt      ;
    std::vector<double>  Reco_Electron_Eta     ;
    std::vector<double>  Reco_Electron_Phi     ;
    std::vector<double>  Reco_Electron_M       ;
    std::vector<double>  Reco_Electron_truthPdgId;
    std::vector<double>  Reco_Electron_charge  ;
    std::vector<double>  Reco_Electron_dRTruthEle  ;
    std::vector<double>  Reco_Muon_Pt      ;
    std::vector<double>  Reco_Muon_Eta     ;
    std::vector<double>  Reco_Muon_Phi     ;
    std::vector<double>  Reco_Muon_M       ;
    std::vector<double>  Reco_Muon_truthPdgId;
    std::vector<double>  Reco_Muon_charge  ;
    std::vector<double>  Reco_Muon_dRTruthMu;
    std::vector<double>  Reco_TauJet_Pt             ;
    std::vector<double>  Reco_TauJet_Eta            ;
    std::vector<double>  Reco_TauJet_Phi            ;
    std::vector<double>  Reco_TauJet_M              ;
    std::vector<double>  Reco_TauJet_charge         ;
    std::vector<double>  Reco_TauJet_dRTruthTauJet         ;
    std::vector<double>  Reco_TauJet_truthPdgId ;
    std::vector<double>  Reco_BJet_Pt;
    std::vector<double>  Reco_BJet_Eta;
    std::vector<double>  Reco_BJet_Phi;
    std::vector<double>  Reco_BJet_M;
    std::vector<double>  Reco_BJet_dRTruthHadBJet;
    std::vector<double>  Reco_BJet_dRTruthLepBJet;
    std::vector<double>  Reco_BJet_truthPdgId;

    double dPhiMet_1stQuad;
    double dPhiMet_2ndQuad;
    double dPhiMet_3rdQuad;
    double dPhiMet_4thQuad;
    double MetProjection_1stQuad;
    double MetProjection_2ndQuad;
    double MetProjection_3rdQuad;
    double MetProjection_4thQuad;
    
    double DeltaR  [66];
    double DeltaPhi[66];
    double DeltaEta[66];
};

#endif
